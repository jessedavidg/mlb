#!/bin/bash

GAMEID="12842"
STARTTIME=""
ENDTIME=""

echo --- getting new games ---
cd scrapers/mlb
python schedule.py
python boxscore.py
cd ..

echo --- getting fanduel data ---
cd fanduel
scrapy crawl fanduel -a gameid="$GAMEID"
cd ..

echo --- get lineups ---
cd lineups
cd baseballpress
scrapy crawl lineups -a gameid="$GAMEID" -a starttime="$STARTTIME" -a endtime="$ENDTIME" -a platform="fanduel"

echo --- housekeeping ---
cd ../../../housekeeping
python player_sync.py
python player_info.py

echo --- lineups ---
cd ../lineups
python lineup_optimizer.py "$GAMEID" "fanduel"