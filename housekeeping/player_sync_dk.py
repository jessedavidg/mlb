## Match fanduel players with database

import MySQLdb
import sys


db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")

c = db.cursor()
c2 = db.cursor()
c3 = db.cursor()

c.execute("""SELECT dk.dkid,dk.name,dk.pos,dk.team FROM draftkings_contests dk LEFT JOIN players ON dk.dkid=players.dkid WHERE players.dkid IS NULL GROUP BY dk.dkid""")

for result in c.fetchall():
	print result
	if result[2] == 'P':
		c2.execute("""SELECT distinct(pid) FROM pitchers WHERE name = %s""",(result[1],))
	else:
		c2.execute("""SELECT distinct(pid) FROM batters WHERE name = %s""",(result[1],))

	players = c2.fetchall()

	if len(players) == 1:
		print 'UPDATING PLAYER:',result[0],players[0][0],result[1],result[1]
		c3.execute("""INSERT IGNORE INTO players (dkid,pid,name) VALUES (%s,%s,%s)""",(result[0],players[0][0],result[1])) 
		c3.execute("""UPDATE players SET dkid = %s WHERE pid=%s""",(result[0],players[0][0]))
		db.commit()
	elif len(players) == 0:
		if result[2] == 'P':
			c3.execute("""SELECT distinct(pid),name, team, pos FROM pitchers WHERE name LIKE %s and year(gametime) = 2015""",("%"+result[1].split()[1]+'%',))
		else:
			c3.execute("""SELECT distinct(pid),name, team, pos FROM batters WHERE name LIKE %s and year(gametime) = 2015""",("%"+result[1].split()[1]+"%",))
		print 'No MATCH!', result[0], result[1], result[3]
		for d in c3.fetchall():
			print d
		print '*'*50
	else:
		print 'Multiple matches',result
		if result[2] == 'P':
			c3.execute("""SELECT distinct(pid),name, team, pos FROM pitchers WHERE name LIKE %s and year(gametime) = 2015""",("%"+result[1].split()[1]+'%',))
		else:
			c3.execute("""SELECT distinct(pid),name, team, pos FROM batters WHERE name LIKE %s and year(gametime) = 2015""",("%"+result[1].split()[1]+"%",))
		for d in c3.fetchall():
			print d
		print '*'*50