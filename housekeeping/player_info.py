import urllib2, json, sys, re
from datetime import datetime,date,timedelta
import xml.etree.ElementTree as ET
import MySQLdb

db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
b = db.cursor()
p = db.cursor()
c = db.cursor()

def process_pitcher(row):
	ab = row['ab']
	batid = row['player_id']
	sac = row['sac']
	ao = row['ao']
	go = row['go']
	pitchid = row['pitcher_id']
	cs = row['cs']
	tpa = row['tpa']
	sf = row['sf']
	t = row['t']
	np = row['np']
	tb = row['tb']
	bb = row['bb']
	hr = row['hr']
	season = row['season']
	gidp = row['gidp']
	d = row['d']
	h = row['h']
	ibb = row['ibb']
	hbp = row['hbp']
	r = row['r']
	so = row['so']
	rbi = row['rbi']

	c.execute("""INSERT IGNORE INTO battervspitcher (ab,batid,sac,ao,go,pitchid,cs,tpa,sf,t,np,tb,bb,hr,season,gidp,d,h,ibb,hbp,r,so,rbi) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
	(ab,batid,sac,ao,go,pitchid,cs,tpa,sf,t,np,tb,bb,hr,season,gidp,d,h,ibb,hbp,r,so,rbi)
	)

	return



# gameid,name,team,home,away,gametime,temp,wind_speed,wind_dir,weather,umpire_home
# pid,hbp,ab,r,h,d,t,hr,rbi,bb,so,sb,cs,pitches,bat_order,start

# d = json.load(open('joey.json','rb'))
# print d['stats_batter_vs_pitcher_composed']['stats_batter_vs_pitcher']['queryResults']['created']
# for row in d['stats_batter_vs_pitcher_composed']['stats_batter_vs_pitcher']['queryResults']['row']:
# 	print '*'*20+'\n'
# 	for key in row:
# 		print key,row[key]


c.execute("""SELECT distinct(pid) FROM players WHERE bats is NULL""")

k = 0
missed_combinations = 0
players = c.fetchall()

for player in players:
	print 'Processing player:',k
	k += 1
	pid = str(player[0])
	# print "http://mlb.com/lookup/json/named.player_info.bam?sport_code='mlb'&player_id=%s"%(pid)
	req = urllib2.Request("http://mlb.com/lookup/json/named.player_info.bam?sport_code='mlb'&player_id=%s"%(pid))
	response = urllib2.urlopen(req)
	D = json.loads(response.read())
	# print json.dumps(D,indent=4)
	throws = D['player_info']['queryResults']['row']['throws']
	age = D['player_info']['queryResults']['row']['age']
	bats = D['player_info']['queryResults']['row']['bats']
	
	c.execute("""UPDATE players SET throws = %s,bats=%s,age=%s WHERE pid = %s""",
	(throws,bats,age,pid)
	)
				

	db.commit()


print 'Missed Combinations:',missed_combinations
