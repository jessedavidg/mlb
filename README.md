# DFS Lineup Optimizer for MLB

A system for predicting MLB performance and choosing lineups on Daily Fantasy Sports websites ([Fanduel](https://www.fanduel.com) and [Draft Kings](https://www.draftkings.com)).

## Description

* This codebase creates a training set using historical MLB data.

* It uses separate models to train and predict pitcher and batter performance. Currently, a regularized linear model with feature selection is used, which results in sufficient prediction speed and accuracy.

* Finally, a set of optimal lineups are chosen using the player projections and linear programming. A stochastic approach is taken to ensure lineup variability; noise is added to each projection and a set of k lineups with minimal overlap is chosen from the top n results after many iterations.

## Example Usage

Running process.sh will do the following:

* Update historical MLB player performance
* Gather player/salary data from DFS site and weather data from forecast.io
* Scrape current starting lineups from baseball press
* Housekeeping for matching MLB/DFS players
* Player prediction and lineup optimization