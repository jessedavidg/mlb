#!/bin/bash

GAMEID="6714"
STARTTIME="19:00"
ENDTIME=""

echo --- getting new games ---
cd scrapers/mlb
python schedule.py
python boxscore.py
cd ..

echo --- getting draftkings data ---
cd draftkings
# python contests.py "$GAMEID"
cd ..

echo --- get lineups ---
cd lineups
cd baseballpress
# scrapy crawl lineups -a gameid="$GAMEID" -a starttime="$STARTTIME" -a endtime="$ENDTIME" -a platform="draftkings"

echo --- housekeeping ---
cd ../../../housekeeping
# python player_sync_dk.py
# python player_info.py

echo --- lineups ---
cd ../lineups
python lineup_optimizer.py "$GAMEID" "draftkings"