from pylab import plt
import json, sys, pickle
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.feature_selection import SelectKBest, f_classif, chi2
from sklearn.metrics import mean_squared_error
from sklearn import cross_validation
from sklearn.linear_model import SGDRegressor,Lasso,ElasticNet,Ridge,LassoLars,BayesianRidge
import numpy as np
from sklearn.svm import SVC,SVR
from sklearn.grid_search import GridSearchCV
from time import time
from operator import itemgetter
# from sklearn.neural_network.multilayer_perceptron import MultilayerPerceptronRegressor as MPR

# Utility function to report best scores
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        # print("Model with rank: {0}".format(i + 1))
        # print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
        #       score.mean_validation_score,
        #       np.std(score.cv_validation_scores)))
        # print("Parameters: {0}".format(score.parameters))
        # print("")
        scores.append(score.mean_validation_score)

def coefficients(clf,X,y):
	print 'Getting coefficients...'


	all_coef = []
	for k in range(2):
		permutation = np.random.permutation(X.shape[0])
		clf.fit(X[permutation],y[permutation])
		all_coef.append(clf.named_steps['classification'].coef_)
		print clf.named_steps['classification'].coef_[:5]
	all_coef = np.array(all_coef)
	# print np.mean(all_coef,axis=0)[:5]
	coefI = np.argsort(np.abs(np.mean(all_coef,axis=0)))[::-1] #Sorted coeff
	print coefI[-5:]
	# print np.mean(all_coef[:,coefI[:10]],axis=0)
	print '*'*20
	# sys.exit()
	return coefI

## Batter (30-60 days played)
# X = pickle.load(open('../../data/train/data_batter_X_9_70_90.p','rb'))
# y = pickle.load(open('../../data/train/data_batter_y_9_70_90.p','rb'))


## Pitcher 
X = pickle.load(open('../../data/train/data_pitcher_X_m10_gamesPlayed_4_12.p','rb'))
y = pickle.load(open('../../data/train/data_pitcher_y_m10_gamesPlayed_4_12.p','rb'))
initial_coefI = np.arange(X.shape[1])

p = preprocessing.StandardScaler()

X = X[:,initial_coefI]

print 'Train data shape:',X.shape
print "Accuracy using unweighted fp mean: %.3f"%mean_squared_error(X[:,0],y)

clf = Pipeline([
('scale', preprocessing.StandardScaler()),
# ('classification', SVR(kernel='rbf',C=3,gamma=.001)),
# ('classification', MPR(algorithm='sgd',eta0=.0001,n_hidden=2500,max_iter=300,shuffle=True,random_state=3,activation="tanh",verbose=True))
# ('classification', ElasticNet(alpha=.05,l1_ratio=.05,max_iter=1000))
('classification', Ridge(alpha=1000,solver='lsqr'))
# ('classification', Lasso(alpha=1000))
# ('classification', LassoLars(alpha=1000))
])

shift = 250
start_alpha = 3000
alphas = np.array([start_alpha-shift,start_alpha],dtype=int)
# alphas = np.arange(1000,5000,250)

param_grid = [
  {'classification__alpha': alphas} ## Ridge
  # {'classification__alpha': [.01,.03,.1]} ## Lasso
 ]

# param_grid = [
#   {'classification__alpha': np.arange(.01,.5,.1), 'classification__l1_ratio': np.arange(.01,.4,.1)} ##
#   # {'classification__alpha': np.arange(.01,.2,.05), 'classification__l1_ratio': np.arange(.01,.16,.05)} ##batter
#  ]

start = time()

if False:
	clf.fit(X,y)
	coef = clf.named_steps['classification'].coef_
	coefI = np.argsort(np.abs(coef))[::-1]
	for k in range(len(coef)):
		print k,coefI[k],coef[coefI[k]]

	plt.bar(range(len(coef)),coef[coefI])
	plt.show()

	sys.exit()


permutation = np.random.permutation(X.shape[0])
X = X[permutation,:]
y = y[permutation]

cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=50,test_size=0.2, random_state=928)
grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')
grid_search.fit(X,y)
best_score = grid_search.best_score_
print -1.*grid_search.best_score_,grid_search.best_params_
print 'time to run first iteration: %.2f'%((time()-start)/60.)

# clf.named_steps['classification'].l1_ratio = grid_search.best_params_['classification__l1_ratio']
clf.named_steps['classification'].alpha = grid_search.best_params_['classification__alpha']

scores = []

for k in range(X.shape[1]-1):

	if grid_search.best_params_['classification__alpha'] == alphas[0]:
		if ((alphas[0] - shift) > 0):
			print 'Shifting alphas.'
			alphas = alphas-shift
		print alphas
		param_grid = [{'classification__alpha': alphas} ] ## Ridge
		grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')


	if k == 100:
		cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=500,test_size=0.2, random_state=928)
		grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')
	# Fit model to get feature weights
	clf.fit(X,y)
	coef = clf.named_steps['classification'].coef_
	# coef = np.dot(p.fit_transform(X).transpose(),y)
	coefI = np.argsort(np.abs(coef))[::-1] #Sorted coeff

	# coefI = coefficients(clf,np.array(X),y)

	wfI = -1
	while wfI > -1*len(coefI):
		# Remove feature with smalles magnitude
		worst_feature = coefI[wfI]
		print 'Removing feature %i from training set on iteration %i\n'%(initial_coefI[worst_feature],k)
		# X = X[:,coefI[:-1]]
		X_minus_feature = np.delete(X,worst_feature,1)

		# Test new feature vector performance
		start = time()
		grid_search.fit(X_minus_feature,y)
		print k,-1.*grid_search.best_score_,grid_search.best_params_
		# print grid_search.best_score_,best_score
		if abs(grid_search.best_score_) <= abs(best_score) or True:
			best_score = grid_search.best_score_
			scores.append(-1.*grid_search.best_score_)
			# clf.named_steps['classification'].l1_ratio = grid_search.best_params_['classification__l1_ratio']
			clf.named_steps['classification'].alpha = grid_search.best_params_['classification__alpha']

			# Update intial coefficients
			initial_coefI = np.delete(initial_coefI,worst_feature)
			print '\n'
			print ','.join(np.array(initial_coefI,dtype=str))
			print '\n'

			# Update dataset
			X = np.delete(X,worst_feature,1)
			
			break

		else:
			print 'Removed good feature! Trying another.'
			wfI -= 1

	print 'time to run this iteration: %.2f'%((time()-start)/60.)
	print '\n'
	print '*'*50
	# print wfI,-1*len(coefI)
	if wfI == -1*len(coefI):
		break
print 'Best score:',np.min(scores)
plt.plot(scores)
plt.title('MSE vs features 4-12 games, ridge regression')
plt.show()

print ','.join(np.array(initial_coefI,dtype=str))

##############
# Best score including fp and 51 features: 9.16421702563 (9.464)
# Best score without fp and 53 features: 9.16549791473
# Best score with lr and 58 features: 9.260 (9.549)

