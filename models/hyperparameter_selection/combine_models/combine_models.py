from pylab import plt
import json, sys, pickle
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestRegressor as RF
from sklearn.feature_selection import SelectKBest, f_classif, chi2
from sklearn.metrics import mean_squared_error,mean_absolute_error
from sklearn import cross_validation
from sklearn.linear_model import SGDRegressor,Lasso,ElasticNet,LinearRegression,Ridge
import numpy as np
from sklearn.svm import SVC,SVR
from sklearn.grid_search import GridSearchCV
from time import time
from operator import itemgetter
# from sklearn.neural_network.multilayer_perceptron import MultilayerPerceptronRegressor as MPR


def train_predict(X1,X2,X3,y1,y2,y3,ind1,ind2,ind3):
	## Train model clf, predict probabilities, and determine best threshold
	f2 = pickle.load(open('../../data/train/data_batter_f_9_50_70.p','rb'))
	gc = []
	for k in range(len(f2)):
		gc.append(f2[k]['game_count'])
	gc = np.array(gc,dtype=int)
	
	clf = Pipeline([
	('scale', preprocessing.StandardScaler()),
	('classification', Ridge(alpha=2000,max_iter=5000))
	])

	m1 = Pipeline([
	('scale', preprocessing.StandardScaler()),
	('classification', Ridge(alpha=2000,max_iter=5000))
	])

	m2 = Pipeline([
	('scale', preprocessing.StandardScaler()),
	('classification', Ridge(alpha=1800,max_iter=5000))
	])

	m3 = Pipeline([
	('scale', preprocessing.StandardScaler()),
	('classification', Ridge(alpha=1900,max_iter=5000))
	])

	m1.fit(X1[:,ind1],y1)
	m2.fit(X2[:,ind2],y2)
	m3.fit(X3[:,ind3],y3)

	LR = LinearRegression(fit_intercept=False)

	print gc
	cv = cross_validation.ShuffleSplit(X3.shape[0], n_iter=200,test_size=0.2, random_state=12)

	# all_scores = []
	# for train, test in cv:
	# 	X_train, X_test, y_train, y_test = X2[train,:][:,ind2], X2[test,:][:,ind2], y2[train], y2[test]
	# 	clf.fit(X_train, y_train)

	# 	y_pred = clf.predict(X_test)
	# 	all_scores.append(mean_squared_error(y_test,y_pred))

	# print 'All scores 1 model:',np.mean(all_scores)
	all_coef = []
	all_scores = []
	all_scores_comb = []
	all_intercepts = []
	cutoff = 55
	for train, test in cv:
		X_train, X_test, y_train, y_test = X3[train,:], X3[test,:], y3[train], y3[test]

		clf.fit(X_train[:,ind3], y_train)

		y_pred1 = m1.predict(X_test[:,ind1])
		y_pred2 = m2.predict(X_test[:,ind2])

		y_pred3 = clf.predict(X_test[:,ind3])

		X_pred = np.vstack([y_pred1,y_pred2,y_pred3]).transpose()
		LR.fit(X_pred,y_test)
		all_coef.append(LR.coef_)
		all_intercepts.append(LR.intercept_)

		### 30-50
		# y_total = (y_pred1*.86 + y_pred2*.14 + y_pred3*0.)
		
		### 50-70
		# y_total = (y_pred1*.14 + y_pred2*.76 + y_pred3*.1)

		### 70-90
		y_total = (y_pred1*0. + y_pred2*.18 + y_pred3*.82)

		all_scores.append(mean_squared_error(y_test,y_pred2))
		all_scores_comb.append(mean_squared_error(y_test,y_total))

	print 'All scores 1 model:',np.mean(all_scores)
	print 'All scores 2 models:',np.mean(all_scores_comb)
	print np.mean(all_coef,axis=0)
	print np.sum(np.mean(all_coef,axis=0))
	print np.mean(all_coef,axis=0)/np.sum(np.mean(all_coef,axis=0))
	print np.mean(all_intercepts)
	return np.mean(all_scores)


# Utility function to report best scores
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")


############ 30 - 50 days ############

## Batter a = 2000
X1 = pickle.load(open('../../data/train/data_batter_X_9_30_50.p','rb'))
y1 = pickle.load(open('../../data/train/data_batter_y_9_30_50.p','rb'))
ind1 = [0,13,18,20,22,23,25,30,33,35,42,48,68,73,81,94,95,99,100,102,103,104,105,106,112,113,116,120,121,123,124,136,140,142,143,144,145,153,159,168,169,171,177,179,180,181,186,188,200]

#######################################


############ 50 - 70 days ############

## Batter a = 1800
X2 = pickle.load(open('../../data/train/data_batter_X_9_50_70.p','rb'))
y2 = pickle.load(open('../../data/train/data_batter_y_9_50_70.p','rb'))

ind2 = [1,4,5,9,18,20,22,27,28,29,31,39,41,47,48,50,54,58,76,94,96,103,104,105,106,107,108,112,113,115,120,124,132,136,139,140,146,151,152,154,162,171,179,185,186,191,193,196]
#######################################

############ 70 - 90 days ############

## Batter a = 1900
X3 = pickle.load(open('../../data/train/data_batter_X_9_70_90.p','rb'))
y3 = pickle.load(open('../../data/train/data_batter_y_9_70_90.p','rb'))
ind3 = [0,8,9,18,24,28,31,32,35,36,43,45,51,55,63,64,81,84,87,90,91,94,96,97,99,100,103,104,105,107,113,114,115,117,119,120,126,129,139,146,149,154,156,158,166,167,171,172,176,177,188,195]

#######################################

max_iter = 100
eta0 = .0001



start = time()

score = train_predict(X1,X2,X3,y1,y2,y3,ind1,ind2,ind3)


# print 'Accuracy score: %.2f\n'%(score)
# print "Accuracy using unweighted fp mean: %.3f"%mean_squared_error(fp_year,y)
