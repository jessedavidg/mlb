from pylab import plt
import json, sys, pickle
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestRegressor as RF
from sklearn.feature_selection import SelectKBest, f_classif, chi2
from sklearn.metrics import mean_squared_error,mean_absolute_error
from sklearn import cross_validation
from sklearn.linear_model import SGDRegressor,Lasso,ElasticNet,LinearRegression,Ridge
import numpy as np
from sklearn.svm import SVC,SVR
from sklearn.grid_search import GridSearchCV
from time import time
from operator import itemgetter
from sklearn.kernel_ridge import KernelRidge
# from sklearn.neural_network.multilayer_perceptron import MultilayerPerceptronRegressor as MPR


def train_predict(clf,X,y,cv):
	## Train model clf, predict probabilities, and determine best threshold

	all_scores = []
	for train, test in cv:
		X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]

		# all_scores.append(mean_squared_error(y_test,X_test[:,-1]))
		# continue

		clf.fit(X_train, y_train)
		# y_pred = np.exp(clf.predict(X_test))-3
		# y_test = np.exp(y_test)-3

		y_pred = clf.predict(X_test)

		all_scores.append(mean_squared_error(y_test,y_pred))
		# all_scores.append(mean_squared_error(y_test,y_pred))

		# print all_scores
	return np.mean(all_scores)


# Utility function to report best scores
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")


############ 30 - 50 days ############

## Batter a = 2000
# X = pickle.load(open('../../data/train/data_batter_X_9_30_50.p','rb'))
# y = pickle.load(open('../../data/train/data_batter_y_9_30_50.p','rb'))
# ind = [0,13,18,20,22,23,25,30,33,35,42,48,68,73,81,94,95,99,100,102,103,104,105,106,112,113,116,120,121,123,124,136,140,142,143,144,145,153,159,168,169,171,177,179,180,181,186,188,200]

#######################################


############ 50 - 70 days ############

## Batter a = 1800
# X = pickle.load(open('../../data/train/data_batter_X_9_50_70.p','rb'))
# y = pickle.load(open('../../data/train/data_batter_y_9_50_70.p','rb'))
# ind = [1,4,5,9,18,20,22,27,28,29,31,39,41,47,48,50,54,58,76,94,96,103,104,105,106,107,108,112,113,115,120,124,132,136,139,140,146,151,152,154,162,171,179,185,186,191,193,196]

#######################################

############ 70 - 90 days ############

## Batter a = 1900
# X = pickle.load(open('../../data/train/data_batter_X_9_70_90.p','rb'))
# y = pickle.load(open('../../data/train/data_batter_y_9_70_90.p','rb'))
# ind = [0,8,9,18,24,28,31,32,35,36,43,45,51,55,63,64,81,84,87,90,91,94,96,97,99,100,103,104,105,107,113,114,115,117,119,120,126,129,139,146,149,154,156,158,166,167,171,172,176,177,188,195]

#######################################



########### Pitcher 4-12 games ############

## Pitcher a=400
# X = pickle.load(open('../../data/train/data_pitcher_X_7_4_12.p','rb'))
# y = pickle.load(open('../../data/train/data_pitcher_y_7_4_12.p','rb'))
# ind = [5,6,8,22,26,32,37,45,48,49,50,52,56,57,69,72,76,82,87,99]

#######################################

########### Pitcher 12-20 games ############

## Pitcher a=700
X = pickle.load(open('../../data/train/data_pitcher_X_7_12_20.p','rb'))
y = pickle.load(open('../../data/train/data_pitcher_y_7_12_20.p','rb'))
ind = [0,5,6,8,14,16,27,28,32,37,45,46,47,48,51,53,63,70,74,82,87,94,100,101,102,107]

# y = np.log(y+3)
# y_avg = np.mean(y)
# y_std = np.std(y)
# y = (y-y_avg)/y_std
#######################################

fp_year = X[:,0]
X = X[:,ind]

# plt.hist(y,bins=50)
# plt.show()

print 'Train data shape:',X.shape

max_iter = 100
eta0 = .0001

clf = Pipeline([
('scale', preprocessing.StandardScaler()),
# ('classification', SVR(kernel='linear',C=.003)),
# ('classification', SVR(kernel='rbf',C=3,gamma=.001)),
# ('classification', RF(n_estimators=1000,n_jobs=3))
('classification', KernelRidge(alpha=100,kernel='rbf',gamma=.001))
# ('classification', Ridge(alpha=1))
# ('classification', MPR(algorithm='l-bfgs',eta0=eta0,n_hidden=1000,max_iter=max_iter,shuffle=True,random_state=11,activation="tanh",verbose=True))
])

# print 'max_iter:',max_iter
# print 'eta0:',eta0

start = time()


cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=30,test_size=0.2, random_state=12)


score = train_predict(clf,X,y,cv)


print 'Accuracy score: %.5f\n'%(score)
print "Accuracy using unweighted fp mean: %.3f"%mean_squared_error(fp_year,y)
