from pylab import plt
import json, sys, pickle
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.feature_selection import SelectKBest, f_classif, chi2
from sklearn.metrics import mean_squared_error
from sklearn import cross_validation
from sklearn.linear_model import SGDRegressor,Lasso,ElasticNet,Ridge
import numpy as np
from sklearn.svm import SVC,SVR
from sklearn.grid_search import GridSearchCV
from time import time
from operator import itemgetter
# from sklearn.neural_network.multilayer_perceptron import MultilayerPerceptronRegressor as MPR

# Utility function to report best scores
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        # print("Model with rank: {0}".format(i + 1))
        # print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
        #       score.mean_validation_score,
        #       np.std(score.cv_validation_scores)))
        # print("Parameters: {0}".format(score.parameters))
        # print("")
        scores.append(score.mean_validation_score)


X = pickle.load(open('../../data/train/data_batter_X_5_lr.p','rb'))
y = pickle.load(open('../../data/train/data_batter_y_5_lr.p','rb'))


print 'Train data shape:',X.shape
print "Accuracy using unweighted fp mean: %.3f"%mean_squared_error(X[:,0],y)

clf = Pipeline([
('scale', preprocessing.StandardScaler()),
# ('classification', SVR(kernel='rbf',C=3,gamma=.001)),
# ('classification', MPR(algorithm='sgd',eta0=.0001,n_hidden=2500,max_iter=300,shuffle=True,random_state=3,activation="tanh",verbose=True))
('classification', ElasticNet(alpha=.05,l1_ratio=.05,max_iter=2000))
# ('classification', Ridge(alpha=.05))
])

param_grid = [
  # {'classification__alpha': np.logspace(2, 4, 6)} ##pitcher
  {'classification__alpha': np.arange(.02,.12,.02), 'classification__l1_ratio': np.arange(.05,.1,.1)} ##batter
 ]

start = time()

clf.fit(X,y)

coef = clf.named_steps['classification'].coef_

coefI = np.argsort(np.abs(coef))[::-1]

if False:
	for k in range(len(coef)):
		print k,coefI[k],coef[coefI[k]]

	plt.bar(range(len(coef)),coef[coefI])
	plt.show()

	sys.exit()

cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=20,test_size=0.2, random_state=111)
grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')

scores = []
for k in range(1,len(coef)+1):
	# print coefI[:k]
	grid_search.fit(X[:,coefI[:k]],y)
	print k,-1.*grid_search.best_score_,grid_search.best_params_
	scores.append(-1.*grid_search.best_score_)
plt.plot(scores)
plt.title('')
plt.show()

##############
# Best score including fp and 51 features: 9.16421702563 (9.464)
# Best score without fp and 53 features: 9.16549791473
# Best score with lr and 58 features: 9.260 (9.549)

