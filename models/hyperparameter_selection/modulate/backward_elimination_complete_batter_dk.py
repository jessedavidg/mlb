import pickle, sys, time
import numpy as np
from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation
from sklearn.metrics import mean_squared_error
import pylab as plt
from operator import itemgetter
import gc

# Utility function to report best scores
def report(grid_scores, n_top=3):
	top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
	for i, score in enumerate(top_scores):
		print("Model with rank: {0}".format(i + 1))
		print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
			score.mean_validation_score,
			np.std(score.cv_validation_scores)))
		print("Parameters: {0}".format(score.parameters))
		print("")

def run_iter(clf,X,y,train,test):
	X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]
	clf.fit(X_train, y_train)
	y_pred = clf.predict(X_test)
	return mean_squared_error(y_test,y_pred)

def train_predict(clf,X,y,cv):
	## Train model clf, predict probabilities, and determine best threshold
	all_scores = []
	k = 0
	for train, test in cv:
		score = run_iter(clf,X,y,train,test)
		# print k,score
		all_scores.append(score)
		k += 1
		gc.collect()
	return np.mean(all_scores)

def modulate_features(X):
	num_fea = X.shape[1]
	num_samples = X.shape[0]

	mod = []

	for i in range(num_fea):
		mod.append(X[:,i])

	for i in range(num_fea):
		for j in range(i,num_fea):
			mod.append(X[:,i]*X[:,j])
		gc.collect()
	return np.array(mod,dtype=float).transpose()

def modulate_features_subset(X,ind):
	num_fea = X.shape[1]

	mod = []
	k = 0
	for i in range(num_fea):
		if k in ind:
			mod.append(X[:,k])
		k += 1
	for i in range(num_fea):
		for j in range(i,num_fea):
			if k in ind:
				mod.append(X[:,i]*X[:,j])
			k += 1

	mod = np.array(mod,dtype=float)
	return mod.transpose()

def trim_known_features(num_fea,i0,j0):
	keepers = []
	print num_fea
	k = 0
	for i in range(num_fea):
		keepers.append(k)
		k += 1

	for i in range(num_fea):
		for j in range(i,num_fea):
			# print k
			if (i in i0) and (j in j0):
				pass
			else:
				keepers.append(k)
			k += 1
	return np.array(keepers,dtype=int)


fresh_start = True

## Load Data
X_mod = pickle.load(open('../../../data/train/data_dk_batter_X_20.p','rb'))
y = pickle.load(open('../../../data/train/data_dk_batter_y_20.p','rb'))
X_start_shape = X_mod.shape[1]

# X_mod = X_mod[100:200,:]
# y = y[100:200]

## Define gridsearch and model
clf = Pipeline([
('scale', preprocessing.StandardScaler()),
('regression', Ridge(alpha=650000,solver='sparse_cg'))
])

if False:
	cv = cross_validation.ShuffleSplit(X_mod.shape[0], n_iter=20,test_size=0.2, random_state=280)
	X_mod = modulate_features(X_mod)

	for alpha in np.arange(100000,200000,10000):
		clf.named_steps['regression'].alpha = alpha
		print 'Error for model with alpha=%i: %.5f'%(alpha,train_predict(clf,X_mod,y,cv))
		gc.collect()


	sys.exit()




## Get coef order



## Backward elimination
backward_step = 1000
n_steps = 22

start = 670000
shift = 31000

clf.named_steps['regression'].alpha = start
alphas = np.array([start-shift,start],dtype=int)
cv = cross_validation.ShuffleSplit(X_mod.shape[0], n_iter=5,test_size=0.2, random_state=289)


if fresh_start:
	## Modulate features
	X_mod = modulate_features(X_mod)
	original_coef = np.arange(X_mod.shape[1])
	print 'X_mod shape:',X_mod.shape
	gc.collect()

	coefI = trim_known_features(X_start_shape,range(113,173),range(113,173))
	X_mod = X_mod[:,coefI]
	original_coef = original_coef[coefI]
	print 'X_mod shape:',X_mod.shape
	gc.collect()

	fout = open('idx_b_dk20.txt','wb')
	for n in range(1,n_steps+1):
		print n,clf.named_steps['regression'].alpha,X_mod.shape
		clf.fit(X_mod,y)
		coef = clf.named_steps['regression'].coef_
		clf.named_steps['regression'].alpha = clf.named_steps['regression'].alpha - shift
		coefI = np.argsort(np.abs(coef))[::-1] # Sorted coeff	
		coefI = sorted(coefI[:-backward_step])
		X_mod = X_mod[:,coefI]
		original_coef = original_coef[coefI]
		gc.collect()

		fout.write(str(X_mod.shape[1])+': ')
		fout.write(','.join(np.array(original_coef,dtype=str)))
		fout.write('\n\n')
else:
	fout = open('idxp3.txt','wb')
	coefI = []
	X_mod = modulate_features_subset(X_mod,coefI)
	original_coef = np.array(coefI,dtype=int)
	gc.collect()

print 'Backward selection shape:',X_mod.shape

## alpha starting
start = 10000
shift = 1000
alphas = np.array([start-shift,start],dtype=int)
cv = cross_validation.ShuffleSplit(X_mod.shape[0], n_iter=25,test_size=0.2, random_state=238)

fea_min = 1000
fea_max = 3000
fea_step = 100

clf.named_steps['regression'].alpha = 15000
clf.fit(X_mod,y)
coef = clf.named_steps['regression'].coef_
coefI = np.argsort(np.abs(coef))[::-1] #Sorted coeff
coefI = sorted(coefI[:fea_max])
X_mod = X_mod[:,coefI]
original_coef = original_coef[coefI]
gc.collect()



print 'Training data shape:',X_mod.shape
all_scores = []
nfea = len(coefI)
nfea_steps = np.arange(fea_min,fea_max,fea_step)
clf.named_steps['regression'].alpha = start

for n in nfea_steps[::-1]:
	print '*'*50
	print '\n'
	print 'Training with top %i features'%(n)

	clf.fit(X_mod,y)
	coef = clf.named_steps['regression'].coef_
	coefI = np.argsort(np.abs(coef))[::-1] #Sorted coeff	
	coefI = sorted(coefI[:n])
	X_mod = X_mod[:,coefI]
	original_coef = original_coef[coefI]
	gc.collect()

	scores = []
	print 'X_mod train size:',X_mod.shape
	for alpha in alphas:
		clf.named_steps['regression'].alpha = alpha
		score = train_predict(clf,X_mod,y,cv)
		print 'Alpha: %i, Score: %.5f'%(alpha,score)
		scores.append(score)
		gc.collect()


	if scores[0] < scores[1]:
		best_score = scores[0]
		best_alpha = alphas[0]
		clf.named_steps['regression'].alpha = alphas[0]
	else:
		best_score = scores[1]
		best_alpha = alphas[1]
		clf.named_steps['regression'].alpha = alphas[1]

	all_scores.append(best_score)

	fout.write(str(n)+'('+str(best_alpha)+'): ')
	fout.write(','.join(np.array(original_coef,dtype=str)))
	fout.write('\n\n')
	# print ','.join(np.array(original_coef[coefI[:k]],dtype=str))

	print '\nModel score: %.6f'%(best_score)


	## Reset alphas
	if scores[0] < scores[1]:
		alphas = alphas - shift
		print 'Shifting alphas:',alphas



#### plot results
print '\n'
plt.plot(nfea_steps[::-1],all_scores)
plt.show()





