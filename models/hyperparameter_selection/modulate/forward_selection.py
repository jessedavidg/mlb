import pickle, sys
import numpy as np
from sklearn.linear_model import Ridge
from sklearn.pipeline import Pipeline
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation
from sklearn.metrics import mean_squared_error
import pylab as plt
from operator import itemgetter

# Utility function to report best scores
def report(grid_scores, n_top=3):
	top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
	for i, score in enumerate(top_scores):
		print("Model with rank: {0}".format(i + 1))
		print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
			score.mean_validation_score,
			np.std(score.cv_validation_scores)))
		print("Parameters: {0}".format(score.parameters))
		print("")

def train_predict(clf,X,y,cv):
	## Train model clf, predict probabilities, and determine best threshold
	all_scores = []
	for train, test in cv:
		X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]

		clf.fit(X_train, y_train)
		y_pred = clf.predict(X_test)

		all_scores.append(mean_squared_error(y_test,y_pred))

	return np.mean(all_scores)

def modulate_features(X):
	num_fea = X.shape[1]

	mod = []

	for i in range(num_fea):
		for j in range(i,num_fea):
			mod.append(X[:,i]*X[:,j])

	mod = np.array(mod,dtype=float)
	return np.hstack([X,mod.transpose()])

## Load Data
X_mod = pickle.load(open('../../../data/train/data_dk_pitcher_X_11.p','rb'))
y = pickle.load(open('../../../data/train/data_dk_pitcher_y_11.p','rb'))

## Modulate features
X_mod = modulate_features(X_mod)


## Define gridsearch and model
clf = Pipeline([
('scale', preprocessing.StandardScaler()),
('regression', Ridge(alpha=140000,solver='sparse_cg')) #140k dk
])

if False:
	cv = cross_validation.ShuffleSplit(X_mod.shape[0], n_iter=20,test_size=0.2, random_state=280)

	# param_grid = [{'regression__alpha': [60000,150000,10000]} ]
	# grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')
	# ## Find best alpha and report
	# grid_search.fit(X_mod,y)
	# report(grid_search.grid_scores_)

	for alpha in np.arange(140000,200000,10000):
		clf.named_steps['regression'].alpha = alpha
		print 'Error for model with alpha=%i: %.5f'%(alpha,train_predict(clf,X_mod,y,cv))


	sys.exit()


## Get coef order
clf.fit(X_mod,y)
coef = clf.named_steps['regression'].coef_
coefI = np.argsort(np.abs(coef))[::-1] #Sorted coeff

## alpha starting
start = 3000
shift = 500
alphas = np.array([start,start+shift],dtype=int)
param_grid = [{'regression__alpha': alphas} ] ## Ridge
cv = cross_validation.ShuffleSplit(X_mod.shape[0], n_iter=50,test_size=0.2, random_state=28)
grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')


fea_start = 400
fea_max = 800
fea_step = 50

X_mod = X_mod[:,coefI[:fea_max]]
print 'Training data shape:',X_mod.shape
all_scores = []
nfea = len(coefI)
nfea_steps = np.arange(fea_start,fea_max,fea_step)

for k in nfea_steps:
	print '*'*50
	print '\n'
	print 'Training with top %i of %i features'%(k,nfea)

	# score = train_predict(clf,X_mod[:,coefI[:k+1]],y,cv)
	grid_search.fit(X_mod[:,:k],y)
	all_scores.append(-1.*grid_search.best_score_)

	print ','.join(np.array(coefI[:k],dtype=str))

	print '\nModel score: %.6f'%(-1.*grid_search.best_score_)

	## Reset alphas
	if grid_search.best_params_['regression__alpha'] == alphas[1]:
		alphas = alphas + shift
		print 'Shifting alphas:',alphas
		param_grid = [{'regression__alpha': alphas} ] ## Ridge
		grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')

	print '\n'



#### plot results
print '\n'
plt.plot(nfea_steps,all_scores)
plt.show()





