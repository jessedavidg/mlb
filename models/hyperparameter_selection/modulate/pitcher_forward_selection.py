import pickle, sys
import numpy as np
from sklearn.linear_model import Ridge,SGDRegressor
from sklearn.pipeline import Pipeline
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation
from sklearn.metrics import mean_squared_error
import pylab as plt

def train_predict(clf,X,y,cv):
	## Train model clf, predict probabilities, and determine best threshold
	all_scores = []
	for train, test in cv:
		X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]

		clf.fit(X_train, y_train)
		y_pred = clf.predict(X_test)

		all_scores.append(mean_squared_error(y_test,y_pred))

	return np.mean(all_scores)

def modulate_features(X):
	num_fea = X.shape[1]

	mod = []

	for i in range(num_fea):
		for j in range(i,num_fea):
			mod.append(X[:,i]*X[:,j])

	mod = np.array(mod,dtype=float)
	return np.hstack([X,mod.transpose()])

## Load Data
X = pickle.load(open('../../../data/train/data_pitcher_X_11.p','rb'))
y = pickle.load(open('../../../data/train/data_pitcher_y_11.p','rb'))

## Modulate features
X_mod = modulate_features(X)

## Define gridsearch and model
clf = Pipeline([
('scale', preprocessing.StandardScaler()),
('regression', SGDRegressor(alpha=100,eta0=.0003,n_iter=50,verbose=0)) #4-12:190k, 12-20:130k
])

if False:
	cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=1,test_size=0.2, random_state=280)
	param_grid = [{'regression__alpha': [100,1000,10000],'regression__eta0': [.0001,.0003]}  ]
	grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')


	## Find best alpha and set
	grid_search.fit(X_mod,y)
	# clf.named_steps['regression'].alpha = grid_search.best_params_['regression__alpha']
	print 'Best params found:',grid_search.best_params_
	print 'Model score: %.6f'%(-1.*grid_search.best_score_)
	sys.exit()


## Get coef order
clf.fit(X_mod,y)
coef = clf.named_steps['regression'].coef_
coefI = np.argsort(np.abs(coef))[::-1] #Sorted coeff

## alpha starting
start = 100
shift = 100
alphas = np.array([start,start+shift],dtype=int)
param_grid = [{'regression__alpha': [1],'regression__eta0': [.001]} ] ## Ridge
cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=20,test_size=0.2, random_state=28)
grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')


all_scores = []
nfea = len(coefI)
nfea_steps = np.arange(10,500,20)
for k in nfea_steps:
	print '*'*50
	print '\n'
	print 'Training with top %i of %i features\n'%(k,nfea)

	# score = train_predict(clf,X_mod[:,coefI[:k+1]],y,cv)
	grid_search.fit(X_mod[:,coefI[:k]],y)
	all_scores.append(-1.*grid_search.best_score_)

	print ','.join(np.array(coefI[:k+1],dtype=str))

	print '\nModel score: %.6f'%(-1.*grid_search.best_score_)
	print '\nBest params:',grid_search.best_params_

	print '\n'

##### run alternative model for comparison
ind = [0,5,6,8,14,16,28,29,32,48,50,53,56,61,65,66,77,78,79,83,85,90,96,102,114,119,126,151,152,153,157]
param_grid = [{'regression__alpha': [600]} ]
grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')
grid_search.fit(X_mod[:,ind],y)
print '\n\nAlternative model score: %.6f'%(-1.*grid_search.best_score_)



#### plot results
print '\n'
plt.plot(nfea_steps,all_scores)
plt.show()





