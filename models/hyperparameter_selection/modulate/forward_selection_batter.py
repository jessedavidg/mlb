import pickle, sys
import numpy as np
import time,gc
from sklearn.linear_model import Ridge,SGDRegressor
from sklearn.svm import LinearSVR
from sklearn.pipeline import Pipeline
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation
from sklearn.metrics import mean_squared_error
from operator import itemgetter
import pylab as plt

# Utility function to report best scores
def report(grid_scores, n_top=4):
	top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
	for i, score in enumerate(top_scores):
		print("Model with rank: {0}".format(i + 1))
		print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
			score.mean_validation_score,
			np.std(score.cv_validation_scores)))
		print("Parameters: {0}".format(score.parameters))
		print("")

def run_iter(clf,X,y,train,test):
	X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]
	clf.fit(X_train, y_train)
	y_pred = clf.predict(X_test)
	return mean_squared_error(y_test,y_pred)

def train_predict(clf,X,y,cv):
	## Train model clf, predict probabilities, and determine best threshold
	all_scores = []
	k = 0
	for train, test in cv:
		score = run_iter(clf,X,y,train,test)
		# print k,score
		all_scores.append(score)
		k += 1
		gc.collect()
	return np.mean(all_scores)

def modulate_features(X):
	num_fea = X.shape[1]
	mod = []
	for i in range(num_fea):
		for j in range(i,num_fea):
			mod.append(X[:,i]*X[:,j])

	mod = np.array(mod,dtype=float)
	return np.hstack([X,mod.transpose()])

## Load Data
X = pickle.load(open('../../../data/train/data_dk_batter_X_11.p','rb'))
yo = pickle.load(open('../../../data/train/data_dk_batter_y_11.p','rb'))

print 'Accuracty of FP total mean: %.3f'%mean_squared_error(X[:,0],yo)

for _k in range(10):
	if True: ## Sample large dataset
		idx = np.random.choice(np.arange(X.shape[0]), 9000, replace=False)
		
		# Modulate features
		X_mod = X[idx,:]
		y = yo[idx]
		# print ','.join(np.array(idx,dtype=str))

	gc.collect()
	X_mod = modulate_features(X_mod)
	print 'Model Shape:',X_mod.shape
	print 'Accuracty of FP sample mean: %.3f'%mean_squared_error(X_mod[:,0],y)
	gc.collect()

	## Define gridsearch and model
	clf = Pipeline([
	('scale', preprocessing.StandardScaler()),
	('regression', Ridge(alpha=600000,solver='sparse_cg')) #fd:850k, 
	])

	if True:
		t = time.time()
		cv = cross_validation.ShuffleSplit(X_mod.shape[0], n_iter=50,test_size=0.2, random_state=293)
		# param_grid = [{'regression__alpha': np.arange(4e5,8e6,1e5)} ]
		# grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')
		# grid_search.fit(X_mod,y)
		# report(grid_search.grid_scores_)

		## Find best alpha and report
		for alpha in np.arange(4e5,1e6,1e5):
			clf.named_steps['regression'].alpha = alpha
			score = train_predict(clf,X_mod,y,cv)
			print 'Mean for alpha=%i: %.5f'%(alpha,score)
			
		print 'Time to run: %.2f min'%((time.time()-t)/60.)
sys.exit()

## Get coef order
print 'Fitting model...'
clf.fit(X_mod,y)
coef = clf.named_steps['regression'].coef_
coefI = np.argsort(np.abs(coef))[::-1] #Sorted coeff

## alpha starting
start = 30000
shift = 5000
alphas = np.array([start,start+shift],dtype=int)
param_grid = [{'regression__alpha': alphas} ] ## Ridge
cv = cross_validation.ShuffleSplit(X_mod.shape[0], n_iter=10,test_size=0.2, random_state=28)
grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')

fea_start = 500
fea_max = 1200
fea_step = 100

all_scores = []
nfea = len(coefI)
nfea_steps = np.arange(fea_start,fea_max,fea_step)
for k in nfea_steps:
	print '*'*50
	print '\n'
	print 'Training with top %i of %i features'%(k,nfea)

	# score = train_predict(clf,X_mod[:,coefI[:k+1]],y,cv)
	grid_search.fit(X_mod[:,:k],y)
	all_scores.append(-1.*grid_search.best_score_)

	print ','.join(np.array(coefI[:k],dtype=str))

	print '\nModel score: %.6f'%(-1.*grid_search.best_score_)

	## Reset alphas
	if grid_search.best_params_['regression__alpha'] == alphas[1]:
		alphas = alphas + shift
		print 'Shifting alphas:',alphas
		param_grid = [{'regression__alpha': alphas} ] ## Ridge
		grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')

	print '\n'



#### plot results
print '\n'
plt.plot(nfea_steps,all_scores)
plt.show()





