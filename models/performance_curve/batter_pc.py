import numpy as np
import time,pickle
from datetime import datetime,date,timedelta
import csv, sys, json
from sklearn import preprocessing,decomposition
from sklearn.ensemble import RandomForestRegressor as RF
from sklearn.ensemble import ExtraTreesClassifier,AdaBoostClassifier,GradientBoostingClassifier
from sklearn.cross_validation import StratifiedKFold as KFold
from sklearn import cross_validation
from sklearn.linear_model import LinearRegression,SGDRegressor,ElasticNet
from sklearn.metrics import mean_squared_error,mean_absolute_error
import pylab as plt
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC,SVR
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.decomposition import PCA
from sklearn.tree import DecisionTreeClassifier
import MySQLdb
from scipy.misc import comb

# import warnings
# warnings.filterwarnings("ignore")

class batters():

  def __init__(self):
    self.db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
    self.pre_processors = {}
    self.feature_headers = []
    self.transforms = self.initialize_transforms()
    self.year = '2014'
    
  def sigmoid_avg(self,d,a=1,b=1,ploton=True):
    # print 'length:',len(d)
    x = np.arange((len(d)))
    # print np.average(d,weights=1./(1.+np.exp(b*(x-a))))
    if ploton:
      plt.plot(x,1./(1+np.exp(b*(x-a))))
      plt.ylim((0,1))
      # plt.show()
    return np.average(d,weights=1./(1.+np.exp(b*(x-a))))

  def initialize_transforms(self):
    ## Initialize transformations
    transforms = {}
    transforms['venues'] = self.venue_encoder()

    ## Export transformations
    pickle.dump(transforms,open('/Users/jesse123green/Documents/fantasy/mlb/data/models/transforms_batter_0.p','wb'))

    return transforms

  def venue_encoder(self):
    le = preprocessing.LabelBinarizer()
    c = self.db.cursor()
    c.execute("""SELECT DISTINCT(venue_id) from pitchers""")

    venues = []
    for venue in c.fetchall():
      venues.append(venue[0])
    le.fit(venues)

    return le


  def daterange(self,start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

  def combine_features(self,X,xnew):
    xnew = np.array(xnew,dtype=float)
    if len(xnew.shape) == 1:
      xnew = xnew.reshape(xnew.shape[0], 1)
    print X.shape
    print xnew.shape
    return np.concatenate((X,xnew),axis=1)

  def load_data(self,min_games=14):
    c = self.db.cursor()
    X = []
    y = []
    c.execute("""SELECT DATE(MIN(gametime)),DATE(MAX(gametime)) from games WHERE year(gametime) = %s"""%(self.year))
    days = c.fetchone()

    k = 0
    # for aday in self.daterange(days[1]-timedelta(20),days[1]):
    for aday in self.daterange(days[0],days[1]+timedelta(1)):
    # for aday in self.daterange(days[0],days[0]+timedelta(30)):
      print aday
      
      # hist.sum_so/hist.sum_ab,hist.sum_cs/hist.sum_ab,hist.sum_h/hist.sum_ab,hist.sum_d/hist.sum_ab,hist.sum_t/hist.sum_ab,hist.sum_hr/hist.sum_ab,hist.sum_rbi/hist.sum_ab,hist.sum_r/hist.sum_ab,hist.sum_bb/hist.sum_ab,hist.sum_sb/hist.sum_ab,hist.sum_hbp/hist.sum_ab,hist.sum_ab \
      # (hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3+hist.sum_rbi+hist.sum_r+hist.sum_bb+hist.sum_sb*2+hist.sum_hbp-(hist.sum_ab-hist.sum_h)*.25)/game_count fph,\
      c.execute("SELECT today.pid,today.gameid,today.gametime,today.team,today.home,today.away,\
        (today.h+today.d+today.t*2+today.hr*3+today.rbi+today.r+today.bb+today.sb*2+today.hbp-(today.ab-today.h)*.25) fp,\
        (hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3+hist.sum_rbi+hist.sum_r+hist.sum_bb+hist.sum_sb*2+hist.sum_hbp-(hist.sum_ab-hist.sum_h)*.25)/game_count fph\
        FROM \
        (SELECT pid,sum(ao) sum_ao,sum(go) sum_go,sum(so) sum_so,sum(cs) sum_cs,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(sb) sum_sb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(pid) game_count \
          FROM batters \
          WHERE gametime < %s and year(gametime) = %s group by pid having count(pid) > %s)\
          AS hist,\
        (SELECT gameid,gametime,team,home,away,pid,h,d,t,hr,rbi,r,bb,sb,hbp,ab \
            FROM batters \
            WHERE date(gametime) = %s AND start = 1 AND pos != 'P')\
            AS today \
        WHERE hist.pid = today.pid order by today.gameid,today.pid;",
        (aday,self.year,min_games,aday))
      k = 0
      for d in c.fetchall():
        historical_stats = list(d[7:])

        feature_headers = {}
        feature_headers['pid'] = d[0]
        feature_headers['gameid'] = d[1]
        feature_headers['gametime'] = d[2]
        feature_headers['team'] = d[3]

        # feature_headers = list(d[:4])
        if d[3] == d[4]:
          feature_headers['opp'] = d[5]
        else:
          feature_headers['opp'] = d[4]


        self.feature_headers.append(feature_headers)

        X.append(historical_stats)
        y.append(d[6])
        k += 1
      # print k

    return np.array(X,dtype=float),np.array(y,dtype=float)

  def add_batting_weighted(self,X,y,a,b):
    c = self.db.cursor()
    batting = []
    X_trim = []
    y_trim = []
    feature_headers = []
    k = 0
    for row in self.feature_headers:
      c.execute("SELECT h/(ab+sf+bb+hbp),bb/(ab+sf+bb+hbp)\
          FROM batters \
          WHERE pid=%s AND gametime >= '2014-01-01' AND gametime < %s order by gametime desc",(row['pid'],row['gametime']))
      
      result = c.fetchall()
      if result != None:
        r = []
        for row in result:
          if row[0] != None:
            r.append(float(row[0]))

        
        X_trim.append(X[k,:])
        y_trim.append(y[k])
        # feature_headers.append(row)
        batting.append(self.sigmoid_avg(r,a,b,False))
      k += 1
    X_trim = np.array(X_trim,dtype=float)
    # self.feature_headers = feature_headers
    return self.combine_features(X_trim,batting),np.array(y_trim,dtype=float)


  def train_predict(self,clf,X,y,cv):
    ## Train model clf, predict probabilities, and determine best threshold

    all_scores = []
    for train, test in cv:
        X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]

        # all_scores.append(mean_squared_error(y_test,X_test[:,-1]))
        # continue

        clf.fit(X_train, y_train)
        # y_pred = np.exp(clf.predict(X_test))
        # y_test = np.exp(y_test)

        y_pred = clf.predict(X_test)
        all_scores.append(mean_squared_error(y_test,y_pred))
        # all_scores.append(mean_absolute_error(y_test,y_pred))

    # print all_scores
    return np.mean(all_scores)


def run_cv(clf,P,X,y):
  cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=30,test_size=0.2,random_state=18)
  score = P.train_predict(clf,X,y,cv)
  print X.shape
  print 'Accuracy score: %.3f\n'%(score)
  return score


if __name__ == "__main__":


  clf = Pipeline([
  ('scale', preprocessing.StandardScaler()),
  ('classification', ElasticNet(l1_ratio=.1,alpha=.05))
  ])

  t = time.time()
  ti= time.time()
  B = batters()



  print 'Loading historical player stats...'
  X,y = B.load_data()
  print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,B,X,y)
  ti= time.time()

  all_a = np.arange(0,200,20)
  all_b = [.006,.01,.03,.06,.1,.3,.6,1,3,6,10,30]

  # all_a = np.arange(10,20,10)
  # all_b = [.000001,.001]

  scores = np.zeros((len(all_a),len(all_b)))

  for a in range(len(all_a)):
    for b in range(len(all_b)):
      _X,_y = B.add_batting_weighted(X,y,all_a[a],all_b[b])
      score = run_cv(clf,B,_X,_y)
      print all_a[a],all_b[b],score
      scores[a,b] = score
      ti= time.time()
      # B.sigmoid_avg(np.arange(0,130,1),all_a[a],all_b[b],True)
  plt.pcolor(scores)
  plt.colorbar()
  plt.show()


  print 'Total time: %.2f mins\n'%((time.time()-t)/60.)
  ti= time.time()




  print 'Train data shape:',X.shape
  print "Accuracy using unweighted fp mean: %.3f"%mean_squared_error(X[:,0],y)






