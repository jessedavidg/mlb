import MySQLdb, json,time
import numpy as np
from scipy.cluster.hierarchy import dendrogram,linkage,fclusterdata,fcluster,cophenet
from scipy.spatial.distance import pdist
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.neighbors import NearestNeighbors


def update_pitcher_clusters(db,n_clusters=3,min_samples=15,num_batters=300,num_pitchers = 200,n_components=100):
	
	c = db.cursor()

	c.execute("""SELECT batid FROM battervspitcher group by batid order by count(batid) desc LIMIT %s""",(num_batters,))
	battersI = {}

	bcount = 0
	for batter in c.fetchall():
		battersI[int(batter[0])] = bcount
		bcount += 1

	c.execute("""SELECT pitchid FROM battervspitcher group by pitchid order by count(batid) desc LIMIT %s""",(num_pitchers,))
	pitchersI = {}

	pcount = 0
	for pitcher in c.fetchall():
		pitchersI[int(pitcher[0])] = pcount
		pcount += 1

	X = -1+np.zeros((bcount,pcount))
	throwing = {}

	for p in pitchersI:
		c.execute("""SELECT pitchid,sum(h)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)) FROM battervspitcher WHERE pitchid = %s""",(p,))
		result = c.fetchone()
		h_avg = float(result[1])
		bb_avg = float(result[2])
		c.execute("""SELECT throws FROM players WHERE pid = %s""",(p,))
		result = c.fetchone()
		if result != None:
			throwing[p] = result[0]
		else:
			throwing[p] = 'NA'
		for b in battersI:
			c.execute("""SELECT pitchid,batid,sum(h)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(tpa) FROM battervspitcher WHERE batid = %s and pitchid = %s""",(b,p))

			for row in c.fetchall():
				# print row
				if row[2] != None:
					tpa = int(row[4])
					if tpa > min_samples:
						h = float(row[2])
						bb = float(row[3])
					else:
						h = ((tpa * float(row[2])) + ((min_samples - tpa) * h_avg)) / min_samples
						bb = ((tpa * float(row[3])) + ((min_samples - tpa) * bb_avg)) / min_samples
					# print h,bb,tpa
					X[battersI[b],pitchersI[p]] = h + bb
					# X[num_batters+battersI[b],pitchersI[p]] = bb
				else:

					X[battersI[b],pitchersI[p]] = h_avg + bb_avg
					# X[num_batters+battersI[b],pitchersI[p]] = bb_avg

	X = X.transpose()

	for k in range(X.shape[1]):
		# print k
		X[:,k] = X[:,k] - np.mean(X[:,k])




	pca = PCA(n_components=n_components)
	pca.fit(X)
	# print(pca.explained_variance_ratio_)
	print np.sum(pca.explained_variance_ratio_)


	X_pca = pca.transform(X)

	# for p in pitchersI:
	# 	if throwing[p] == 'L':
	# 		plt.plot(X_pca[pitchersI[p],0],X_pca[pitchersI[p],1],'o',color='blue')
	# 	elif throwing[p] == 'R':
	# 		plt.plot(X_pca[pitchersI[p],0],X_pca[pitchersI[p],1],'o',color='green')
	# 	else:
	# 		# plt.plot(X_pca[pitchersI[p],0],X_pca[pitchersI[p],1],'o',color='red')
	# 		pass
	# plt.show()


	c.execute("""TRUNCATE pitcher_clusters""")
	db.commit()

	# clt = KMeans(n_clusters = n_clusters,max_iter=1000,n_init=10)
	# clt = KMeans(n_clusters = n_clusters)
	# clt.fit(X_pca)
	# C = clt.labels_


	D = pdist(X_pca, 'cosine')
	Y =  linkage(D,method='complete')
	C = fcluster(Y,t=n_clusters,criterion='maxclust')
	# Z = dendrogram(Y)

	# print 'den'
	# plt.show()

	for p in pitchersI:
		c.execute("""INSERT INTO pitcher_clusters (pid,cluster) VALUES (%s,%s)""",(p,C[pitchersI[p]]))


	db.commit()
	return 'hi'

def update_pitcher_knn(db,min_samples=20,num_batters=200,num_pitchers = 150,n_components=100):
	
	c = db.cursor()

	c.execute("""SELECT batid FROM battervspitcher group by batid order by count(batid) desc LIMIT %s""",(num_batters,))
	battersI = {}

	bcount = 0
	for batter in c.fetchall():
		battersI[int(batter[0])] = bcount
		bcount += 1

	c.execute("""SELECT pitchid FROM battervspitcher group by pitchid order by count(batid) desc LIMIT %s""",(num_pitchers,))
	pitchersI = {}
	pitchersID = {}

	pcount = 0
	for pitcher in c.fetchall():
		pitchersI[int(pitcher[0])] = pcount
		pitchersID[pcount] = int(pitcher[0])
		pcount += 1

	X = -1+np.zeros((bcount,pcount))
	throwing = {}

	for p in pitchersI:
		c.execute("""SELECT pitchid,sum(h)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)) FROM battervspitcher WHERE pitchid = %s""",(p,))
		result = c.fetchone()
		h_avg = float(result[1])
		bb_avg = float(result[2])
		c.execute("""SELECT throws FROM players WHERE pid = %s""",(p,))
		result = c.fetchone()
		if result != None:
			throwing[p] = result[0]
		else:
			throwing[p] = 'NA'
		for b in battersI:
			c.execute("""SELECT pitchid,batid,sum(h)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(tpa) FROM battervspitcher WHERE batid = %s and pitchid = %s""",(b,p))

			for row in c.fetchall():
				# print row
				if row[2] != None:
					tpa = int(row[4])
					if tpa > min_samples:
						h = float(row[2])
						bb = float(row[3])
					else:
						h = ((tpa * float(row[2])) + ((min_samples - tpa) * h_avg)) / min_samples
						bb = ((tpa * float(row[3])) + ((min_samples - tpa) * bb_avg)) / min_samples
					# print h,bb,tpa
					X[battersI[b],pitchersI[p]] = h + bb
					# X[num_batters+battersI[b],pitchersI[p]] = bb
				else:

					X[battersI[b],pitchersI[p]] = h_avg + bb_avg
					# X[num_batters+battersI[b],pitchersI[p]] = bb_avg

	X = X.transpose()

	for k in range(X.shape[1]):
		# print k
		X[:,k] = X[:,k] - np.mean(X[:,k])




	pca = PCA(n_components=n_components)
	pca.fit(X)
	print(pca.explained_variance_ratio_)
	print np.sum(pca.explained_variance_ratio_)


	X_pca = pca.transform(X)

	for p in pitchersI:
		if throwing[p] == 'L':
			plt.plot(X_pca[pitchersI[p],0],X_pca[pitchersI[p],1],'o',color='blue')
		elif throwing[p] == 'R':
			plt.plot(X_pca[pitchersI[p],0],X_pca[pitchersI[p],1],'o',color='green')
		else:
			# plt.plot(X_pca[pitchersI[p],0],X_pca[pitchersI[p],1],'o',color='red')
			pass
	plt.show()


	c.execute("""TRUNCATE pitchers_knn""")
	db.commit()

	nn = 6
	nbrs = NearestNeighbors(n_neighbors=nn, algorithm='ball_tree').fit(X_pca)
	distances, indices = nbrs.kneighbors(X_pca)

	for p in pitchersID:
		c.execute("""INSERT INTO pitchers_knn (pitcherid,k,d,nnid) VALUES (%s,%s,%s,%s)""",(pitchersID[p],0,0,pitchersID[p]))
		for k in range(1,nn):
			print p,k,indices[p,k],distances[p,k]
			c.execute("""INSERT INTO pitchers_knn (pitcherid,k,d,nnid) VALUES (%s,%s,%s,%s)""",(pitchersID[p],k,distances[p,k],pitchersID[indices[p,k]]))

	# print indices
	# print distances

	# for p in pitchersI:
	# 	c.execute("""INSERT INTO pitcher_clusters (pid,cluster) VALUES (%s,%s)""",(p,C[pitchersI[p]]))


	db.commit()
	return 'done'

if __name__ == "__main__":
	db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
	# update_pitcher_knn(db)
	update_pitcher_clusters(db)

