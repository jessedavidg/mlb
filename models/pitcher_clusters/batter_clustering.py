import numpy as np
import time,pickle
from datetime import datetime,date,timedelta
import csv, sys, json
from sklearn import preprocessing,decomposition
from sklearn.ensemble import RandomForestRegressor as RF
from sklearn.ensemble import ExtraTreesClassifier,AdaBoostClassifier,GradientBoostingClassifier
from sklearn.cross_validation import StratifiedKFold as KFold
from sklearn import cross_validation
from sklearn.linear_model import LinearRegression,SGDRegressor,ElasticNet,Ridge
from sklearn.metrics import mean_squared_error,mean_absolute_error
import pylab as plt
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC,SVR
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.decomposition import PCA
import MySQLdb
from scipy.misc import comb
from cluster_pitchers import update_pitcher_clusters

# import warnings
# warnings.filterwarnings("ignore")

class batters():

  def __init__(self):
    self.db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
    self.pre_processors = {}
    self.feature_headers = []
    self.transforms = self.initialize_transforms()
    

  def initialize_transforms(self):
    ## Initialize transformations
    transforms = {}
    transforms['venues'] = self.venue_encoder()

    ## Export transformations
    pickle.dump(transforms,open('../../data/models/transforms_batter_0.p','wb'))

    return transforms

  def venue_encoder(self):
    le = preprocessing.LabelBinarizer()
    c = self.db.cursor()
    c.execute("""SELECT DISTINCT(venue_id) from pitchers""")

    venues = []
    for venue in c.fetchall():
      venues.append(venue[0])
    le.fit(venues)

    return le


  def daterange(self,start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

  def combine_features(self,X,xnew):
    xnew = np.array(xnew,dtype=float)
    if len(xnew.shape) == 1:
      xnew = xnew.reshape(xnew.shape[0], 1)
    # print X.shape
    # print xnew.shape
    return np.concatenate((X,xnew),axis=1)

  def load_data(self,min_games=30):
    c = self.db.cursor()
    X = []
    y = []
    c.execute("""SELECT DATE(MIN(gametime)),DATE(MAX(gametime)) from games WHERE year(gametime) = %s or year(gametime) = 2013"""%('2014'))
    days = c.fetchone()

    k = 0
    for aday in self.daterange(days[0],days[1]+timedelta(1)):
    # for aday in self.daterange(days[1]-timedelta(15),days[1]):
      print aday
      
      # hist.sum_so/hist.sum_ab,hist.sum_cs/hist.sum_ab,hist.sum_h/hist.sum_ab,hist.sum_d/hist.sum_ab,hist.sum_t/hist.sum_ab,hist.sum_hr/hist.sum_ab,hist.sum_rbi/hist.sum_ab,hist.sum_r/hist.sum_ab,hist.sum_bb/hist.sum_ab,hist.sum_sb/hist.sum_ab,hist.sum_hbp/hist.sum_ab,hist.sum_ab \
      # (hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3+hist.sum_rbi+hist.sum_r+hist.sum_bb+hist.sum_sb*2+hist.sum_hbp-(hist.sum_ab-hist.sum_h)*.25)/game_count fph,\
      c.execute("SELECT today.pid,today.gameid,today.gametime,today.team,today.home,today.away,\
        (today.h+today.d+today.t*2+today.hr*3+today.rbi+today.r+today.bb+today.sb*2+today.hbp-(today.ab-today.h)*.25) fp,\
        (hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3+hist.sum_rbi+hist.sum_r+hist.sum_bb+hist.sum_sb*2+hist.sum_hbp-(hist.sum_ab-hist.sum_h)*.25)/game_count fph,\
        (hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_bb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf), \
        hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
        hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
        hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        hist.sum_sb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_cs/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        (hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf)/game_count,\
        hist.sum_go/hist.sum_ab,hist.sum_ao/hist.sum_ab\
        FROM \
        (SELECT pid,sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(cs) sum_cs,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(sb) sum_sb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(pid) game_count \
          FROM batters \
          WHERE start = 1 AND pos != 'P' AND gametime < %s and year(gametime) = %s group by pid having count(pid) > %s)\
          AS hist,\
        (SELECT gameid,gametime,team,home,away,pid,h,d,t,hr,rbi,r,bb,sb,hbp,ab \
            FROM batters \
            WHERE date(gametime) = %s) \
            AS today \
        WHERE hist.pid = today.pid order by today.gameid,today.pid;",
        (aday,aday.year,min_games,aday))
      k = 0
      for d in c.fetchall():
        historical_stats = list(d[7:])

        feature_headers = {}
        feature_headers['year'] = aday.year
        feature_headers['pid'] = d[0]
        feature_headers['gameid'] = d[1]
        feature_headers['gametime'] = d[2]
        feature_headers['team'] = d[3]
        feature_headers['year'] = aday.year
        # feature_headers = list(d[:4])
        if d[3] == d[4]:
          feature_headers['opp'] = d[5]
        else:
          feature_headers['opp'] = d[4]


        self.feature_headers.append(feature_headers)

        X.append(historical_stats)
        y.append(d[6])
        k += 1
      # print k

    return np.array(X,dtype=float),np.array(y,dtype=float)

  def add_opposing_pitcher(self,X,y):
    c = self.db.cursor()
    pitching = []
    X_trim = []
    y_trim = []
    feature_headers = []
    k = 0
    for row in self.feature_headers:
      #sum(s)/sum(outs),sum(s)/sum(np)
      c.execute("SELECT pid,sum(so)/sum(outs),sum(bb)/sum(outs),sum(h)/sum(outs),sum(np)/sum(outs),sum(s)/sum(np) FROM pitchers WHERE gametime >= %s and gametime < %s and pid = (SELECT pid FROM pitchers WHERE gameid = %s and start = 1 and team = %s)",(str(int(row['year'])-1)+'-01-01',row['gametime'],row['gameid'],row['opp']))
      result = c.fetchone()
      whip = result[1]
      if whip != None and result[4] != None:
        X_trim.append(X[k,:])
        y_trim.append(y[k])
        row['pitcher'] = result[0]
        feature_headers.append(row)
        pitching.append(result[1:])

      k += 1
    X_trim = np.array(X_trim,dtype=float)
    self.feature_headers = feature_headers
    return self.combine_features(X_trim,pitching),np.array(y_trim,dtype=float)

  def add_batting_year(self,X,y):
    c = self.db.cursor()
    batting = []
    X_trim = []
    y_trim = []
    feature_headers = []
    k = 0
    for row in self.feature_headers:
      c.execute("SELECT hist.cpid,\
        (hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3+hist.sum_rbi+hist.sum_r+hist.sum_bb+hist.sum_sb*2+hist.sum_hbp-(hist.sum_ab-hist.sum_h)*.25)/game_count fph,\
        (hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_bb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf), \
        hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
        hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
        hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        hist.sum_sb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_cs/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        (hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf)/game_count,\
        hist.sum_go/hist.sum_ab,hist.sum_ao/hist.sum_ab\
        FROM \
        (SELECT pid,count(pid) cpid,sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(cs) sum_cs,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(sb) sum_sb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(pid) game_count \
          FROM batters \
          WHERE pid=%s AND gametime >= %s and gametime < %s group by pid having count(pid) > 10)\
          AS hist",(row['pid'],str(int(row['year'])-1)+'-01-01',row['gametime']))
      
      result = c.fetchone()
      if result != None:
        if result[1] != None and result[6] != None:
          X_trim.append(X[k,:])
          y_trim.append(y[k])
          row['game_count_2013'] = result[0]
          feature_headers.append(row)
          batting.append(result[1:])
      k += 1
    X_trim = np.array(X_trim,dtype=float)
    self.feature_headers = feature_headers
    return self.combine_features(X_trim,batting),np.array(y_trim,dtype=float)

  def add_cluster(self,X,y):
    c = self.db.cursor()
    batting = []
    X_trim = []
    y_trim = []
    feature_headers = []
    k = 0
    for row in self.feature_headers:
      # print row['pitcher'],row['pid']
      c.execute("SELECT \
        (hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_bb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf), \
        hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
        hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
        hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
        hist.sum_go/hist.sum_ab,hist.sum_ao/hist.sum_ab\
        FROM \
        (SELECT sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(batid) game_count \
          FROM battervspitcher,pitcher_clusters \
          WHERE batid=%s AND cluster = (SELECT cluster FROM pitcher_clusters WHERE pid=%s) and pitchid=pid and season < %s and season > '2009' group by cluster having sum(tpa)>20)\
          AS hist",(row['pid'],row['pitcher'],row['year']))
      
      # print "SELECT nnid FROM pitchers_knn WHERE pitchid = %s AND d < .1"%row['pitcher']
      result = c.fetchone()
      # print result
      if result != None:
        if result[0] != None and result[5] != None:
        # if result[0] != None:
          X_trim.append(X[k,:])
          y_trim.append(y[k])
          feature_headers.append(row)
          batting.append(result)
      k += 1
    X_trim = np.array(X_trim,dtype=float)
    # self.feature_headers = feature_headers
    return self.combine_features(X_trim,batting),np.array(y_trim,dtype=float)


  def train_predict(self,clf,X,y,cv):
    ## Train model clf, predict probabilities, and determine best threshold

    all_scores = []
    for train, test in cv:
        X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]

        # all_scores.append(mean_squared_error(y_test,X_test[:,-1]))
        # continue

        clf.fit(X_train, y_train)
        # y_pred = np.exp(clf.predict(X_test))
        # y_test = np.exp(y_test)

        y_pred = clf.predict(X_test)
        all_scores.append(mean_squared_error(y_test,y_pred))
        # all_scores.append(mean_absolute_error(y_test,y_pred))

    # print all_scores
    return np.mean(all_scores)


def run_cv(clf,P,X,y):

  cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=250,test_size=0.2,random_state=18)

  score = P.train_predict(clf,X,y,cv)
  print X.shape
  print 'Accuracy score: %.3f\n'%(score)
  return score


if __name__ == "__main__":

  clf = Pipeline([
  ('scale', preprocessing.StandardScaler()),
  # ('classification', ElasticNet(l1_ratio=.05,alpha=.1))
  ('classification', Ridge(alpha=1000))
  ])

  t = time.time()
  ti= time.time()
  B = batters()

  print 'Loading historical player stats...'
  X,y = B.load_data()
  print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,B,X,y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding previous year...'
  X,y = B.add_batting_year(X,y)
  print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,B,X,y)
  print 'Old model on new samples...'
  run_cv(clf,B,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding opposing pitcher'
  X,y = B.add_opposing_pitcher(X,y)
  print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,B,X,y)
  print 'Old model on new samples...'
  run_cv(clf,B,X[:,:startX[1]],y)
  ti= time.time()


  # update_pitcher_clusters(B.db,n_clusters=8,n_components=100)
  # _X,_y = B.add_cluster(X,y)

  startX = X.shape
  for n_pca in [50]:
    xdata = []
    for k in np.arange(30, 80, 10):
      print 'Clustering Pitchers into %i groups with %i components...'%(k,n_pca)
      update_pitcher_clusters(B.db,n_clusters=k,n_components=n_pca)
      _X,_y = B.add_cluster(np.array(X),np.array(y))
      
      new_score = run_cv(clf,B,_X,_y)
      ti= time.time()

  

      _X = _X[:,:startX[1]]
      score = run_cv(clf,B,_X,_y)
      
      xdata.append(new_score-score)
      print (new_score - score)


    plt.plot(xdata)

  plt.show()


  print 'Total time: %.2f mins\n'%((time.time()-t)/60.)
  ti= time.time()




  print 'Train data shape:',X.shape
  print "Accuracy using unweighted fp mean: %.3f"%mean_squared_error(X[:,0],y)

  if False:
    pickle.dump(X,open('/Users/jesse123green/Documents/fantasy/mlb/data/train/data_batter_X_no_pitcher.p','wb'))
    pickle.dump(y,open('/Users/jesse123green/Documents/fantasy/mlb/data/train/data_batter_y_no_pitcher.p','wb'))





