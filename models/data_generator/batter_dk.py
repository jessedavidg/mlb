import numpy as np
import time,pickle
from datetime import datetime,date,timedelta
import csv, sys, json
from sklearn import preprocessing,decomposition
from sklearn.ensemble import RandomForestRegressor as RF
from sklearn.ensemble import ExtraTreesClassifier,AdaBoostClassifier,GradientBoostingClassifier
from sklearn.cross_validation import StratifiedKFold as KFold
from sklearn import cross_validation
from sklearn.linear_model import LinearRegression,SGDRegressor,ElasticNet,Ridge
from sklearn.metrics import mean_squared_error,mean_absolute_error
import pylab as plt
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC,SVR
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.decomposition import PCA
from sklearn.tree import DecisionTreeClassifier
import MySQLdb
from scipy.misc import comb
from scipy.cluster.hierarchy import dendrogram,linkage,fclusterdata,fcluster,cophenet
from scipy.spatial.distance import pdist
from sklearn.decomposition import PCA
from sklearn.grid_search import GridSearchCV
from operator import itemgetter

def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")

def run_cv(clf,P,X,y):
  cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=1,test_size=0.2,random_state=18)
  score = P.train_predict(clf,X,y,cv)
  print X.shape
  print 'Accuracy score: %.6f\n'%(score)
  return score

def run_grid_search(clf,P,X,y):
  start = time.time()
  cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=5,test_size=0.2,random_state=18)
  param_grid = [
  {'classification__alpha': np.arange(.01,.6,.1), 'classification__l1_ratio': np.arange(.01,.6,.1)} ## pitcher
  ]
  grid_search = GridSearchCV(clf, param_grid=param_grid,cv=cv,scoring='mean_squared_error')
  grid_search.fit(X,y)

  print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time.time() - start, len(grid_search.grid_scores_)))
  report(grid_search.grid_scores_)
  return

class batters():

 def __init__(self):
  self.db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
  self.pre_processors = {}
  self.feature_headers = []
  self.transforms = self.initialize_transforms()
  
 def daterange(self,start_date, end_date):
  for n in range(int ((end_date - start_date).days)):
    yield start_date + timedelta(n)

 def combine_features(self,X,xnew):
  xnew = np.array(xnew,dtype=float)
  if len(xnew.shape) == 1:
   xnew = xnew.reshape(xnew.shape[0], 1)
  print X.shape
  print xnew.shape
  return np.concatenate((X,xnew),axis=1)

 def initialize_transforms(self):
  ## Initialize transformations
  transforms = {}
  # transforms['venues'] = self.venue_encoder()
  transforms = pickle.load(open('../../data/models/transforms_batter_0.p','rb'))

  ## Export transformations
  # pickle.dump(transforms,open('../../data/models/transforms_batter_0.p','wb'))

  return transforms

 def venue_encoder(self):
  le = preprocessing.LabelBinarizer()
  c = self.db.cursor()
  c.execute("""SELECT DISTINCT(venue_id) from pitchers""")

  venues = []
  for venue in c.fetchall():
   venues.append(venue[0])
  le.fit(venues)

  # print le.classes_

  return le


 def load_data(self,min_games=30,max_games=1000):
  c = self.db.cursor()
  X = []
  y = []
  c.execute("""SELECT DATE(MIN(gametime)),DATE(MAX(gametime)) from games WHERE year(gametime) IN ('2013','2014','2015')""")
  days = c.fetchone()
  k = 0
  # for aday in self.daterange(days[1]-timedelta(10),days[1]):
  for aday in self.daterange(days[0],days[1]+timedelta(1)):
  # for aday in self.daterange(days[0]+timedelta(30),days[0]+timedelta(40)):
   
   print aday

   c.execute("SELECT today.pid,today.gameid,today.gametime,today.team,today.home,today.away,hist.cpid,\
    (today.h*3+today.d*2+today.t*5+today.hr*7+today.rbi*2+today.r*2+today.bb*2+today.sb*5+today.hbp*2-2*today.cs) fp,\
    (hist.sum_h*3+hist.sum_d*2+hist.sum_t*5+hist.sum_hr*7+hist.sum_rbi*2+hist.sum_r*2+hist.sum_bb*2+hist.sum_sb*5+hist.sum_hbp*2-hist.sum_cs*2)/game_count fph,\
    (hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab),hist.sum_bb/(hist.sum_ab+hist.sum_bb+hist.sum_sf), \
    hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
    hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
    hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    hist.sum_sb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_cs/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    (hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf)/game_count,\
    hist.sum_go/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),hist.sum_ao/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
    hist.sum_h/hist.sum_ab,(hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3)/hist.sum_ab \
    FROM \
    (SELECT pid,count(pid) cpid,sum(ao) sum_ao,sum(go) sum_go,sum(so) sum_so,sum(cs) sum_cs,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(sb) sum_sb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(pid) game_count \
     FROM batters \
     WHERE gametime < %s AND year(gametime) = %s and start=1 and (ab+hbp+bb+sf)>0 group by pid having count(pid) >= %s and count(pid) < %s)\
     AS hist,\
    (SELECT gameid,gametime,team,home,away,pid,h,d,t,hr,rbi,r,bb,sb,hbp,ab,cs \
      FROM batters \
      WHERE date(gametime) = %s AND start = 1 AND (ab+hbp+bb+sf)>0 AND pos != 'P')\
      AS today \
    WHERE hist.pid = today.pid order by today.gameid,today.pid;",
    (aday,aday.year,min_games,max_games,aday))
   for d in c.fetchall():
    historical_stats = list(d[8:])

    feature_headers = {}
    feature_headers['pid'] = d[0]
    feature_headers['gameid'] = d[1]
    feature_headers['gametime'] = d[2]
    feature_headers['team'] = d[3]
    feature_headers['year'] = d[2].year

    # feature_headers = list(d[:4])
    if d[3] == d[4]:
     feature_headers['opp'] = d[5]
    else:
     feature_headers['opp'] = d[4]

    feature_headers['game_count'] = d[6]

    self.feature_headers.append(feature_headers)

    X.append(historical_stats)
    y.append(d[7])
    k += 1
   # print k

  return np.array(X,dtype=float),np.array(y,dtype=float)

 def missed_games(self,X,y,n_games):
  c = self.db.cursor()
  missed_games = []

  k = 0
  for row in self.feature_headers:
   c.execute("SELECT count(*) from batters,(select distinct(gametime) from batters where team=%s and gametime < %s order by gametime desc LIMIT 1 OFFSET %s) as `team` where pid=%s and batters.gametime>team.gametime and batters.gametime < %s",(row['team'],row['gametime'],n_games,row['pid'],row['gametime']))
   result = c.fetchone()
   # print result,row['pid'],row['team'],row['gametime']
   missed_games.append(result[0])
   k += 1
  return self.combine_features(X,missed_games),np.array(y,dtype=float)


 def add_batting_year(self,X,y):
  c = self.db.cursor()
  batting = []
  X_trim = []
  y_trim = []
  feature_headers = []
  k = 0
  for row in self.feature_headers:
   c.execute("SELECT hist.cpid,\
    (hist.sum_h*3+hist.sum_d*2+hist.sum_t*5+hist.sum_hr*7+hist.sum_rbi*2+hist.sum_r*2+hist.sum_bb*2+hist.sum_sb*5+hist.sum_hbp*2-hist.sum_cs*2)/game_count fph,\
    (hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab),hist.sum_bb/(hist.sum_ab+hist.sum_bb+hist.sum_sf), \
    hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
    hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
    hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    hist.sum_sb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_cs/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    (hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf)/game_count,\
    hist.sum_go/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),hist.sum_ao/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
    hist.sum_h/hist.sum_ab,(hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3)/hist.sum_ab \
    FROM \
    (SELECT pid,count(pid) cpid,sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(cs) sum_cs,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(sb) sum_sb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(pid) game_count \
     FROM batters \
     WHERE pid=%s AND year(gametime) >= %s and gametime < %s and start = 1 and (ab+hbp+bb+sf)>0 group by pid having count(pid) > 10)\
     AS hist",(row['pid'],str(int(row['year'])-1),row['gametime']))
   
   result = c.fetchone()
   if result != None:
    if result[1] != None and result[6] != None:
     X_trim.append(X[k,:])
     y_trim.append(y[k])
     row['game_count_2013'] = result[0]
     feature_headers.append(row)
     batting.append(result[1:])
   k += 1
  X_trim = np.array(X_trim,dtype=float)
  self.feature_headers = feature_headers
  return self.combine_features(X_trim,batting),np.array(y_trim,dtype=float)

 def add_batting_days(self,X,y,n,no):
  c = self.db.cursor()
  batting = []
  X_trim = []
  y_trim = []
  feature_headers = []
  k = 0
  for row in self.feature_headers:
   # print row['pid'],row['gametime']
   c.execute("SELECT \
    (sum(h)*3+sum(d)*2+sum(t)*5+sum(hr)*7+sum(rbi)*2+sum(r)*2+sum(bb)*2+sum(sb)*5+sum(hbp)*2-sum(cs)*2)/count(pid) fph,\
    (sum(h)-sum(d)-sum(t)-sum(hr))/(sum(ab)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)), \
    sum(d)/sum(ab),sum(t)/(sum(ab)),\
    sum(hr)/sum(ab),(sum(h)-sum(hr))/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
    sum(so)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(hbp)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
    sum(sb)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(rbi)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
    sum(r)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(cs)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
    (sum(ab)+sum(hbp)+sum(bb)+sum(sf))/count(pid),\
    sum(go)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),sum(ao)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
    sum(h)/sum(ab),(sum(h)+sum(d)+sum(t)*2+sum(hr)*3)/sum(ab) \
    FROM \
    (SELECT pid,go,ao,so,cs,h,d,t,hr,rbi,r,bb,sb,hbp,ab,sf \
     FROM batters \
     WHERE pid=%s AND gametime < %s and start=1 and (ab+hbp+bb+sf)>0 order by gametime desc LIMIT %s OFFSET %s)\
     AS hist",(row['pid'],row['gametime'],n,no))
   result = list(c.fetchone())
   if result != None:
    if result[1] != None:
     if result[6] == None:
      result[6] = 0
      result[14] = 0
      result[15] = 0
     X_trim.append(X[k,:])
     y_trim.append(y[k])
     feature_headers.append(row)
     batting.append(result)
    else:
     print 'NO FP',row['pid'],row['gametime']
   else:  
    print 'NO RESULT',row['pid'],row['gametime']
   k += 1

  X_trim = np.array(X_trim,dtype=float)
  self.feature_headers = feature_headers
  return self.combine_features(X_trim,batting),np.array(y_trim,dtype=float)

 def historical_fantasy_points(self,X,y,n):
  c = self.db.cursor()
  batting = []

  k = 0
  for row in self.feature_headers:
   # print row['pid'],row['gametime']
    c.execute("SELECT \
    (h*3 + d*2 + t*5 + hr*7 + rbi*2 + r*2 + bb*2 + sb*5 + hbp*2 - (cs*2)) fph\
    FROM batters \
    WHERE pid=%s AND gametime < %s and start=1 and (ab+hbp+bb+sf)>0 order by gametime desc LIMIT %s",(row['pid'],row['gametime'],n))

    results = c.fetchall()
    hfp = []
    for result in results:
      hfp.append(float(result[0]))
    # print row['game_count']
    batting.append(hfp)
  batting = np.array(batting)
  # print batting
  # print X
  return self.combine_features(X,batting)

 def add_similar_pitchers(self,X,y,n,year):
  c = self.db.cursor()
  batting = []
  X_trim = []
  y_trim = []
  feature_headers = []
  k = 0
  for row in self.feature_headers:
   # print row['pitcher'],row['pid']
   c.execute("SELECT \
    hist.sum_h/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_bb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf)\
    FROM \
    (SELECT sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(batid) game_count \
     FROM battervspitcher \
     WHERE batid=%s and pitchid IN (SELECT nnid FROM pitchers_knn WHERE pitcherid = %s AND d < 100) and season > 2012 and season < 2014 group by batid having sum(tpa) > 20)\
     AS hist",(row['pid'],row['pitcher']))
   
   # print "SELECT nnid FROM pitchers_knn WHERE pitchid = %s AND d < .1"%row['pitcher']
   result = c.fetchone()
   # print result
   if result != None:
    # if result[0] != None and result[5] != None:
    if result[0] != None:
     X_trim.append(X[k,:])
     y_trim.append(y[k])
     feature_headers.append(row)
     batting.append(result)
   k += 1
  X_trim = np.array(X_trim,dtype=float)
  self.feature_headers = feature_headers
  return self.combine_features(X_trim,batting),np.array(y_trim,dtype=float)

 def update_pitcher_clusters(self,n_clusters=50,min_samples=15,num_batters=300,num_pitchers =200,n_components=50):
  
  c = self.db.cursor()

  c.execute("""SELECT batid FROM battervspitcher group by batid order by count(batid) desc LIMIT %s""",(num_batters,))
  battersI = {}

  bcount = 0
  for batter in c.fetchall():
   battersI[int(batter[0])] = bcount
   bcount += 1

  c.execute("""SELECT pitchid FROM battervspitcher group by pitchid order by count(batid) desc LIMIT %s""",(num_pitchers,))
  pitchersI = {}

  pcount = 0
  for pitcher in c.fetchall():
   pitchersI[int(pitcher[0])] = pcount
   pcount += 1

  X = -1+np.zeros((bcount,pcount))
  throwing = {}

  for p in pitchersI:
   c.execute("""SELECT pitchid,sum(h)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)) FROM battervspitcher WHERE pitchid = %s""",(p,))
   result = c.fetchone()
   h_avg = float(result[1])
   bb_avg = float(result[2])
   c.execute("""SELECT throws FROM players WHERE pid = %s""",(p,))
   result = c.fetchone()
   if result != None:
    throwing[p] = result[0]
   else:
    throwing[p] = 'NA'
   for b in battersI:
    c.execute("""SELECT pitchid,batid,sum(h)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)+sum(hbp)),sum(tpa) FROM battervspitcher WHERE batid = %s and pitchid = %s""",(b,p))

    for row in c.fetchall():
     # print row
     if row[2] != None:
      tpa = int(row[4])
      if tpa > min_samples:
       h = float(row[2])
       bb = float(row[3])
      else:
       h = ((tpa * float(row[2])) + ((min_samples - tpa) * h_avg)) / min_samples
       bb = ((tpa * float(row[3])) + ((min_samples - tpa) * bb_avg)) / min_samples
      # print h,bb,tpa
      X[battersI[b],pitchersI[p]] = h + bb
      # X[num_batters+battersI[b],pitchersI[p]] = bb
     else:

      X[battersI[b],pitchersI[p]] = h_avg + bb_avg
      # X[num_batters+battersI[b],pitchersI[p]] = bb_avg

  X = X.transpose()

  for k in range(X.shape[1]):
   # print k
   X[:,k] = X[:,k] - np.mean(X[:,k])




  pca = PCA(n_components=n_components)
  pca.fit(X)
  # print(pca.explained_variance_ratio_)
  # print np.sum(pca.explained_variance_ratio_)


  X_pca = pca.transform(X)


  c.execute("""TRUNCATE pitcher_clusters""")
  self.db.commit()


  D = pdist(X_pca, 'cosine')
  Y = linkage(D,method='complete')
  C = fcluster(Y,t=n_clusters,criterion='maxclust')

  for p in pitchersI:
   c.execute("""INSERT INTO pitcher_clusters (pid,cluster) VALUES (%s,%s)""",(p,C[pitchersI[p]]))


  self.db.commit()
  return

 def add_cluster(self,X,y):
  c = self.db.cursor()
  cluster_averages = [0.1757,0.0804,0.0547,0.0062,0.0331,0.3048,0.1798,0.0082,0.1119,0.117,0.3819,0.3363,0.2696,0.4358]

  batting = []
  total_under5 = 0
  total_none = 0
  k = 0
  for row in self.feature_headers:
    # print '*'*20
    # print row['pitcher'],row['pid']
    c.execute("SELECT \
    (hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab),hist.sum_bb/(hist.sum_ab+hist.sum_bb+hist.sum_sf), \
    hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
    hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
    hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
    hist.sum_go/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),hist.sum_ao/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
    hist.sum_h/hist.sum_ab,(hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3)/hist.sum_ab,hist.sum_ab \
    FROM \
    (SELECT sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(batid) game_count,sum(tpa) sum_tpa \
    FROM battervspitcher,pitcher_clusters \
    WHERE batid=%s AND cluster = (SELECT cluster FROM pitcher_clusters WHERE pid=%s) and pitchid=pid and season < %s  and season > '2009' group by cluster having sum(tpa)>0)\
    AS hist",(row['pid'],row['pitcher'],row['year']))

    # print "SELECT nnid FROM pitchers_knn WHERE pitchid = %s AND d < .1"%row['pitcher']
    result = c.fetchone()
    cluster_stats = []
    if result != None:
      for statI in range(len(cluster_averages)):
        if result[statI] == None:
          # print 'using average!'
          cluster_stats.append(cluster_averages[statI])
        elif result[0] == None:
          cluster_stats.append(cluster_averages[statI])
        else:
          # print 'using stat :)'
          cluster_stats.append(result[statI])
    else:
      cluster_stats = list(cluster_averages)

    if result == None:
      total_none +=1
      for k in [0,0,0,0,0,0]:
        cluster_stats.append(k)
    elif result[0] == None:
      total_none +=1
      for k in [0,0,0,0,0,0]:
        cluster_stats.append(k)      
    elif result[-1] < 4:
      total_under5 += 1
      for k in [1,0,0,0,0,0]:
        cluster_stats.append(k)
    elif result[-1] < 7:
      total_under5 += 1
      for k in [0,1,0,0,0,0]:
        cluster_stats.append(k)
    elif (result[-1] < 10):
      for k in [0,0,1,0,0,0]:
        cluster_stats.append(k)
    elif (result[-1] < 15):
      for k in [0,0,0,1,0,0]:
        cluster_stats.append(k) 
    elif (result[-1] < 20):
      for k in [0,0,0,0,1,0]:
        cluster_stats.append(k)                      
    elif (result[-1] >=20):
      for k in [0,0,0,0,0,1]:
        cluster_stats.append(k) 
    else:
      print 'error boy'

    batting.append(cluster_stats)
  print 'Total under 5:',total_under5
  print 'Total none:',total_none
  return self.combine_features(X,batting),np.array(y,dtype=float)


 def add_matchup(self,X,y):
  lvp_avg = [0.2625, 0.2973, 0.0068, 0.0058, 0.0544, 0.0326, 0.0792, 0.4320, 0.2682, 0.1754, 0.1149, 0.1797, 0.1097]
  c = self.db.cursor()
  matchup = []

  for row in self.feature_headers:
    # print """SELECT sum(ao)/sum(ab),sum(go)/sum(ab),sum(cs)/sum(tpa),sum(t)/sum(ab),sum(d)/sum(ab),sum(hr)/sum(ab),sum(bb)/sum(tpa),sum(tb)/sum(ab),sum(h)/sum(ab),(sum(h)-sum(d)-sum(t)-sum(hr))/sum(ab),sum(r)/sum(tpa),sum(so)/sum(tpa),sum(rbi)/sum(tpa),sum(tpa) from battervspitcher WHERE season < %s and pitchid = %s and batid %s"""%(row['year'],row['pitcher'],row['pid'])
    c.execute("""SELECT sum(ao)/sum(ab),sum(go)/sum(ab),sum(cs)/sum(tpa),sum(t)/sum(ab),sum(d)/sum(ab),sum(hr)/sum(ab),sum(bb)/sum(tpa),sum(tb)/sum(ab),sum(h)/sum(ab),(sum(h)-sum(d)-sum(t)-sum(hr))/sum(ab),sum(r)/sum(tpa),sum(so)/sum(tpa),sum(rbi)/sum(tpa),sum(ab) \
      from battervspitcher WHERE season < %s and pitchid = %s and batid = %s""",(row['year'],row['pitcher'],row['pid']))
    result = c.fetchone()
    if result[0] == None:
      batting = list(lvp_avg)
    else:
      batting = list(result[:-1])
      

    if result[0] == None:
      for k in [0,0,0,0,0,0]:
        batting.append(k)
    elif result[-1] < 4:
      for k in [1,0,0,0,0,0]:
        batting.append(k)
    elif result[-1] < 7:
      for k in [0,1,0,0,0,0]:
        batting.append(k)
    elif (result[-1] < 10):
      for k in [0,0,1,0,0,0]:
        batting.append(k)
    elif (result[-1] < 15):
      for k in [0,0,0,1,0,0]:
        batting.append(k) 
    elif (result[-1] < 20):
      for k in [0,0,0,0,1,0]:
        batting.append(k)                       
    elif (result[-1] >=20):
      for k in [0,0,0,0,0,1]:
        batting.append(k)

    matchup.append(batting)
  return self.combine_features(X,matchup),np.array(y,dtype=float)


 def add_bo(self,X,y):
  c = self.db.cursor()
  batting_orders = []
  # X_trim = []
  # y_trim = []
  # feature_headers = []
  k = 0
  for row in self.feature_headers:
   c.execute("SELECT bo FROM batters WHERE gameid = %s and pid = %s",(row['gameid'],row['pid']))
   result = c.fetchone()
   bo = [0,0,0,0,0,0,0,0,0]
   bo[int(result[0][0])-1] = 1
   batting_orders.append(bo)
   k += 1
   row['bo'] = int(result[0][0])
  # X_trim = np.array(X_trim,dtype=float)
  # self.feature_headers = feature_headers
  return self.combine_features(X,batting_orders),np.array(y,dtype=float)

 def add_age(self,X,y):
  c = self.db.cursor()
  ages = []
  X_trim = []
  y_trim = []
  feature_headers = []
  k = 0
  for row in self.feature_headers:
   c.execute("SELECT age FROM players WHERE pid = %s",(row['pid'],))
   result = c.fetchone()
   if result != None:
    X_trim.append(X[k,:])
    y_trim.append(y[k])
    feature_headers.append(row)
    ages.append(int(result[0])-(2015-int(row['year'])))
    # ages.append([max(int(result[0])-28.4,0),min(0,28.4-int(result[0]))])
   # print ages
   k += 1
  X_trim = np.array(X_trim,dtype=float)
  self.feature_headers = feature_headers
  return self.combine_features(X_trim,ages),np.array(y_trim,dtype=float)

 def get_that_wind(self,X,y):
  c = self.db.cursor()
  winds= []
  X_trim = []
  y_trim = []
  feature_headers = []

  windI = {'Out to CF':0,'L to R':1,'In from LF':2,'In from CF':3,'In from RF':4,'Out to RF':5,'Out to LF':6,'R to L':7}
  # windI = {'Out to CF':0,'L to R':1,'In from LF':2,'In from CF':2,'In from RF':2,'Out to RF':0,'Out to LF':0,'R to L':3}
  # windI = {'Out to CF':0,'L to R':1,'In from LF':2,'In from CF':0,'In from RF':3,'Out to RF':3,'Out to LF':2,'R to L':1}
  # windC = {'Out to CF':1,'L to R':1,'In from LF':-1,'In from CF':-1,'In from RF':-1,'Out to RF':1,'Out to LF':1,'R to L':-1}
  
  k = 0
  for row in self.feature_headers:
    c.execute("SELECT wind_dir,wind_speed FROM batters WHERE gameid = %s and pid = %s",(row['gameid'],row['pid']))
    result = c.fetchone()
    if result != None:
      wind = [0,0,0,0,0,0,0,0]
      try:
        if row['venue'] not in [4169,12,15,32,2392,14]:
          wind[windI[result[0]]] = int(result[1])
      except:
        pass
    X_trim.append(X[k,:])
    y_trim.append(y[k])
    feature_headers.append(row)
    winds.append(wind)
    k += 1
  X_trim = np.array(X_trim,dtype=float)
  self.feature_headers = feature_headers
  return self.combine_features(X_trim,winds),np.array(y_trim,dtype=float)


 def add_opposing_pitcher(self,X,y):
  c = self.db.cursor()
  pitching_average = [0.0744,0.2350,3.7733,2.4001,0.1872,0.1187,0.6361,17.6559]
  pitching = []
  for row in self.feature_headers:
    c.execute("SELECT pid,count(*),sum(bb)/sum(bf),sum(h)/sum(bf),sum(np)/sum(bf),sum(s)/sum(bf),sum(so)/sum(bf),sum(r)/sum(bf),sum(s)/sum(np),sum(outs)/count(distinct(gameid)) FROM pitchers WHERE start=1 and gametime >= %s and gametime < %s and pid = (SELECT pid FROM pitchers WHERE gameid = %s and start = 1 and team = %s)",(str(int(row['year'])-1)+'-01-01',row['gametime'],row['gameid'],row['opp']))
    results = c.fetchone()
    row['pitcher'] = results[0]
    pcount = int(results[1])
    if results[2] == None:
      this_pitcher = list(pitching_average)
    else:
      this_pitcher = list(results[2:])

    if results[2] == None:
      for k in [0,0,0,0,0]:
        this_pitcher.append(k)
    elif results[1] < 4:
      for k in [1,0,0,0,0]:
        this_pitcher.append(k)
    elif results[1] < 7:
      for k in [0,1,0,0,0]:
        this_pitcher.append(k)
    elif results[1] < 10:
      for k in [0,0,1,0,0]:
        this_pitcher.append(k)
    elif results[1] < 20:
      for k in [0,0,0,1,0]:
        this_pitcher.append(k)
    else:
      for k in [0,0,0,0,1]:
        this_pitcher.append(k)

    pitching.append(this_pitcher)

  return self.combine_features(X,pitching),np.array(y,dtype=float)

 def home_away(self,X,y):
  c = self.db.cursor()
  homeaway = []
  X_trim = []
  y_trim = []
  feature_headers = []
  k = 0
  for row in self.feature_headers:
   c.execute("SELECT team,home,away FROM batters WHERE gameid = %s and pid = %s",(row['gameid'],row['pid']))
   result = c.fetchone()
   if result[0] == result[1]:
    X_trim.append(X[k,:])
    y_trim.append(y[k])
    homeaway.append(1)
    feature_headers.append(row)
   elif result[0] == result[2]:
    X_trim.append(X[k,:])
    y_trim.append(y[k])
    homeaway.append(0)
    feature_headers.append(row)
   else:
    pass
   k += 1
  X_trim = np.array(X_trim,dtype=float)
  self.feature_headers = feature_headers
  return self.combine_features(X_trim,homeaway),np.array(y_trim,dtype=float)

 def add_venue(self,X,y):
  c = self.db.cursor()
  venues = []
  X_trim = []
  y_trim = []
  feature_headers = []
  k = 0
  for row in self.feature_headers:
   c.execute("SELECT venue_id FROM batters WHERE gameid = %s and pid = %s",(row['gameid'],row['pid']))
   result = c.fetchone()
   venues.append(self.transforms['venues'].transform(result)[0])
   row['venue'] = result[0]
   k += 1
  return self.combine_features(X,venues),np.array(y,dtype=float) 

 def add_games_played_categories(self,X,y):
  c = self.db.cursor()
  gp_all = []
  for row in self.feature_headers:
    gp = [0,0,0,0]
    if row['game_count'] < 50:
      gp[0] = 1
    elif row['game_count'] < 70:
      gp[1] = 1
    elif row['game_count'] < 90:
      gp[2] = 1
    else:
      gp[3] = 1
    gp_all.append(gp)

  return self.combine_features(X,gp_all),np.array(y,dtype=float) 

 def add_venue_games(self,X,y):
  c = self.db.cursor()
  venue_games = []
  X_trim = []
  y_trim = []
  feature_headers = []
  k = 0
  for row in self.feature_headers:
   c.execute("SELECT venue_id,count(*)/games_total\
    FROM batters,\
    (SELECT count(*) games_total FROM batters WHERE pid=%s AND start=1 and (ab+hbp+bb+sf)>0 and year(gametime)=%s and gametime<%s) btotal \
    WHERE pid=%s AND start=1 and (ab+hbp+bb+sf)>0 and year(gametime)=%s and gametime<%s group by venue_id",(row['pid'],row['year'],row['gametime'],row['pid'],row['year'],row['gametime']))
   results = c.fetchall()
   venues = self.transforms['venues'].transform([-1])[0]
   # print venues
   for venue in results:
    # print self.transforms['venues'].transform([venue[0]])[0]*float(venue[1])
    # venues += self.transforms['venues'].transform([venue[0]])[0]*float(venue[1])
    venues = venues + self.transforms['venues'].transform([venue[0]])[0]*float(venue[1])

   venue_games.append(venues)
   k += 1
  return self.combine_features(X,venue_games),np.array(y,dtype=float) 

 def batters_from_lineup(self,X,y,offset=1):
  X_trim = []
  y_trim = []
  feature_headers = []

  c = self.db.cursor()
  
  team_batting = []
  k = 0

  for row in self.feature_headers:
   # bo = row['bo']
   # bo = str(bo + offset) + '00'
   # if bo == '1000':
   #  bo = '100'
   # print row['bo']

   # bo = []
   # for n in range(1,10):
   #  if row['bo'] != n:
   #   bo.append(str(n)+'00')
   c.execute("""SELECT \
      (hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_bb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf), \
      hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
      hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
      hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
      hist.sum_sb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
      hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_cs/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
      hist.sum_go/hist.sum_ab,hist.sum_ao/hist.sum_ab FROM\
      (SELECT sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(cs) sum_cs,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(sb) sum_sb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf\
      FROM batters \
      WHERE gametime < %s AND gametime >= %s AND pid IN (SELECT pid FROM batters WHERE gameid=%s AND bo != %s AND start = 1 AND team = %s)) as hist""",(row['gametime'],str(int(row['year'])-1)+'-01-01',row['gameid'],row['bo'],row['team'])) #str(int(row['year'])-1)+'-01-01'
   result = c.fetchone()

   if result == None:
    # c.execute("""SELECT sum(so)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(h)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(bb)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb))\
    #   FROM batters WHERE bo = %s""",(bo,))
    # d = c.fetchone()
    pass
    # print '1',d
   elif result[0] == None:
    # c.execute("""SELECT sum(so)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(h)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(bb)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb))\
    #   FROM batters WHERE bo = %s""",(bo,))
    # d = c.fetchone()
    # print '2',d
    pass
   else:
    d = result
    X_trim.append(X[k,:])
    y_trim.append(y[k])
    feature_headers.append(row)
    team_batting.append(result)

   k += 1

  X_trim = np.array(X_trim,dtype=float)

  return self.combine_features(X_trim,team_batting),np.array(y_trim,dtype=float)

 def temperature(self,X):

  c = self.db.cursor()
  temp = []

  for row in self.feature_headers:
   c.execute("""SELECT temp\
      FROM batters \
      WHERE gameid = %s""",(row['gameid'],))


   temp.append(c.fetchone())

  return self.combine_features(X,temp)

 def train_predict(self,clf,X,y,cv):
  ## Train model clf, predict probabilities, and determine best threshold

  all_scores = []
  for train, test in cv:
    X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]

    # all_scores.append(mean_squared_error(y_test,X_test[:,-1]))
    # continue

    clf.fit(X_train, y_train)
    # y_pred = np.exp(clf.predict(X_test))
    # y_test = np.exp(y_test)

    y_pred = clf.predict(X_test)
    all_scores.append(mean_squared_error(y_test,y_pred))
    # all_scores.append(mean_absolute_error(y_test,y_pred))

  # print all_scores
  return np.mean(all_scores)

if __name__ == "__main__":

 clf = Pipeline([
 ('scale', preprocessing.StandardScaler()),
 ('classification', Ridge(alpha=1000,max_iter=2000))
 ])

 t = time.time()
 ti= time.time()
 B = batters()
 
 # X = pickle.load(open('../data/train/data_batter_X_6t.p','rb'))
 # y = pickle.load(open('../data/train/data_batter_y_6t.p','rb'))
 # B.feature_headers = pickle.load(open('../data/train/data_batter_fh_6t.p','rb'))

 print 'Loading historical player stats...'
 X,y = B.load_data(30,1000)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print "Accuracy using unweighted fp mean: %.3f"%mean_squared_error(X[:,0],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding previous year...'
 X,y = B.add_batting_year(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding last 7 games...'
 X,y = B.add_batting_days(X,y,7,0)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding last 14 games...'
 X,y = B.add_batting_days(X,y,14,0)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding last 21 games...'
 X,y = B.add_batting_days(X,y,21,0)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding opposing pitcher'
 X,y = B.add_opposing_pitcher(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding batting order...'
 X,y = B.add_bo(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding home/away for predicted game...'
 X,y = B.home_away(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding venue...'
 X,y = B.add_venue(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding venue distribution...'
 X,y = B.add_venue_games(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding temp for day...'
 X = B.temperature(X)
 print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding wind...'
 X,y = B.get_that_wind(X,y)
 print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding last n fp...'
 X = B.historical_fantasy_points(X,y,5)
 print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding missed games out of 10...'
 X,y = B.missed_games(X,y,10)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Adding missed games out of 3...'
 X,y = B.missed_games(X,y,3)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 # B.update_pitcher_clusters(n_clusters=50,min_samples=15,num_batters=300,num_pitchers =200,n_components=50)
 print '*'*40
 startX = X.shape
 print 'Batter vs Cluster...'
 X,y = B.add_cluster(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Games Played...'
 X,y = B.add_games_played_categories(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()

 print '*'*40
 startX = X.shape
 print 'Matchup...'
 X,y = B.add_matchup(X,y)
 print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
 run_cv(clf,B,X,y)
 print 'Old model on new samples...'
 run_cv(clf,B,X[:,:startX[1]],y)
 ti= time.time()
## **************************************************** ##

 # run_grid_search(clf,B,X,y)
 # print '*'*40
 # startX = X.shape
 # print 'Adding player age...'
 # X,y = B.add_age(X,y)
 # print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
 # run_cv(clf,B,X,y)
 # print 'Old model on new samples...'
 # run_cv(clf,B,X[:,:startX[1]],y)
 # ti= time.time()


 print 'Total time: %.2f mins\n'%((time.time()-t)/60.)
 ti= time.time()




 print 'Train data shape:',X.shape
 print "Accuracy using unweighted fp mean: %.3f"%mean_squared_error(X[:,0],y)

 if True:
  pickle.dump(X,open('../../data/train/data_dk_batter_X_20.p','wb'))
  # pickle.dump(B.feature_headers,open('../data/train/data_batter_f_9_50_70.p','wb'))
  pickle.dump(y,open('../../data/train/data_dk_batter_y_20.p','wb'))





