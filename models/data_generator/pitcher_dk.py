import numpy as np
import time,pickle
from datetime import datetime,date,timedelta
import csv, sys, json
from sklearn import preprocessing,decomposition
from sklearn.ensemble import RandomForestRegressor as RF
from sklearn.ensemble import ExtraTreesClassifier,AdaBoostClassifier,GradientBoostingClassifier
from sklearn.cross_validation import StratifiedKFold as KFold
from sklearn import cross_validation
from sklearn.linear_model import LinearRegression,SGDRegressor,ElasticNet
from sklearn.metrics import mean_squared_error,mean_absolute_error
import pylab as plt
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC,SVR
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.decomposition import PCA
from sklearn.tree import DecisionTreeClassifier
import MySQLdb
from scipy.misc import comb

# import warnings
# warnings.filterwarnings("ignore")

class pitchers():

  def __init__(self):
    self.db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
    self.feature_headers = []
    self.transforms = self.initialize_transforms()


  def initialize_transforms(self):
    ## Initialize transformations
    transforms = {}
    # transforms['venues'] = self.venue_encoder()
    transforms = pickle.load(open('../../data/models/transforms_pitcher_0.p','rb'))
    ## Export transformations
    # pickle.dump(transforms,open('../../data/models/transforms_pitcher_0.p','wb'))

    return transforms

  def venue_encoder(self):
    le = preprocessing.LabelBinarizer()
    c = self.db.cursor()
    c.execute("""SELECT DISTINCT(venue_id) from pitchers""")

    venues = []
    for venue in c.fetchall():
      venues.append(venue[0])
    le.fit(venues)

    return le


  def daterange(self,start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

  def combine_features(self,X,xnew):
    xnew = np.array(xnew,dtype=float)
    if len(xnew.shape) == 1:
      xnew = xnew.reshape(xnew.shape[0], 1)
    # print X.shape
    # print xnew.shape
    return np.concatenate((X,xnew),axis=1)

  def load_data(self,min_games=4,max_games=10):
    c = self.db.cursor()
    X = []
    y = []
    c.execute("""SELECT DATE(MIN(gametime)),DATE(MAX(gametime)) from games WHERE year(gametime) IN ('2013','2014','2015')""") #
    days = c.fetchone()

    k = 0
    for aday in self.daterange(days[0],days[1]+timedelta(1)):
    # for aday in self.daterange(days[1]-timedelta(30),days[1]+timedelta(1)):
      print aday
      # \
      c.execute("SELECT today.pid,today.gameid,today.gametime,today.team,today.home,today.away,hist.game_count,\
        ((1./3)*today.outs*2.25 + today.so*2 + today.w*4 - 2*today.er - 0.6*today.h - 0.6*today.bb -.6*today.hb + IF(today.outs=27,2.5,0) + IF(today.outs=27 AND today.r=0,2.5,0) + IF(today.outs=27 AND today.h=0,5,0) ) fp,\
        ((1./3)*hist.sum_outs*2.25 + hist.sum_so*2 + hist.sum_w*4 - 2*hist.sum_er - 0.6*hist.sum_h - 0.6*hist.sum_bb - 0.6*hist.sum_hb + completed*2.5 + shutouts*2.5 + nohitters*5)/ game_count fph,\
        hist.sum_bb/hist.sum_bf,hist.sum_h/hist.sum_bf,hist.sum_np/hist.sum_bf,hist.sum_s/hist.sum_bf,hist.sum_so/hist.sum_bf,hist.sum_outs/game_count,hist.sum_s/hist.sum_np \
        FROM \
        (SELECT pid,sum(so) sum_so,sum(outs) sum_outs,sum(er) sum_er, sum(w) sum_w,sum(bf) sum_bf,sum(h) sum_h,sum(r) sum_r,sum(hr) sum_hr,sum(bb) sum_bb,sum(np) sum_np,sum(s) sum_s,sum(hb) sum_hb,count(pid) game_count,sum(case when outs=27 then 1 else 0 end) as completed,sum(case when (outs=27 and r=0) then 1 else 0 end) as shutouts,sum(case when (outs=27 and h=0) then 1 else 0 end) as nohitters \
          FROM pitchers \
          WHERE gametime < %s and year(gametime) = %s and start = 1 and bf > 0 group by pid having count(pid) >= %s and count(pid) < %s) \
          AS hist,\
        (SELECT gametime,gameid,team,home,away,pid,w,er,so,outs,h,r,bb,hb \
            FROM pitchers \
            WHERE start = 1 AND bf > 0 AND date(gametime) = %s) \
            AS today \
        WHERE hist.pid = today.pid order by today.outs desc;",
        (aday,aday.year,min_games,max_games,aday))
      k = 0
      for d in c.fetchall():
        # print d[0],aday,d[7]
        historical_stats = list(d[8:])

        feature_headers = {}
        feature_headers['pid'] = d[0]
        feature_headers['gameid'] = d[1]
        feature_headers['gametime'] = d[2]
        feature_headers['team'] = d[3]
        feature_headers['game_count'] = d[6]
        feature_headers['year'] = feature_headers['gametime'].year

        # feature_headers = list(d[:4])
        if d[3] == d[4]:
          feature_headers['opp'] = d[5]
        else:
          feature_headers['opp'] = d[4]


        self.feature_headers.append(feature_headers)

        X.append(historical_stats)
        y.append(d[7])
        k += 1

    return np.array(X,dtype=float),np.array(y,dtype=float)


  def add_venue_games(self,X,y):
    c = self.db.cursor()
    venue_games = []
    X_trim = []
    y_trim = []
    feature_headers = []
    k = 0
    for row in self.feature_headers:
      c.execute("SELECT venue_id,count(*)/games_total\
      FROM pitchers,\
      (SELECT count(*) games_total FROM pitchers WHERE pid=%s AND start=1 and bf>0 and year(gametime)=%s and gametime<%s) btotal \
      WHERE pid=%s AND start=1 and bf>0 and year(gametime)=%s and gametime<%s group by venue_id",(row['pid'],row['year'],row['gametime'],row['pid'],row['year'],row['gametime']))
      results = c.fetchall()
      venues = self.transforms['venues'].transform([-1])[0]
      # print venues
      for venue in results:
        # print self.transforms['venues'].transform([venue[0]])[0]*float(venue[1])
        # venues += self.transforms['venues'].transform([venue[0]])[0]*float(venue[1])
        venues = venues + self.transforms['venues'].transform([venue[0]])[0]*float(venue[1])

      venue_games.append(venues)
      k += 1
    return self.combine_features(X,venue_games),np.array(y,dtype=float)

  def home_away(self,X,y):
    c = self.db.cursor()
    homeaway = []
    X_trim = []
    y_trim = []
    feature_headers = []
    k = 0
    for row in self.feature_headers:
      c.execute("SELECT team,home,away FROM batters WHERE gameid = %s and pid = %s",(row['gameid'],row['pid']))
      result = c.fetchone()
      if result[0] == result[1]:
        X_trim.append(X[k,:])
        y_trim.append(y[k])
        homeaway.append(1)
        feature_headers.append(row)
      elif result[0] == result[2]:
        X_trim.append(X[k,:])
        y_trim.append(y[k])
        feature_headers.append(row)
        homeaway.append(0)
      else:
        pass
      k += 1
    X_trim = np.array(X_trim,dtype=float)
    self.feature_headers = feature_headers
    return self.combine_features(X_trim,homeaway),np.array(y_trim,dtype=float)

  def add_venue(self,X,y):
    c = self.db.cursor()
    venues = []
    X_trim = []
    y_trim = []
    feature_headers = []
    k = 0
    for row in self.feature_headers:
      c.execute("SELECT venue_id FROM pitchers WHERE gameid = %s and pid = %s",(row['gameid'],row['pid']))
      result = c.fetchone()
      row['venue'] = result[0]
      venues.append(self.transforms['venues'].transform(result)[0])

    return self.combine_features(X,venues),np.array(y,dtype=float)  

  def pitching_prior_year(self,X):

    c = self.db.cursor()
    pitching = []

    for row in self.feature_headers:
      c.execute("SELECT sum(bb)/sum(bf),sum(h)/sum(bf),sum(np)/sum(bf),sum(s)/sum(bf),sum(so)/sum(bf),sum(r)/sum(bf),sum(s)/sum(np),sum(outs)/count(distinct(gameid)) FROM pitchers WHERE gametime >= %s and gametime < %s and pid = %s",(str(int(row['year'])-1)+'-01-01',row['gametime'],row['pid']))


      pitching.append(c.fetchone())

    return self.combine_features(X,pitching)

  def pitching_prior_games(self,X,n,no):

    c = self.db.cursor()
    pitching = []

    for row in self.feature_headers:
      c.execute("SELECT bb/bf,h/bf,np/bf,s/bf,so/bf,r/bf,s/np,outs FROM pitchers WHERE bf> 0 and start = 1 and gametime < %s and pid = %s order by gametime desc limit %s offset %s",(row['gametime'],row['pid'],n,no))
      result = c.fetchone()
      # print result
      pitching.append(result)

    return self.combine_features(X,pitching)

  def opposing_pitcher(self,X,y):

    p_avg = [0.0744,0.2350,3.7733,2.4001,0.1872,0.1187,0.6361,17.6559]
    c = self.db.cursor()
    pitching = []


    for row in self.feature_headers:
      c.execute("SELECT sum(bb)/sum(bf),sum(h)/sum(bf),sum(np)/sum(bf),sum(s)/sum(bf),sum(so)/sum(bf),sum(r)/sum(bf),sum(s)/sum(np),sum(outs)/count(distinct(gameid)),count(distinct(gameid)) FROM pitchers WHERE gametime >= %s and gametime < %s and pid = (SELECT pid FROM pitchers WHERE gameid=%s and team=%s and start=1) and bf>0 and start=1",(str(int(row['year'])-1)+'-01-01',row['gametime'],row['gameid'],row['opp']))
      result = c.fetchone()
      if result != None:
        if result[0] != None:
          this_pitcher = list(result[:-1])
        else:
          this_pitcher = list(p_avg)
      else:
        this_pitcher = list(p_avg)

      if result[0] == None:
        for k in [1,0,0,0,0,0]:
          this_pitcher.append(k)
      elif result[-1] < 4:
        for k in [0,1,0,0,0,0]:
          this_pitcher.append(k)
      elif result[-1] < 7:
        for k in [0,0,1,0,0,0]:
          this_pitcher.append(k)
      elif result[-1] < 10:
        for k in [0,0,0,1,0,0]:
          this_pitcher.append(k)
      elif result[-1] < 20:
        for k in [0,0,0,0,1,0]:
          this_pitcher.append(k)
      else:
        for k in [0,0,0,0,0,1]:
          this_pitcher.append(k)

      pitching.append(this_pitcher)
    return self.combine_features(X,pitching),y

  def opp_batting(self,X):

    c = self.db.cursor()
    opponent_batting = []

    for row in self.feature_headers:
      # if isopp:
      #   team = row['opp']
      # else:
      #   team = row['team']
      team = row['opp']

      c.execute("""SELECT \
            (sum(h)-sum(d)-sum(t)-sum(hr))/(sum(ab)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)), \
            sum(d)/sum(ab),sum(t)/(sum(ab)),\
            sum(hr)/sum(ab),(sum(h)-sum(hr))/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
            sum(so)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(hbp)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
            sum(sb)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(rbi)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
            sum(r)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(cs)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
            (sum(ab)+sum(hbp)+sum(bb)+sum(sf))/count(pid),\
            sum(go)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),sum(ao)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
            sum(h)/sum(ab),(sum(h)+sum(d)+sum(t)*2+sum(hr)*3)/sum(ab), \
            sum(r)/count(distinct(gameid))
            FROM batters \
            WHERE gametime < %s AND team = %s and YEAR(gametime) = %s""",(row['gametime'],team,row['year']))


      opponent_batting.append(c.fetchone())

    return self.combine_features(X,opponent_batting)

  def team_batting(self,X):

    c = self.db.cursor()
    opponent_batting = []

    for row in self.feature_headers:

      team = row['team']

      c.execute("""SELECT \
            (sum(h)-sum(d)-sum(t)-sum(hr))/(sum(ab)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)), \
            sum(d)/sum(ab),sum(t)/(sum(ab)),\
            sum(hr)/sum(ab),(sum(h)-sum(hr))/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
            sum(so)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(hbp)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
            sum(sb)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(rbi)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
            sum(r)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(cs)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
            (sum(ab)+sum(hbp)+sum(bb)+sum(sf))/count(pid),\
            sum(go)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),sum(ao)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
            sum(h)/sum(ab),(sum(h)+sum(d)+sum(t)*2+sum(hr)*3)/sum(ab), \
            sum(r)/count(distinct(gameid))
            FROM batters \
            WHERE gametime < %s AND team = %s and YEAR(gametime) = %s""",(row['gametime'],team,row['year']))


      opponent_batting.append(c.fetchone())

    return self.combine_features(X,opponent_batting)

  def temperature(self,X):

    c = self.db.cursor()
    temp = []

    for row in self.feature_headers:
      c.execute("""SELECT temp\
            FROM batters \
            WHERE gameid = %s""",(row['gameid'],))


      temp.append(c.fetchone())

    return self.combine_features(X,temp)

  def batters_from_lineup(self,X):

    c = self.db.cursor()
    
    opponent_batting = []



    for row in self.feature_headers:
      # print row
      c.execute("""SELECT sum(so)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(h)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(bb)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb))\
            FROM batters \
            WHERE gametime < %s AND gametime >= %s AND pid IN (SELECT pid FROM batters WHERE gameid=%s AND start = 1 AND team = %s)""",(row['gametime'],str(int(row['year'])-1)+'-01-01',row['gameid'],row['opp']))
      result = c.fetchone()

      if result == None:
        c.execute("""SELECT sum(so)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(h)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(bb)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb))\
            FROM batters""")
        d = c.fetchone()
        print '1',d
      elif result[0] == None:
        c.execute("""SELECT sum(so)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(h)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb)),sum(bb)/(sum(ab)+sum(hbp)+sum(sf)+sum(bb))\
            FROM batters""")
        d = c.fetchone()
        print '2',d
      else:
        d = result
        # print '3',d

      opponent_batting.append(d)

    return self.combine_features(X,opponent_batting)

  def add_games_played_categories(self,X,y):
    c = self.db.cursor()
    gp_all = []
    for row in self.feature_headers:
      gp = [0,0,0,0,0]
      if row['game_count'] < 7:
        gp[0] = 1
      elif row['game_count'] < 10:
        gp[1] = 1
      elif row['game_count'] < 15:
        gp[2] = 1
      elif row['game_count'] < 20:
        gp[3] = 1
      else:
        gp[4] = 1
      gp_all.append(gp)
      # print row['game_count']
      # print gp

    return self.combine_features(X,gp_all),np.array(y,dtype=float) 

  def get_that_wind(self,X,y):
    c = self.db.cursor()
    winds= []
    X_trim = []
    y_trim = []
    feature_headers = []

    windI = {'Out to CF':0,'L to R':1,'In from LF':2,'In from CF':3,'In from RF':4,'Out to RF':5,'Out to LF':6,'R to L':7}

    k = 0
    for row in self.feature_headers:
      c.execute("SELECT wind_dir,wind_speed FROM pitchers WHERE gameid = %s and pid = %s",(row['gameid'],row['pid']))
      result = c.fetchone()
      if result != None:
        wind = [0,0,0,0,0,0,0,0]
        # if windI.has_key(result[0]) and row['venue'] in [3,7,10,13,15,16,17,22,680,2681,3289,3309]:
        try:
          if row['venue'] not in [4169,12,15,32,2392,14]:
            wind[windI[result[0]]] = int(result[1])
        except:
          pass
        X_trim.append(X[k,:])
        y_trim.append(y[k])
        feature_headers.append(row)
        winds.append(wind)
      k += 1
    X_trim = np.array(X_trim,dtype=float)
    self.feature_headers = feature_headers
    return self.combine_features(X_trim,winds),np.array(y_trim,dtype=float)

  def lineup_vs_pitcher(self,X,y):
    c = self.db.cursor()
    
    opponent_batting = []

    lvp_avg = [0.2625, 0.2973, 0.0068, 0.0058, 0.0544, 0.0326, 0.0792, 0.4320, 0.2682, 0.1754, 0.1149, 0.1797, 0.1097]

    for row in self.feature_headers:
      c.execute("""SELECT sum(ao)/sum(ab),sum(go)/sum(ab),sum(cs)/sum(tpa),sum(t)/sum(ab),sum(d)/sum(ab),sum(hr)/sum(ab),sum(bb)/sum(tpa),sum(tb)/sum(ab),sum(h)/sum(ab),(sum(h)-sum(d)-sum(t)-sum(hr))/sum(ab),sum(r)/sum(tpa),sum(so)/sum(tpa),sum(rbi)/sum(tpa),sum(ab) \
        from battervspitcher WHERE season < %s and pitchid = %s and batid IN (SELECT pid FROM batters WHERE gameid=%s AND start = 1 AND team = %s)""",(row['year'],row['pid'],row['gameid'],row['opp']))
      r = c.fetchone()
      if r[0] == None:
        b = list(lvp_avg)
      else:
        b = list(r[:-1])

      if r[0] == None:
        for k in [1,0,0,0,0,0]:
          b.append(k)
      elif (r[-1] < 4):
        for k in [0,1,0,0,0,0]:
          b.append(k)
      elif (r[-1] < 7):
        for k in [0,0,1,0,0,0]:
          b.append(k)
      elif (r[-1] < 10):
        for k in [0,0,0,1,0,0]:
          b.append(k) 
      elif (r[-1] < 20):
        for k in [0,0,0,0,1,0]:
          b.append(k)                       
      elif (r[-1] >=20):
        for k in [0,0,0,0,0,1]:
          b.append(k)    

      opponent_batting.append(b)
    
    return self.combine_features(X,opponent_batting),y

  def train_predict(self,clf,X,y,cv):
    ## Train model clf, predict probabilities, and determine best threshold

    all_scores = []
    for train, test in cv:
        X_train, X_test, y_train, y_test = X[train,:], X[test,:], y[train], y[test]

        # all_scores.append(mean_squared_error(y_test,X_test[:,-1]))
        # continue

        clf.fit(X_train, y_train)
        # y_pred = np.exp(clf.predict(X_test))
        # y_test = np.exp(y_test)

        y_pred = clf.predict(X_test)
        all_scores.append(mean_squared_error(y_test,y_pred))
        # all_scores.append(mean_absolute_error(y_test,y_pred))

    # print all_scores
    return np.mean(all_scores)


def run_cv(clf,P,X,y):
  cv = cross_validation.ShuffleSplit(X.shape[0], n_iter=5,test_size=0.2,random_state=18)
  score = P.train_predict(clf,X,y,cv)
  print X.shape
  print 'Accuracy score: %.3f\n'%(score)
  return


if __name__ == "__main__":

  clf = Pipeline([
  ('scale', preprocessing.StandardScaler()),
  ('classification', ElasticNet(l1_ratio=.5,alpha=.11,max_iter=10000))
  ])

  t = time.time()
  ti= time.time()
  P = pitchers()

  print 'Loading historical player stats...'
  X,y = P.load_data(4,1000)
  print 'Finished after %.2f mins\n'%((time.time()-t)/60.)
  run_cv(clf,P,X,y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding home/away for predicted game...'
  X,y = P.home_away(X,y)
  print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding venue...'
  X,y = P.add_venue(X,y)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding venue distribution...'
  X,y = P.add_venue_games(X,y)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding pitching prior year...'
  X = P.pitching_prior_year(X)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding pitching last 1 games...'
  X = P.pitching_prior_games(X,1,0)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding pitching last 2 games...'
  X = P.pitching_prior_games(X,1,1)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding pitching last 3 games...'
  X = P.pitching_prior_games(X,1,2)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding opposing pitcher...'
  X,y = P.opposing_pitcher(X,y)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding opponent batting for season...'
  X = P.opp_batting(X)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding team batting for season...'
  X = P.team_batting(X)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding temp for day...'
  X = P.temperature(X)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding wind...'
  X,y = P.get_that_wind(X,y)
  print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Games Played...'
  X,y = P.add_games_played_categories(X,y)
  print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

  print '*'*40
  startX = X.shape
  print 'Adding lineup vs pitcher...'
  X,y = P.lineup_vs_pitcher(X,y)
  print 'Finished after %.3f mins\n'%((time.time()-ti)/60.)
  run_cv(clf,P,X,y)
  print 'Old model on new samples...'
  run_cv(clf,P,X[:,:startX[1]],y)
  ti= time.time()

#################################################################################

  # print '*'*40
  # startX = X.shape
  # print 'Adding batters from lineup...'
  # X = P.batters_from_lineup(X)
  # print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  # run_cv(clf,P,X,y)
  # print 'Old model on new samples...'
  # run_cv(clf,P,X[:,:startX[1]],y)
  # ti= time.time()

  # print '*'*40
  # startX = X.shape
  # print 'Adding team batting for season...'
  # X = P.team_batting(X)
  # print 'Finished after %.2f mins\n'%((time.time()-ti)/60.)
  # run_cv(clf,P,X,y)
  # print 'Old model on new samples...'
  # run_cv(clf,P,X[:,:startX[1]],y)
  # ti= time.time()

  print 'Total time: %.2f mins\n'%((time.time()-t)/60.)
  ti= time.time()




  print 'Train data shape:',X.shape
  print "Accuracy using unweighted fp mean: %.2f"%mean_squared_error(X[:,0],y)
  if True:
    pickle.dump(X,open('../../data/train/data_dk_pitcher_X_21.p','wb'))
    pickle.dump(y,open('../../data/train/data_dk_pitcher_y_21.p','wb'))





