# -*- coding: utf-8 -*-

# Scrapy settings for baseballpress project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'baseballpress'

SPIDER_MODULES = ['baseballpress.spiders']
NEWSPIDER_MODULE = 'baseballpress.spiders'

ITEM_PIPELINES = [
    'baseballpress.pipelines.BaseballpressPipeline',
]
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'baseballpress (+http://www.yourdomain.com)'
