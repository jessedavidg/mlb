# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BaseballpressItem(scrapy.Item):
    # define the fields for your item here like:
    pid = scrapy.Field()
    pitcherid = scrapy.Field()
    team = scrapy.Field()
    bo = scrapy.Field()
    gameid = scrapy.Field()
    pitchers = scrapy.Field()
    platform = scrapy.Field()
    pass
