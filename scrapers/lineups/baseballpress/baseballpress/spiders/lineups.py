# -*- coding: utf-8 -*-
import scrapy
from baseballpress.items import BaseballpressItem
import time
from dateutil import parser

class LineupsSpider(scrapy.Spider):
	name = "lineups"
	def __init__(self, gameid=12453,starttime='', endtime='', platform=''):
		self.gameid = gameid
		self.allowed_domains = ["http://www.baseballpress.com/lineups"]
		self.start_urls = ('http://www.baseballpress.com/lineups',)
		self.platform = platform
		if starttime == '':
			self.starttime = ''
		else:
			self.starttime = parser.parse(starttime)
		if endtime == '':
			self.endtime = ''
		else:
			self.endtime = parser.parse(endtime)

	def parse(self, response):
		item = BaseballpressItem()
		item['pitchers'] = {}
		item['gameid'] = self.gameid
		item['platform'] = self.platform
		# for sel in response.xpath("//div[@class='header']"):
			# print sel.extract()
		for sel in response.xpath("//div[@class='game clearfix']"):
			try:
				gametime_str = sel.xpath(".//div[@class='game-time']/text()").extract()[0]
			except:
				continue
			print gametime_str
			

			gametime = parser.parse(gametime_str)
			if self.starttime != '':
				if (gametime < self.starttime):
					print 'START EXITING',gametime,self.starttime
					continue
			if self.endtime != '':
				if (gametime > self.endtime):
					print 'END EXITING',gametime,self.endtime
					continue


			print 'NOT EXITING',gametime
			
			item['pitchers'] = {}
			d = {}
			for t in sel.xpath('.//div[@class="team-data"]'):
				team = t.xpath('a/@href').extract()[0].split('/')[-1]
				d[team] = {}
				d[team]['batters'] = False
				for p in t.xpath('.//a[@class="player-link"]'):
					d[team]['pitcher'] = p.xpath('@data-mlb').extract()[0]
			for t in sel.xpath(".//div[@class='team-lineup clearfix']"):
				team = t.xpath('a/@href').extract()[0].split('/')[-1]
				print team
				# print '!!!!!!'
				for b in t.xpath('.//div[@class="players"]/div'):
					d[team]['batters'] = True
					item['pid'] = b.xpath('a/@data-mlb').extract()[0]
					for pteam in d:
						if team != pteam:
							item['pitcherid'] = d[pteam]['pitcher']
					item['bo'] = int(b.xpath('text()').extract()[0].strip().replace('.',''))
					item['team'] = team
					yield item
			
			print 'defining pitchers'
			print '*'*20
			item['pitchers'] = dict(d)
			yield item
