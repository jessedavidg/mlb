# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import MySQLdb

class BaseballpressPipeline(object):
	db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")

	def process_item(self, item, spider):
		# print 'PROCESSING'
		c = self.db.cursor()

		if item['pitchers'] != {}:
			
			for team in item['pitchers']:
				if item['platform'] == 'fanduel':
					## FANDUEL
					c.execute("""UPDATE fanduel_contests SET pitcher = %s WHERE gameid = %s and opp = (SELECT fanduel FROM venues WHERE baseballpress = %s)""",\
					(item['pitchers'][team]['pitcher'],item['gameid'],team))

					if item['pitchers'][team]['batters']:
						c.execute("""UPDATE fanduel_contests SET bo = 0 WHERE bo IS NULL and gameid = %s and team = (SELECT fanduel FROM venues WHERE baseballpress = %s)""",\
						(item['gameid'],team))

				elif item['platform'] == 'draftkings':
					## DRAFT KINGS
					c.execute("""UPDATE draftkings_contests SET pitcher = %s WHERE gameid = %s and opp = (SELECT draftkings FROM venues WHERE baseballpress = %s)""",\
					(item['pitchers'][team]['pitcher'],item['gameid'],team))

					if item['pitchers'][team]['batters']:
						c.execute("""UPDATE draftkings_contests SET bo = 0 WHERE bo IS NULL and gameid = %s and team = (SELECT draftkings FROM venues WHERE baseballpress = %s)""",\
						(item['gameid'],team))


		else:

			## Dump data

			if item['platform'] == 'fanduel':
				c.execute("""UPDATE fanduel_contests SET bo = %s,pitcher = %s WHERE gameid = %s and fid = (SELECT fid FROM players WHERE pid = %s)""",\
				(item['bo'],item['pitcherid'],item['gameid'],item['pid']))
			elif item['platform'] == 'draftkings':
				c.execute("""UPDATE draftkings_contests SET bo = %s,pitcher = %s WHERE gameid = %s and dkid = (SELECT dkid FROM players WHERE pid = %s)""",\
				(item['bo'],item['pitcherid'],item['gameid'],item['pid']))	
		
		self.db.commit()

		return item
