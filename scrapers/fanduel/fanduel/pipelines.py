# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import MySQLdb,csv
from datetime import date, datetime
from cbbplayer import Player


class FanduelPipeline(object):

  db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")


  def process_item(self, item, spider):
    today = date.today()

    c = self.db.cursor()
    print item

    try:
    	gametime = item['contestday'].strftime('%Y %m %d') +'T'+ item['games'][item['team']]
    	temp = item['weather'][item['team']]['temp']
        windSpeed = item['weather'][item['team']]['windSpeed']
        windBearing = item['weather'][item['team']]['windBearing']
    except:
    	gametime = item['contestday'].strftime('%Y %m %d') + 'T'+item['games'][item['opp']]
    	temp = item['weather'][item['opp']]['temp']
        windSpeed = item['weather'][item['opp']]['windSpeed']
        windBearing = item['weather'][item['opp']]['windBearing']

    gametime = datetime.strptime(gametime,'%Y %m %dT%I:%M%p')

    ## Dump data
    c.execute("""INSERT IGNORE INTO fanduel_contests (gameid,contestday,fid,fppg,team,opp,home,indicator,name,pos,salary,gametime,temp,wind_speed,wind_dir) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",\
              (item['gameid'],item['contestday'],item['fid'],item['fppg'],item['team'],item['opp'],item['home'],item['indicator'],item['name'],item['position'],item['salary'],gametime,temp,windSpeed,windBearing))

    c.execute("""UPDATE fanduel_contests SET temp=%s,wind_speed=%s,wind_dir=%s WHERE gameid=%s and fid=%s""",\
              (temp,windSpeed,windBearing,item['gameid'],item['fid']))    
    self.db.commit()