# -*- coding: utf-8 -*-

# Scrapy settings for hbp project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'hbp'

SPIDER_MODULES = ['hbp.spiders']
NEWSPIDER_MODULE = 'hbp.spiders'

ITEM_PIPELINES = [
    'hbp.pipelines.HbpPipeline',
]

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'hbp (+http://www.yourdomain.com)'
