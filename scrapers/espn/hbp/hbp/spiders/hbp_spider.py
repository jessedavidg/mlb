import scrapy,re,sys
from hbp.items import HbpItem
from datetime import datetime,date,timedelta
import MySQLdb
from collections import defaultdict

class hbpSpider(scrapy.Spider):
	name = "hbp"
	allowed_domains = ["scores.espn.go.com"]

	db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
	c = db.cursor()
	c.execute("""SELECT distinct(gameid) from batters where ab is null""")

	start_urls = ["http://scores.espn.go.com/mlb/playbyplay?gameId=" + game[0] for game in c.fetchall()]
	# start_urls = ["http://scores.espn.go.com/mlb/playbyplay?gameId=350310107"]

	def parse(self, response):
		print '* '*50
		item = HbpItem()
		item['gameid'] = response.url.split('=')[-1]
		item['hbp'] = defaultdict(int)
		for row in ['even','odd']:
			for sel in response.xpath("//tr[@class='%s']"%row):
				# print sel.xpath("td/text()").extract()[0]
				text = sel.xpath("td/text()").extract()[0]
				hitbypitch = re.search(r'.+? hit by pitch',text)
				if hitbypitch:
					item['hbp'][text.split('hit by pitch')[0].strip()] += 1
		yield item

