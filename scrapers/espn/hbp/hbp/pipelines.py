# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import MySQLdb

class HbpPipeline(object):

	db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")

	def process_item(self, item, spider):

		c = self.db.cursor()

		for key in item['hbp']:

			c.execute("""SELECT name FROM batters WHERE gameid=%s and name=%s""",
				(item['gameid'],key))

			results = c.fetchall()

			if len(results) == 0:
				print 'NO RESULTS',item['hbp'][key],item['gameid'],key
			elif len(results) > 1:
				print 'MULTIPLE RESULTS',item['hbp'][key],item['gameid'],key
			else:
				print results[0],key

				c.execute("""UPDATE batters SET hbp=%s WHERE gameid=%s and name=%s""",
				(item['hbp'][key],item['gameid'],key)
				)
		self.db.commit()
		return item
