# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class PlayersItem(scrapy.Item):
	# define the fields for your item here like:
	# name = scrapy.Field()

	# Game
	hometeam = scrapy.Field()
	awayteam = scrapy.Field()
	gameid = scrapy.Field()
	gametime = scrapy.Field()
	temperature = scrapy.Field()
	wind = scrapy.Field()
	weather = scrapy.Field()
	hpu = scrapy.Field()


	# Player
	playerid = scrapy.Field()
	playerteam = scrapy.Field()
	playername = scrapy.Field()
	position = scrapy.Field()

	# Batter
	batting = scrapy.Field()
	pitches = scrapy.Field()

	# Pitcher
	pc = scrapy.Field()
	st = scrapy.Field()
	order = scrapy.Field()
	pass
