# -*- coding: utf-8 -*-

# Scrapy settings for players project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'players'

SPIDER_MODULES = ['players.spiders']
NEWSPIDER_MODULE = 'players.spiders'

ITEM_PIPELINES = [
    'players.pipelines.PlayersPipeline',
]

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'players (+http://www.yourdomain.com)'
