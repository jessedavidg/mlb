import scrapy,re,sys
from players.items import PlayersItem
from datetime import datetime,date,timedelta
import MySQLdb

class cbbdataSpider(scrapy.Spider):
    name = "players"
    allowed_domains = ["espn.go.com"]

    db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
    c = db.cursor()
    c.execute("""SELECT gameid from games WHERE home IS NULL""")

    start_urls = ["http://espn.go.com/mlb/boxscore?id=" + game[0] for game in c.fetchall()]
    # start_urls = ["http://espn.go.com/mlb/boxscore?id=330330108"]
    # print start_urls

    def parse(self, response):
        print '* '*50

        item = PlayersItem()
        for sel in response.xpath("//div[@class='team away']"):
            item['awayteam'] = sel.xpath(".//a/@href").extract()[0].split('/')[-2]
            # item['awayteamname'] = sel.xpath(".//a/text()").extract()[0]
        for sel in response.xpath("//div[@class='team home']"):
            item['hometeam'] = sel.xpath(".//a/@href").extract()[0].split('/')[-2]
            # item['hometeamname'] = sel.xpath(".//a/text()").extract()[0]
        # print response.xpath("//div[@class='game-time-location']").extract()
        datestr = response.xpath("//div[@class='game-time-location']/p/text()").extract()[0]
        if datestr[1] == ':':
            datestr = '0' + datestr
        if datestr[-8] == ' ':
            datestr = datestr.replace(datestr[-7:],'0' + datestr[-7:])
        datestr = datestr.replace('ET,','')
        item['gametime'] = datetime.strptime(datestr,'%I:%M %p  %B %d, %Y')
        item['gameid'] = response.url.split('=')[-1]
        # print item

        tablenum = 1

        for sel in response.xpath("//div[@class='mod-container mod-open mod-open-gamepack']"):
            if sel.xpath("div/h4/text()").extract()[0] == 'Game Information':
                for row in sel.xpath(".//tbody/tr"):
                    category = row.xpath("td/strong/text()").extract()[0]
                    if category == 'Weather':
                        weather = row.xpath("td[position()=2]/text()").extract()[0]
                        if weather == 'indoors':
                            item['temperature'] = 75
                            item['wind'] = 0
                            item['weather'] = 'indoors'
                        else:
                            weather = weather.split(',')
                            item['temperature'] = weather[0].split()[0]
                            item['weather'] = weather[1].strip()
                    elif category == 'Wind':
                        wind = row.xpath("td[position()=2]/text()").extract()[0]
                        item['wind'] = wind.split()[0]
                    elif category == 'Umpires':
                        umpires = row.xpath("td[position()=2]/text()").extract()[0]
                        u = re.search(r'Home Plate - .+?,',umpires)
                        if u:
                            item['hpu'] = u.group(0).replace(',','').split('-')[-1].strip()
                        else:
                            item['hpu'] = 'None'
                break

        for sel in response.xpath("//table[@class='mod-data mlb-box']"):
            lineupspot = 1
            print 'l',tablenum


            if tablenum == 1: ## away batting
                item['playerteam'] = item['awayteam']
                for row in sel.xpath("tbody[position()=1]/tr"):
                    item['position'] = 'B'
                    item['pitches'] = row.xpath("td[position()=8]/text()").extract()[0].strip()
                    
                    indent = row.xpath(".//div").extract()
                    print lineupspot,indent
                    
                    if indent:
                        pid = row.xpath("td/div/a/@href").extract()
                        if len(pid) == 0:
                            continue
                        else:
                            item['playername'] = row.xpath("td/div/a/text()").extract()[0].strip()
                            item['playerid']= row.xpath("td/div/a/@href").extract()[0].split('/')[-2]
                        item['batting'] = 0
                    else:
                        pid = row.xpath("td/a/@href").extract()
                        if len(pid) == 0:
                            lineupspot += 1
                            continue
                        else:
                            item['playerid']= row.xpath("td/a/@href").extract()[0].split('/')[-2]
                        item['playername'] = row.xpath("td/a/text()").extract()[0].strip()
                        item['batting'] = lineupspot
                        lineupspot += 1

                    yield item

            elif tablenum == 3: ## home batting
                item['playerteam'] = item['hometeam']
                for row in sel.xpath("tbody[position()=1]/tr"):
                    item['position'] = 'B'
                    item['pitches'] = row.xpath("td[position()=8]/text()").extract()[0].strip()
                    
                    indent = row.xpath(".//div").extract()
                    print lineupspot,indent
                    
                    if indent:
                        pid = row.xpath("td/div/a/@href").extract()
                        if len(pid) == 0:
                            continue
                        else:
                            item['playername'] = row.xpath("td/div/a/text()").extract()[0].strip()
                            item['playerid']= row.xpath("td/div/a/@href").extract()[0].split('/')[-2]
                        item['batting'] = 0
                    else:
                        pid = row.xpath("td/a/@href").extract()
                        if len(pid) == 0:
                            lineupspot += 1
                            continue
                        else:
                            item['playerid']= row.xpath("td/a/@href").extract()[0].split('/')[-2]
                        item['playername'] = row.xpath("td/a/text()").extract()[0].strip()
                        item['batting'] = lineupspot
                        lineupspot += 1
                    yield item
                    

            elif tablenum == 2: ## home pitching
                item['playerteam'] = item['awayteam']
                order = 1
                for row in sel.xpath("tbody[position()=1]/tr"):
                    item['position'] = 'P'
                    pid = row.xpath("td/a/@href").extract()
                    if len(pid) == 0:
                        order += 1
                        continue
                    else:
                        item['playerid'] = pid[0].split('/')[-2]
                    item['playername'] = row.xpath("td/a/text()").extract()[0].strip()
                    item['pc'] = row.xpath("td[position()=9]/text()").extract()[0].split('-')[0]
                    item['st'] = row.xpath("td[position()=9]/text()").extract()[0].split('-')[1]
                    item['order'] = order
                    order += 1

                    yield item

            elif tablenum == 4: ## away pitching
                item['playerteam'] = item['hometeam']
                order = 1
                for row in sel.xpath("tbody[position()=1]/tr"):
                    item['position'] = 'P'
                    pid = row.xpath("td/a/@href").extract()
                    if len(pid) == 0:
                        order += 1
                        continue
                    else:
                        item['playerid'] = pid[0].split('/')[-2]
                    item['playername'] = row.xpath("td/a/text()").extract()[0].strip()
                    item['pc'] = row.xpath("td[position()=9]/text()").extract()[0].split('-')[0]
                    item['st'] = row.xpath("td[position()=9]/text()").extract()[0].split('-')[1]                    
                    item['order'] = order
                    order += 1
                
                    yield item                
            tablenum += 1


