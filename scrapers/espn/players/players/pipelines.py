# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import MySQLdb

class PlayersPipeline(object):

	db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")

	def process_item(self, item, spider):
		c = self.db.cursor()


		c.execute("""UPDATE games SET home=%s,away=%s,gametime=%s,temp=%s,wind=%s,weather=%s,hpu=%s WHERE gameid=%s""",
		(item['hometeam'],item['awayteam'],item['gametime'],item['temperature'],item['wind'],item['weather'],item['hpu'],item['gameid'])
		)


		if item['position'] == 'B':
			c.execute("""INSERT IGNORE INTO batters (pid,gameid,name,team,home,away,gametime,temp,wind,weather,hpu,pitches,`order`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
			(item['playerid'],item['gameid'],item['playername'],item['playerteam'],item['hometeam'],item['awayteam'],item['gametime'],item['temperature'],\
				item['wind'],item['weather'],item['hpu'],item['pitches'],item['batting'])
			)
		elif item['position'] == 'P':
			c.execute("""INSERT IGNORE INTO pitchers (pid,gameid,name,team,home,away,gametime,temp,wind,weather,hpu,pc,st,`order`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
			(item['playerid'],item['gameid'],item['playername'],item['playerteam'],item['hometeam'],item['awayteam'],item['gametime'],item['temperature'],\
				item['wind'],item['weather'],item['hpu'],item['pc'],item['st'],item['order'])
			)

		self.db.commit()

		return item
