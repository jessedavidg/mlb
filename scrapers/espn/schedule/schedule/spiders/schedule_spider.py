import scrapy,re,sys
from schedule.items import ScheduleItem
from datetime import datetime,date,timedelta
import MySQLdb

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

class cbbgamesSpider(scrapy.Spider):
    name = "schedule"
    allowed_domains = ["espn.go.com"]


    db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
    c = db.cursor()
    c.execute("""SELECT MAX(gameid) from games""")

    result = c.fetchone()
    
    if True:
      startdate = date(2012,3,28)
      stopdate = date(2012,10,28)
    else:
      startdate = result[0].date() + timedelta(1)
      stopdate = date.today()

    start_urls = []

    for adate in daterange(startdate,stopdate):
      start_urls.append("http://espn.go.com/mlb/schedule?date="+adate.strftime('%Y%m%d'))

    def parse(self, response):
      print '* '*50
      item = ScheduleItem()
      
      for sel in response.xpath("//tbody"):
        print sel
        links = sel.xpath(".//td/a[contains(@href, 'recap?gameId')]/@href").extract()
        for link in links:
          item['gameid'] = link.split('=')[-1]
          print item['gameid']
          yield item

      # for sel in response.xpath("//tr[contains(@class, 'oddrow')]"):
      #   links = sel.xpath("td/a[contains(@href, 'recap?gameId')]/@href").extract()
      #   for link in links:
      #     item['id'] = link.split('=')[-1]
      #     yield item
