# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import re,sys,datetime
import MySQLdb

class SchedulePipeline(object):

	db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")

	def process_item(self, item, spider):
		c = self.db.cursor()
		c.execute("""INSERT IGNORE INTO games (gameid) VALUES (%s)""",
			(item['gameid'],)
			)
		self.db.commit()
		return item
