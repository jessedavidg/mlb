import scrapy,re,sys
from batstats.items import BatstatsItem
from datetime import datetime,date,timedelta
import MySQLdb
from collections import defaultdict

class hbpSpider(scrapy.Spider):
	name = "batstats"
	allowed_domains = ["scores.espn.go.com"]

	db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
	c = db.cursor()
	# c.execute("""SELECT distinct(pid) from batters""")
	start_urls = []

	for year in ['2012','2013','2014']:
		c.execute("""SELECT distinct(pid) from pitchers WHERE decision IS NULL and YEAR(gametime) = %s"""%(year))
		for game in c.fetchall():
			start_urls.append("http://espn.go.com/mlb/player/gamelog/_/id/%s/year/%s"%(game[0],year))

		c.execute("""SELECT distinct(pid) from batters WHERE ab IS NULL and YEAR(gametime) = %s"""%(year))
		for game in c.fetchall():
			start_urls.append("http://espn.go.com/mlb/player/gamelog/_/id/%s/year/%s"%(game[0],year))

	# start_urls = ["http://espn.go.com/mlb/player/gamelog/_/id/28563/year/2014"]

	def parse(self, response):
		print '* '*50
		item = BatstatsItem()
		item['playerid'] = response.url.split('/')[-3]

		# Determine if its a pitcher or batter
		for sel in response.xpath("//tr[@class='colhead']"):
			if sel.xpath("td[position()=4]/text()").extract()[0] == 'IP':
				item['pos'] = 'P'
			else:
				item['pos'] = 'B'
			break
		
		if item['pos'] == 'B':
			for row in ['evenrow','oddrow']:
				for sel in response.xpath("//tr[@class='%s']"%row):
					if len(sel.xpath("td[position()=4]/text()").extract()) == 0:
						continue
					if sel.xpath("td[position()=4]/text()").extract()[0] != 'Did not play':
						item['gameid'] = sel.xpath("td[position()=3]/a/@href").extract()[0].split('=')[-1]
						item['ab'] = sel.xpath("td[position()=4]/text()").extract()[0]
						item['r'] = sel.xpath("td[position()=5]/text()").extract()[0]
						item['h'] = sel.xpath("td[position()=6]/text()").extract()[0]
						item['twob'] = sel.xpath("td[position()=7]/text()").extract()[0]
						item['threeb'] = sel.xpath("td[position()=8]/text()").extract()[0]
						item['hr'] = sel.xpath("td[position()=9]/text()").extract()[0]
						item['rbi'] = sel.xpath("td[position()=10]/text()").extract()[0]
						item['bb'] = sel.xpath("td[position()=11]/text()").extract()[0]
						item['so'] = sel.xpath("td[position()=12]/text()").extract()[0]
						item['sb'] = sel.xpath("td[position()=13]/text()").extract()[0]
						item['cs'] = sel.xpath("td[position()=14]/text()").extract()[0]
						yield item
		else:
			for row in ['evenrow','oddrow']:
				for sel in response.xpath("//tr[@class='%s']"%row):
					if len(sel.xpath("td[position()=4]/text()").extract()) == 0:
						continue
					if sel.xpath("td[position()=4]/text()").extract()[0] != 'Did not play':
						item['gameid'] = sel.xpath("td[position()=3]/a/@href").extract()[0].split('=')[-1]
						item['ip'] = sel.xpath("td[position()=4]/text()").extract()[0]
						item['h'] = sel.xpath("td[position()=5]/text()").extract()[0]
						item['r'] = sel.xpath("td[position()=6]/text()").extract()[0]
						item['er'] = sel.xpath("td[position()=7]/text()").extract()[0]
						item['hr'] = sel.xpath("td[position()=8]/text()").extract()[0]
						item['bb'] = sel.xpath("td[position()=9]/text()").extract()[0]
						item['so'] = sel.xpath("td[position()=10]/text()").extract()[0]
						item['gb'] = sel.xpath("td[position()=11]/text()").extract()[0]
						item['fb'] = sel.xpath("td[position()=12]/text()").extract()[0]
						item['pit'] = sel.xpath("td[position()=13]/text()").extract()[0]
						item['bf'] = sel.xpath("td[position()=14]/text()").extract()[0]
						item['w'] = sel.xpath("td[position()=16]/text()").extract()[0].strip()[0].upper()
						if item['w'] == '-':
							item['w'] = 'N'
						yield item


