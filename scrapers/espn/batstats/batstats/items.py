# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BatstatsItem(scrapy.Item):
    # define the fields for your item here like:
    pos = scrapy.Field()
    gameid = scrapy.Field()
    playerid = scrapy.Field()

    ## Batter
    ab = scrapy.Field()
    r = scrapy.Field()
    h = scrapy.Field()
    twob = scrapy.Field()
    threeb = scrapy.Field()
    hr = scrapy.Field()
    rbi = scrapy.Field()
    bb = scrapy.Field()
    so = scrapy.Field()
    sb = scrapy.Field()
    cs = scrapy.Field()

    ## Pitcher
    ip = scrapy.Field()
    er = scrapy.Field()
    hr = scrapy.Field()
    bb = scrapy.Field()
    so = scrapy.Field()
    gb = scrapy.Field()
    fb = scrapy.Field()
    pit = scrapy.Field()
    bf = scrapy.Field()
    w = scrapy.Field()
    pass
