# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import MySQLdb

class BatstatsPipeline(object):
	db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
	
	def process_item(self, item, spider):
		c = self.db.cursor()


		if item['pos'] == 'B':
			c.execute("""UPDATE batters SET ab=%s,r=%s,h=%s,doub=%s,trip=%s,hr=%s,rbi=%s,bb=%s,so=%s,sb=%s,cs=%s WHERE pid=%s AND gameid=%s""",
			(item['ab'],item['r'],item['h'],item['twob'],item['threeb'],item['hr'],item['rbi'],item['bb'],\
				item['so'],item['sb'],item['cs'],item['playerid'],item['gameid'])
			)
		elif item['pos'] == 'P':
			# print """UPDATE pitchers SET ip=%s,r=%s,h=%s,er=%s,hr=%s,bb=%s, so=%s,gb=%s,fb=%s,pit=%s,bf=%s,dec=%s WHERE pid=%s AND gameid=%s"""%(item['ip'],item['r'],item['h'],item['er'],item['hr'],item['bb'],item['so'],item['gb'],item['fb'],item['pit'],item['bf'],item['w'],item['playerid'],item['gameid'])
			c.execute("""UPDATE pitchers SET ip=%s,r=%s,h=%s,er=%s,hr=%s,bb=%s, so=%s,gb=%s,fb=%s,pit=%s,bf=%s,decision=%s WHERE pid=%s AND gameid=%s""",
			(item['ip'],item['r'],item['h'],item['er'],item['hr'],item['bb'],item['so'],item['gb'],item['fb'],\
				item['pit'],item['bf'],item['w'],item['playerid'],item['gameid'])
			)

		self.db.commit()

		return item
