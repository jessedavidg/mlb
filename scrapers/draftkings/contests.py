import urllib2, json, sys, re
from datetime import datetime,date,timedelta
import xml.etree.ElementTree as ET
import MySQLdb

db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
c = db.cursor()

def get_weather(home_venues,gametime,wind,wind_bearing,temp):

	for stadium in home_venues:
		gametime_utc = gametime[stadium]
		print gametime_utc,stadium
		c.execute("""SELECT lat,lng,bearing_3B FROM venues WHERE draftkings = %s""",(stadium,))
		result = c.fetchone()
		lat = result[0]
		lng = result[1]
		bearing = result[2]

		print "https://api.forecast.io/forecast/c2e6c96f226ff429a4759bfa8be6c274/%.4f,%.4f,%i"%(lat,lng,gametime_utc)
		req = urllib2.Request("https://api.forecast.io/forecast/c2e6c96f226ff429a4759bfa8be6c274/%.4f,%.4f,%i"%(lat,lng,gametime_utc))
		response = urllib2.urlopen(req)
		data = json.loads(response.read())

		# data = {"latitude":33.4454,"longitude":-112.067,"timezone":"America/Phoenix","offset":-7,"currently":{"time":1430262000,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":89.16,"apparentTemperature":85.28,"dewPoint":36.76,"humidity":0.16,"windSpeed":3.64,"windBearing":106,"visibility":10,"cloudCover":0.11,"pressure":1011.5,"ozone":341.16},"hourly":{"summary":"Clear throughout the day.","icon":"clear-day","data":[{"time":1430204400,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":69.52,"apparentTemperature":69.52,"dewPoint":41.75,"humidity":0.37,"windSpeed":1.52,"windBearing":82,"visibility":10,"cloudCover":0.04,"pressure":1012.38,"ozone":341.67},{"time":1430208000,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":67.37,"apparentTemperature":67.37,"dewPoint":42.47,"humidity":0.4,"windSpeed":2.23,"windBearing":79,"visibility":10,"cloudCover":0.01,"pressure":1012.62,"ozone":341.77},{"time":1430211600,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":65.71,"apparentTemperature":65.71,"dewPoint":42.74,"humidity":0.43,"windSpeed":3.51,"windBearing":85,"visibility":10,"cloudCover":0.01,"pressure":1012.87,"ozone":341.87},{"time":1430215200,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":64.43,"apparentTemperature":64.43,"dewPoint":43.09,"humidity":0.46,"windSpeed":4.23,"windBearing":90,"visibility":10,"cloudCover":0.01,"pressure":1013.15,"ozone":341.39},{"time":1430218800,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":63.93,"apparentTemperature":63.93,"dewPoint":42.7,"humidity":0.46,"windSpeed":4.5,"windBearing":96,"visibility":10,"cloudCover":0,"pressure":1013.47,"ozone":340.68},{"time":1430222400,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":64.19,"apparentTemperature":64.19,"dewPoint":43.02,"humidity":0.46,"windSpeed":4.87,"windBearing":108,"visibility":10,"cloudCover":0,"pressure":1013.97,"ozone":340.25},{"time":1430226000,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":64.72,"apparentTemperature":64.72,"dewPoint":43.38,"humidity":0.46,"windSpeed":5.91,"windBearing":103,"visibility":10,"cloudCover":0.11,"pressure":1014.91,"ozone":340.3},{"time":1430229600,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":68.44,"apparentTemperature":68.44,"dewPoint":43.86,"humidity":0.41,"windSpeed":6.85,"windBearing":100,"visibility":10,"cloudCover":0.12,"pressure":1015.89,"ozone":340.62},{"time":1430233200,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":73.7,"apparentTemperature":73.7,"dewPoint":44.58,"humidity":0.35,"windSpeed":8.12,"windBearing":99,"visibility":10,"cloudCover":0.13,"pressure":1016.46,"ozone":341.08},{"time":1430236800,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":77.81,"apparentTemperature":77.81,"dewPoint":43.02,"humidity":0.29,"windSpeed":9.2,"windBearing":103,"visibility":10,"cloudCover":0.08,"pressure":1016.54,"ozone":341.7},{"time":1430240400,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":80.35,"apparentTemperature":79.1,"dewPoint":41.73,"humidity":0.25,"windSpeed":10.13,"windBearing":113,"visibility":10,"cloudCover":0.1,"pressure":1016.34,"ozone":342.46},{"time":1430244000,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":83.15,"apparentTemperature":80.81,"dewPoint":41.44,"humidity":0.23,"windSpeed":9.34,"windBearing":116,"visibility":10,"cloudCover":0.1,"pressure":1015.93,"ozone":343.1},{"time":1430247600,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":85.02,"apparentTemperature":82.14,"dewPoint":41.41,"humidity":0.22,"windSpeed":7.72,"windBearing":120,"visibility":10,"cloudCover":0.11,"pressure":1015.19,"ozone":343.68},{"time":1430251200,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":87.14,"apparentTemperature":83.74,"dewPoint":40.37,"humidity":0.19,"windSpeed":4.74,"windBearing":126,"visibility":10,"cloudCover":0.12,"pressure":1014.18,"ozone":344.15},{"time":1430254800,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":88.26,"apparentTemperature":84.6,"dewPoint":39.03,"humidity":0.18,"windSpeed":3.34,"windBearing":109,"visibility":10,"cloudCover":0.13,"pressure":1013.2,"ozone":344},{"time":1430258400,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":88.89,"apparentTemperature":85.08,"dewPoint":37.63,"humidity":0.16,"windSpeed":3.67,"windBearing":109,"visibility":10,"cloudCover":0.12,"pressure":1012.29,"ozone":342.89},{"time":1430262000,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":89.16,"apparentTemperature":85.28,"dewPoint":36.76,"humidity":0.16,"windSpeed":3.64,"windBearing":106,"visibility":10,"cloudCover":0.11,"pressure":1011.5,"ozone":341.16},{"time":1430265600,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":88.66,"apparentTemperature":84.86,"dewPoint":36.59,"humidity":0.16,"windSpeed":4.03,"windBearing":108,"visibility":10,"cloudCover":0.2,"pressure":1011.07,"ozone":339.39},{"time":1430269200,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":87.36,"apparentTemperature":83.79,"dewPoint":36.75,"humidity":0.17,"windSpeed":3.95,"windBearing":103,"visibility":10,"cloudCover":0.19,"pressure":1011.1,"ozone":337.73},{"time":1430272800,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":84.48,"apparentTemperature":81.58,"dewPoint":37.2,"humidity":0.19,"windSpeed":3.63,"windBearing":86,"visibility":10,"cloudCover":0.19,"pressure":1011.44,"ozone":336.02},{"time":1430276400,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":80.88,"apparentTemperature":79.18,"dewPoint":37.24,"humidity":0.21,"windSpeed":3.58,"windBearing":79,"visibility":10,"cloudCover":0.18,"pressure":1011.92,"ozone":334.45},{"time":1430280000,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":78.04,"apparentTemperature":78.04,"dewPoint":37.43,"humidity":0.23,"windSpeed":4.4,"windBearing":82,"visibility":10,"cloudCover":0.09,"pressure":1012.54,"ozone":333.14},{"time":1430283600,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":76.11,"apparentTemperature":76.11,"dewPoint":38.7,"humidity":0.26,"windSpeed":5.21,"windBearing":93,"visibility":10,"cloudCover":0.08,"pressure":1013.25,"ozone":331.97},{"time":1430287200,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":74.01,"apparentTemperature":74.01,"dewPoint":39.16,"humidity":0.28,"windSpeed":5.92,"windBearing":95,"visibility":10,"cloudCover":0.01,"pressure":1013.79,"ozone":330.74}]},"daily":{"data":[{"time":1430204400,"summary":"Clear throughout the day.","icon":"clear-day","sunriseTime":1430225064,"sunsetTime":1430273368,"moonPhase":0.33,"precipIntensity":0,"precipIntensityMax":0,"precipProbability":0,"temperatureMin":63.93,"temperatureMinTime":1430218800,"temperatureMax":89.16,"temperatureMaxTime":1430262000,"apparentTemperatureMin":63.93,"apparentTemperatureMinTime":1430218800,"apparentTemperatureMax":85.28,"apparentTemperatureMaxTime":1430262000,"dewPoint":40.5,"humidity":0.29,"windSpeed":5.06,"windBearing":102,"visibility":10,"cloudCover":0.09,"pressure":1013.58,"ozone":339.84}]},"flags":{"sources":["nwspa","isd","fnmoc","sref","rtma","rap","nam","cmc","gfs","lamp"],"isd-stations":["722780-23183","722787-53126","999999-23183","999999-53156","999999-93140"],"lamp-stations":["KCHD","KDVT","KGEU","KGYR","KIWA","KLUF","KPHX","KSDL"],"units":"us"}}
		# 'Out to CF':0,'L to R':1,'In from LF':2,'In from CF':3,'In from RF':4,'Out to RF':5,'Out to LF':6,'R to L':7
		for d in data['hourly']['data']:
			if d['time'] > gametime_utc:
				print d['time'],gametime_utc
				temp[stadium] = d['temperature']
				wind[stadium] = d['windSpeed']
				bearing = d['windBearing'] - bearing
				if bearing > 359:
					bearing -= 360
				elif bearing < 0:
					bearing += 360


				if bearing < 30:
					wind_bearing[stadium] = 'In from LF'
				elif bearing < 60:
					wind_bearing[stadium] = 'In from CF'
				elif bearing < 90:
					wind_bearing[stadium] = 'In from RF'
				elif bearing < 180:
					wind_bearing[stadium] = 'R to L'
				elif bearing < 210:
					wind_bearing[stadium] = 'Out to LF'
				elif bearing < 240:
					wind_bearing[stadium] = 'Out to CF'
				elif bearing < 270:
					wind_bearing[stadium] = 'Out to RF'
				else:
					wind_bearing[stadium] = 'L to R'


				break

	return

def main(draftGroupId):

	opponents = {}
	venues = {}
	gametime = {}
	wind = {}
	bearing = {}
	temp = {}
	teammap = {}
	home_venues = []

	url = "https://www.draftkings.com/lineup/getavailableplayers?draftGroupId=%s"%(str(draftGroupId),)
	req = urllib2.Request(url)
	response = urllib2.urlopen(req)
	D = json.loads(response.read())

	####### TEAMS #######

	for game in D['teamList']:
		home_venues.append(D['teamList'][game]['ht'])

		opponents[D['teamList'][game]['ht']] = D['teamList'][game]['at']
		opponents[D['teamList'][game]['at']] = D['teamList'][game]['ht']

		venues[D['teamList'][game]['at']] =D['teamList'][game]['ht']
		venues[D['teamList'][game]['ht']] = D['teamList'][game]['ht']
		
		gametime[D['teamList'][game]['at']] = int(re.sub("[^0-9]", "",D['teamList'][game]['tz']))/1000
		gametime[D['teamList'][game]['ht']] = int(re.sub("[^0-9]", "",D['teamList'][game]['tz']))/1000
		
		teammap[D['teamList'][game]['htid']] = D['teamList'][game]['ht']
		teammap[D['teamList'][game]['atid']] = D['teamList'][game]['at']

	get_weather(home_venues,gametime,wind,bearing,temp)



	######### PLAYERS ############

	for player in D['playerList']:
		team = teammap[player['tid']]
		if venues[team] == team:
			home = True
		else:
			home = False
		
		gametime_str = datetime.fromtimestamp(gametime[team]).strftime('%Y-%m-%d %H:%M:%S')
		gameday_str = datetime.fromtimestamp(gametime[team]).strftime('%Y-%m-%d')

		c.execute("""INSERT IGNORE INTO draftkings_contests (gameid,contestday,dkid,fppg,team,opp,home,indicator,name,pos,salary,gametime,temp,wind_speed,wind_dir) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",\
			(draftGroupId,gameday_str,player['pid'],player['ppg'],team,opponents[team],home,player['i'],player['fn']+' '+player['ln'],player['pn'],player['s'],gametime_str,temp[venues[team]],wind[venues[team]],bearing[venues[team]]))

		c.execute("""UPDATE draftkings_contests SET temp=%s,wind_speed=%s,wind_dir=%s WHERE gameid=%s and dkid=%s""",\
			(temp[venues[team]],wind[venues[team]],bearing[venues[team]],draftGroupId,player['pid'])) 

	db.commit()

if __name__ == "__main__":
	try:
		gameid = sys.argv[1]
	except:
		gameid = '6639'
	main(gameid)
