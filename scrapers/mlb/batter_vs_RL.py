import urllib2, json, sys, re
from datetime import datetime,date,timedelta
import xml.etree.ElementTree as ET
import MySQLdb

db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
b = db.cursor()
p = db.cursor()
c = db.cursor()

def process_batter(batter):
	ab = batter['ab']
	hbp = batter['hbp']
	r = batter['r']
	h = batter['h']
	d = batter['h2b']
	t = batter['h3b']
	hr = batter['hr']
	rbi = batter['rbi']
	bb = batter['bb']
	so = batter['so']
	sb = batter['sb']
	sit_code = batter["sit_code"]

	try:
		cs = batter['cs']
	except:
		cs = 0
	try:
		go = batter['go']
	except:
		go = 0
	try:
		ao = batter['ao']
	except:
		ao = 0
	try:
		sf = batter['sf']
	except:
		sf = None
	try:
		sac = batter['sac']
	except:
		sac = None

	c.execute("""INSERT IGNORE INTO batter_sit (pid,season,name,sit_code,hbp,ab,r,h,d,t,hr,rbi,bb,so,sb,cs,go,ao) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
	(pid,season,name,sit_code,hbp,ab,r,h,d,t,hr,rbi,bb,so,sb,cs,go,ao)
	)

	return




c.execute("""SELECT distinct(pid) FROM players""")

k = 0
missed_combinations = 0
players = c.fetchall()



for player in players:
	print 'Processing player:',k
	k += 1
	pid = str(player[0])
	for season in ['2012','2013','2014']:
	# print "http://mlb.com/lookup/json/named.mlb_individual_hitting_sits_composed.bam?sport_code='mlb'&game_type='R'&season=2015&player_id=%s&sit_code='vl'&sit_code='vr'"%(pid)
		req = urllib2.Request("http://mlb.com/lookup/json/named.mlb_individual_hitting_sits_composed.bam?sport_code='mlb'&game_type='R'&season=%s&player_id=%s&sit_code='vl'&sit_code='vr'"%(season,pid))
		response = urllib2.urlopen(req)
		D = json.loads(response.read())
		# print json.dumps(D,indent=4)
		try:
			name = D['mlb_individual_hitting_sits_composed']['mlb_individual_hitting_season_total']['queryResults']['row']['name_display_first_last']
		except:
			continue
		if D['mlb_individual_hitting_sits_composed']['mlb_individual_hitting_sits_season']['queryResults'].has_key('row'):
			# print json.dumps(D['mlb_individual_hitting_sits_composed']['mlb_individual_hitting_sits_season']['queryResults']['row'],indent=4)
			if type(D['mlb_individual_hitting_sits_composed']['mlb_individual_hitting_sits_season']['queryResults']['row']) == list:
				for batter in D['mlb_individual_hitting_sits_composed']['mlb_individual_hitting_sits_season']['queryResults']['row']:
					process_batter(batter)
			else:
				process_batter(D['mlb_individual_hitting_sits_composed']['mlb_individual_hitting_sits_season']['queryResults']['row'])
		
				

	db.commit()


print 'Missed Combinations:',missed_combinations
