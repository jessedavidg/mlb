import urllib2, json, sys
from datetime import datetime,date,timedelta
import MySQLdb

db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
c = db.cursor()

def daterange(start_date, end_date):
	for n in range(int ((end_date - start_date).days)):
		yield start_date + timedelta(n)

def process(game,adate):
	if game['game_type'] in ['S','E','A']:
		return
	if len(game['time']) < 5:
		game['time'] = '0'+game['time']
	datestr = ','.join([game['time'],game['ampm'],'%02d'%adate.day,'%02d'%adate.month,str(adate.year)])
	# print game['id'],game['game_pk'],game['home_name_abbrev'],game['away_name_abbrev'],datetime.strptime(datestr,'%I:%M,%p,%d,%m,%Y')
	c.execute("""INSERT IGNORE INTO games (gameid,game_pk,home,away,gametime) VALUES (%s,%s,%s,%s,%s)""",
	(game['id'],game['game_pk'],game['home_name_abbrev'],game['away_name_abbrev'],datetime.strptime(datestr,'%I:%M,%p,%d,%m,%Y'))
	)

	db.commit()
	return

c.execute("""SELECT MAX(date(gametime)) FROM batters""")
startdate = datetime.strptime(c.fetchone()[0], '%Y-%m-%d').date() + timedelta(1)
# startdate = date(2015,04,5)
# stopdate = date(2014,10,29)
stopdate = date.today()

# game_pk, gameid, home, away, gametime


for adate in daterange(startdate,stopdate):
	print "http://mlb.mlb.com/gdcross/components/game/mlb/year_%s/month_%s/day_%s/master_scoreboard.json"%(str(adate.year),'%02d'%adate.month,'%02d'%adate.day)
	req = urllib2.Request("http://mlb.mlb.com/gdcross/components/game/mlb/year_%s/month_%s/day_%s/master_scoreboard.json"%(str(adate.year),'%02d'%adate.month,'%02d'%adate.day))
	response = urllib2.urlopen(req)
	D = json.loads(response.read())

	if D['data']['games'].has_key('game'):
		if type(D['data']['games']['game']) is not list:
			process(D['data']['games']['game'],adate)
		else:
			for game in D['data']['games']['game']:
				process(game,adate)

	else:
		print 'No games for day:',adate
