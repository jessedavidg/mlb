import urllib2, json, sys, re
from datetime import datetime,date,timedelta
import xml.etree.ElementTree as ET
import MySQLdb

db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
c = db.cursor()

def process_pitcher(pitcher,start):
	hr = pitcher['hr']
	try:
		np = pitcher['np']
	except:
		np = None
	name = pitcher['name_display_first_last']
	pos = pitcher['pos']
	pid = pitcher['id']
	bf = pitcher['bf']
	bb = pitcher['bb']
	so = pitcher['so']
	h = pitcher['h']
	try:
		s = pitcher['s']
	except:
		s = None
	r = pitcher['r']
	er = pitcher['er']
	outs = pitcher['out']


	try:
		if pitcher['win'] == "true":
			w = True
		else:
			w = False
	except:
		w = False

	c.execute("""INSERT IGNORE INTO pitchers (pid,gameid,venue_id,name,pos,team,home,away,gametime,temp,wind_speed,wind_dir,weather,hpu,np,s,outs,h,r,er,hr,bb,so,bf,w,start) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
	(pid,gameid,venue_id,name,pos,team,home,away,gametime,temp,wind_speed,wind_dir,weather,hpu,np,s,outs,h,r,er,hr,bb,so,bf,w,start)
	)
	

	return


c.execute("""SELECT g.gameid,g.gametime FROM games g WHERE YEAR(gametime) = '2015' AND gameid NOT IN (SELECT DISTINCT(gameid) FROM batters) AND gametime > (SELECT MAX(gametime) FROM batters)""")

# gameid,name,team,home,away,gametime,temp,wind_speed,wind_dir,weather,umpire_home
# pid,hbp,ab,r,h,d,t,hr,rbi,bb,so,sb,cs,pitches,bat_order,start


for game in c.fetchall():
	gameid = game[0]
	gametime = game[1]

	print "http://mlb.mlb.com/gdcross/components/game/mlb/year_%s/month_%s/day_%s/gid_%s/boxscore.json"%(str(gametime.year),'%02d'%gametime.month,'%02d'%gametime.day,gameid.replace('-','_').replace('/','_'))
	req = urllib2.Request("http://mlb.mlb.com/gdcross/components/game/mlb/year_%s/month_%s/day_%s/gid_%s/boxscore.json"%(str(gametime.year),'%02d'%gametime.month,'%02d'%gametime.day,gameid.replace('-','_').replace('/','_')))
	try:
		response = urllib2.urlopen(req)
	except:
		print 'File DNE'
		continue
	D = json.loads(response.read())
	
	boxscore = D['data']['boxscore']
	batting = D['data']['boxscore']['batting']
	pitching = D['data']['boxscore']['pitching']

	# print batting

	gameid = boxscore['game_id']
	away = boxscore['away_team_code'].upper()
	home = boxscore['home_team_code'].upper()
	venue_id = boxscore['venue_id']

	root = ET.fromstring("<root>" + boxscore['game_info'].replace('\n','').replace('<br/>','') + "</root>")
	

	### Umpire and weather

	hpu = None
	temp = None
	weather = None
	wind_speed = None
	wind_dir = None

	for umpires in root.iter('umpires'):
		for umpire in umpires.iter('umpire'):
			if umpire.attrib['position'] == 'HP':
				hpu = umpire.attrib['name']

	for weather_xml in root.iter('weather'):
		weather = weather_xml.text.split(',')[1].strip()
		try:
			temp = int(weather_xml.text[:3])
		except:
			temp = int(weather_xml.text[:2])

	for wind_xml in root.iter('wind'):
		if re.search('mph',wind_xml.text):
			wind_speed = int(wind_xml.text[:2])
			try:
				wind_dir = wind_xml.text.split(',')[1].strip()
			except:
				wind_dir = None

	for a_team in pitching:
		if a_team['team_flag'] == 'home':
			team = home
		else:
			team = away

		if type(a_team['pitcher']) is not list:
			start = True
			process_pitcher(a_team['pitcher'],start)

		else:
			pnum = 1
			for pitcher in a_team['pitcher']:
				if pnum == 1:
					start = True
				else:
					start = False
				pnum += 1
				process_pitcher(pitcher,start)

	for a_team in batting:
		if a_team['team_flag'] == 'home':
			team = home
		else:
			team = away
		for batter in a_team['batter']:
			# print '8'*40
			# for key in batter:
			# 	print key,batter[key]
			name = batter['name_display_first_last']
			ab = batter['ab']
			pid = batter['id']
			hbp = batter['hbp']
			r = batter['r']
			h = batter['h']
			d = batter['d']
			t = batter['t']
			hr = batter['hr']
			rbi = batter['rbi']
			bb = batter['bb']
			so = batter['so']
			sb = batter['sb']
			try:
				cs = batter['cs']
			except:
				cs = 0
			# new
			pos = batter['pos']
			try:
				go = batter['go']
			except:
				go = 0
			try:
				ao = batter['ao']
			except:
				ao = 0
			try:
				bo = batter['bo']
			except:
				bo = '000'
			try:
				sf = batter['sf']
			except:
				sf = None
			try:
				sac = batter['sac']
			except:
				sac = None

			if bo in ['100','200','300','400','500','600','700','800','900']:
				start = True
			else:
				start = False

			c.execute("""INSERT IGNORE INTO batters (pid,gameid,venue_id,name,pos,team,home,away,gametime,temp,wind_speed,wind_dir,weather,hpu,bo,hbp,ab,r,h,d,t,hr,rbi,bb,so,sb,cs,go,ao,sf,sac,start) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
			(pid,gameid,venue_id,name,pos,team,home,away,gametime,temp,wind_speed,wind_dir,weather,hpu,bo,hbp,ab,r,h,d,t,hr,rbi,bb,so,sb,cs,go,ao,sf,sac,start)
			)

	db.commit()


