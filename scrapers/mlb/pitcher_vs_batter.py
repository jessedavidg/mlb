import urllib2, json, sys, re
from datetime import datetime,date,timedelta
import xml.etree.ElementTree as ET
import MySQLdb

db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
b = db.cursor()
p = db.cursor()
c = db.cursor()

def process_pitcher(row):
	ab = row['ab']
	batid = row['player_id']
	sac = row['sac']
	ao = row['ao']
	go = row['go']
	pitchid = row['pitcher_id']
	cs = row['cs']
	tpa = row['tpa']
	sf = row['sf']
	t = row['t']
	np = row['np']
	if np == '':
		np = 0
	tb = row['tb']
	bb = row['bb']
	hr = row['hr']
	season = row['season']
	gidp = row['gidp']
	d = row['d']
	h = row['h']
	ibb = row['ibb']
	hbp = row['hbp']
	r = row['r']
	so = row['so']
	rbi = row['rbi']

	c.execute("""INSERT INTO battervspitcher (ab,batid,sac,ao,go,pitchid,cs,tpa,sf,t,np,tb,bb,hr,season,gidp,d,h,ibb,hbp,r,so,rbi) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE ab=%s,sac=%s,ao=%s,go=%s,cs=%s,tpa=%s,sf=%s,t=%s,np=%s,tb=%s,bb=%s,hr=%s,gidp=%s,d=%s,h=%s,ibb=%s,hbp=%s,r=%s,so=%s,rbi=%s""",
	(ab,batid,sac,ao,go,pitchid,cs,tpa,sf,t,np,tb,bb,hr,season,gidp,d,h,ibb,hbp,r,so,rbi,ab,sac,ao,go,cs,tpa,sf,t,np,tb,bb,hr,gidp,d,h,ibb,hbp,r,so,rbi)
	)

	return



# gameid,name,team,home,away,gametime,temp,wind_speed,wind_dir,weather,umpire_home
# pid,hbp,ab,r,h,d,t,hr,rbi,bb,so,sb,cs,pitches,bat_order,start

# d = json.load(open('joey.json','rb'))
# print d['stats_batter_vs_pitcher_composed']['stats_batter_vs_pitcher']['queryResults']['created']
# for row in d['stats_batter_vs_pitcher_composed']['stats_batter_vs_pitcher']['queryResults']['row']:
# 	print '*'*20+'\n'
# 	for key in row:
# 		print key,row[key]


# b.execute("""SELECT pid,count(pid) FROM batters group by pid order by count(pid) desc limit 300""")
# p.execute("""SELECT pid,count(pid) FROM pitchers WHERE start = 1 group by pid order by count(pid) desc limit 200""")

b.execute("""SELECT pid,count(pid) FROM batters where start = 1 and year(gametime) = '2015' group by pid having count(pid) > 30""")
p.execute("""SELECT pid,count(pid) FROM pitchers WHERE start = 1 and year(gametime) = '2015' group by pid having count(pid) > 5 order by count(pid) desc""")


k = 0
missed_combinations = 0
batters = b.fetchall()
pitchers = p.fetchall()

for batter in batters:
	for pitcher in pitchers:
		# print type(batter[0]),type(pitcher[0])
		print 'Processing combo:',k
		if k < 0:
			k+=1
			continue
		k += 1
		# print "http://mlb.com/lookup/json/named.stats_batter_vs_pitcher_composed.bam?sport_code='mlb'&game_type='R'&player_id=%s&pitcher_id=%s"%(str(batter[0]),str(pitcher[0]))
		req = urllib2.Request("http://mlb.com/lookup/json/named.stats_batter_vs_pitcher_composed.bam?sport_code='mlb'&game_type='F'&game_type='D'&game_type='L'&game_type='W'&game_type='C'&game_type='R'&player_id=%s&pitcher_id=%s"%(str(batter[0]),str(pitcher[0])))
		# print "http://mlb.mlb.com/gdcross/components/game/mlb/year_%s/month_%s/day_%s/gid_%s/boxscore.json"%(str(gametime.year),'%02d'%gametime.month,'%02d'%gametime.day,gameid.replace('-','_').replace('/','_'))
		# req = urllib2.Request("http://mlb.mlb.com/gdcross/components/game/mlb/year_%s/month_%s/day_%s/gid_%s/boxscore.json"%(str(gametime.year),'%02d'%gametime.month,'%02d'%gametime.day,gameid.replace('-','_').replace('/','_')))
		# try:
		# except:
		# 	print 'File DNE'
		# 	continue
		response = urllib2.urlopen(req)
		D = json.loads(response.read())
		# print json.dumps(D,indent=4)
		updatetime = D['stats_batter_vs_pitcher_composed']['stats_batter_vs_pitcher']['queryResults']['created']
		if D['stats_batter_vs_pitcher_composed']['stats_batter_vs_pitcher']['queryResults'].has_key('row'):
			data = D['stats_batter_vs_pitcher_composed']['stats_batter_vs_pitcher']['queryResults']['row']
		else:
			print 'No data.',pitcher,batter
			missed_combinations += 1
			continue
		if type(data) == list:
			for row in data:
				process_pitcher(row)
		else:
			process_pitcher(data)
				

	db.commit()


print 'Missed Combinations:',missed_combinations
