import numpy as np
import time,pickle
from datetime import datetime,date,timedelta
import csv, sys, json, os
from sklearn import preprocessing,decomposition
from sklearn.linear_model import ElasticNet
from sklearn.metrics import mean_squared_error,mean_absolute_error
import pylab as plt
from sklearn.pipeline import Pipeline
import MySQLdb


class batter():

	def __init__(self,pid,name,team,pos,salary,home,opponent,bo,pitcher,temp,wind_speed,wind_dir,year,db):
		self.transforms = self.load_transforms()
		self.pid = pid
		self.name = name
		self.team = team
		self.opponent = opponent
		self.pos = pos
		self.temp = temp
		self.salary = salary
		self.data = []
		self.fantasy_prediction = 0.
		self.home = home
		self.wind_speed = wind_speed
		self.wind_dir = wind_dir
		self.bo = bo
		self.pitcher = pitcher
		self.year = year
		self.gamesPlayed = self.checkGamesPlayed(db)
		self.useWeather = self.checkStadium()

	def load_player_data_full(self,db):
		if self.gamesPlayed:
			self.load_data(db)
			self.add_previous_year(db)
			self.add_previous_days(db,7,0)
			self.add_previous_days(db,14,0)
			self.add_previous_days(db,21,0)
			self.add_opposing_pitcher(db)
			self.add_bo()
			self.add_home()
			self.add_venue(db)
			self.add_venue_games(db)
			self.add_temp()
			self.add_wind()
			self.add_historical_fantasy_points(db,5)
			self.missedGames(db,10)
			self.missedGames(db,3)
			self.add_cluster(db)
			self.add_games_played()
			self.add_matchup(db)
		else:
			return


	def checkGamesPlayed(self,db):
		c = db.cursor()
		c.execute("SELECT count(*) FROM batters WHERE (ab+hbp+bb+sf)>0 AND start=1 AND pid = %s and year(gametime)=%s",(self.pid,self.year))

		result = c.fetchone()[0]
		return int(result)

	def missedGames(self,db,n_games):
		c = db.cursor()
		c.execute("SELECT count(*) from batters,(select distinct(gametime) from batters where team=(SELECT team from venues WHERE fanduel = %s) order by gametime desc LIMIT 1 OFFSET %s) as `team` where pid=%s and batters.gametime>team.gametime",(self.team,n_games,self.pid))
		result = c.fetchone()
		self.data.append(int(result[0]))
		# print 'Games played out of last 7:',result[0],self.pid
   		return

	def checkStadium(self):
		if self.home == 1 and self.team not in ['MIA','TAM','ARI','MIL','HOU','TOR']:
			# print 'YES STADIUM',self.team
			return True
		elif self.home == 0 and self.opponent not in ['MIA','TAM','ARI','MIL','HOU','TOR']:
			# print 'YES STADIUM',self.team
			return True
		else:
			# print 'NO STADIUM',self.team
			return False
			

	def load_transforms(self):

		## Load transformations
		transforms = pickle.load(open('models/transforms_batter_0.p','rb'))

		return transforms

	def combine_features(self,X,xnew):
		xnew = np.array(xnew,dtype=float)
		if len(xnew.shape) == 1:
			xnew = xnew.reshape(xnew.shape[0], 1)
		return np.concatenate((X,xnew),axis=1)

	def modulate_features(self):
		num_fea = len(self.data)

		for i in range(num_fea):
			for j in range(i,num_fea):
				# print i,j,self.data[i],self.data[j],type(self.data[i]),type(self.data[j])
				self.data.append(float(self.data[i])*float(self.data[j]))
		return 

	def load_data(self,db):
		c = db.cursor()

		c.execute("SELECT \
		(hist.sum_h*3+hist.sum_d*2+hist.sum_t*5+hist.sum_hr*7+hist.sum_rbi*2+hist.sum_r*2+hist.sum_bb*2+hist.sum_sb*5+hist.sum_hbp*2-hist.sum_cs*2)/game_count fph,\
		(hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab),hist.sum_bb/(hist.sum_ab+hist.sum_bb+hist.sum_sf), \
		hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
		hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
		hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		hist.sum_sb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_cs/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf)/game_count,\
		hist.sum_go/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),hist.sum_ao/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
		hist.sum_h/hist.sum_ab,(hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3)/hist.sum_ab \
		FROM \
		(SELECT sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(cs) sum_cs,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(sb) sum_sb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(pid) game_count \
		FROM batters \
		WHERE pid = %s and (ab+hbp+bb+sf)>0 and start=1 and year(gametime) = %s)\
		AS hist",
		(self.pid,self.year))

		r = c.fetchone()
		try:
			self.data = [float(v) for v in r]
		except:
			self.data = [0. for v in r]
		return


	def add_opposing_pitcher(self,db):
		p_data = []
		c = db.cursor()

		c.execute("SELECT sum(bb)/sum(bf),sum(h)/sum(bf),sum(np)/sum(bf),sum(s)/sum(bf),sum(so)/sum(bf),sum(r)/sum(bf),sum(s)/sum(np),sum(outs)/count(distinct(gameid)),count(distinct(gameid)) FROM pitchers WHERE year(gametime) >= %s AND pid = %s",(int(self.year)-1,self.pitcher))
		r_pitcher = c.fetchone()

		r_avg = [0.0744,0.2350,3.7733,2.4001,0.1872,0.1187,0.6361,17.6559]
		
		for k in range(len(r_avg)):
			if r_pitcher[0] == None:
				# print 'Good pitcher data.'
				self.data.append(float(r_avg[k]))
			else:
				self.data.append(float(r_pitcher[k]))
				# print 'Bad Pitcher data, using average.',self.pitcher

		if r_pitcher[0] == None:
			for k in [0,0,0,0,0]:
				self.data.append(k)
		elif r_pitcher[-1] < 4:
			for k in [1,0,0,0,0]:
				self.data.append(k)
		elif r_pitcher[-1] < 7:
			for k in [0,1,0,0,0]:
				self.data.append(k)
		elif r_pitcher[-1] < 10:
			for k in [0,0,1,0,0]:
				self.data.append(k)
		elif r_pitcher[-1] < 20:
			for k in [0,0,0,1,0]:
				self.data.append(k)
		else:
			for k in [0,0,0,0,1]:
				self.data.append(k)

		return


	def add_historical_fantasy_points(self,db,n):
		c = db.cursor()
		c.execute("SELECT (h*3 + d*2 + t*5 + hr*7 + rbi*2 + r*2 + bb*2 + sb*5 + hbp*2 - (cs*2)) fph FROM batters WHERE pid=%s AND start=1 and (ab+hbp+bb+sf)>0 order by gametime desc LIMIT %s",(self.pid,n))
		r = c.fetchall()
		if self.gamesPlayed < 5:
			for v in range(5):
				self.data.append(0.)
		else:
			for v in r:
				self.data.append(float(v[0]))
		return

	def add_bo(self):
		bo = [0,0,0,0,0,0,0,0,0]
		if self.bo:
			bo[self.bo-1] = 1
		for v in bo:
			self.data.append(v)
		return

	def add_home(self):
		self.data.append(self.home)
		return

	def add_temp(self):
		self.data.append(self.temp)
		return

	def add_venue(self,db):
		c = db.cursor()
		if self.home == 1:
			team = self.team
		else:
			team = self.opponent

		c.execute("SELECT venue_id FROM venues WHERE fanduel = %s",(team,))
		result = c.fetchone()
		for v in self.transforms['venues'].transform(result)[0]:
			self.data.append(v)

		return

	def add_age(self,db):
		c = db.cursor()

		c.execute("SELECT age FROM players WHERE pid = %s",(self.pid,))
		result = c.fetchone()
		self.data.append(int(result[0]))

		return

	def add_wind(self):
		windI = {'Out to CF':0,'L to R':1,'In from LF':2,'In from CF':3,'In from RF':4,'Out to RF':5,'Out to LF':6,'R to L':7}
		wind = [0,0,0,0,0,0,0,0]
		if self.useWeather:
			wind[windI[self.wind_dir]] = self.wind_speed
		for v in wind:
			self.data.append(v)
		# print wind

		return

	def add_previous_year(self,db):
		c = db.cursor()

		c.execute("SELECT \
		(hist.sum_h*3+hist.sum_d*2+hist.sum_t*5+hist.sum_hr*7+hist.sum_rbi*2+hist.sum_r*2+hist.sum_bb*2+hist.sum_sb*5+hist.sum_hbp*2-hist.sum_cs*2)/game_count fph,\
		(hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab),hist.sum_bb/(hist.sum_ab+hist.sum_bb+hist.sum_sf), \
		hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
		hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
		hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		hist.sum_sb/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_cs/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf)/game_count,\
		hist.sum_go/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),hist.sum_ao/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
		hist.sum_h/hist.sum_ab,(hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3)/hist.sum_ab \
		FROM \
		(SELECT sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(cs) sum_cs,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(sb) sum_sb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(pid) game_count \
		FROM batters \
		WHERE pid = %s and start=1 AND (ab+hbp+bb+sf)>0 and gametime >= %s)\
		AS hist",
		(self.pid,str(int(self.year)-1)+'-01-01'))
		# print self.pid,str(int(self.year)-1)+'-01-01'

		r = c.fetchone()
		for v in r:
			try:
				self.data.append(float(v))
			except:
				print 'ERROR! PREVIOUS:',self.pid
				self.data.append(0.)
		return

	def add_cluster(self,db):
		c = db.cursor()
		c.execute("SELECT \
		(hist.sum_h-hist.sum_d-hist.sum_t-hist.sum_hr)/(hist.sum_ab),hist.sum_bb/(hist.sum_ab+hist.sum_bb+hist.sum_sf), \
		hist.sum_d/(hist.sum_ab),hist.sum_t/(hist.sum_ab),\
		hist.sum_hr/(hist.sum_ab),(hist.sum_h-hist.sum_hr)/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
		hist.sum_so/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),hist.sum_hbp/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		hist.sum_rbi/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		hist.sum_r/(hist.sum_ab+hist.sum_hbp+hist.sum_bb+hist.sum_sf),\
		hist.sum_go/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),hist.sum_ao/(hist.sum_ab-hist.sum_so-hist.sum_hr+hist.sum_sf),\
		hist.sum_h/hist.sum_ab,(hist.sum_h+hist.sum_d+hist.sum_t*2+hist.sum_hr*3)/hist.sum_ab,hist.sum_ab \
		FROM \
		(SELECT sum(go) sum_go,sum(ao) sum_ao,sum(so) sum_so,sum(h) sum_h, sum(d) sum_d,sum(t) sum_t,sum(hr) sum_hr,sum(rbi) sum_rbi,sum(r) sum_r,sum(bb) sum_bb,sum(hbp) sum_hbp,sum(ab) sum_ab,sum(sf) sum_sf,count(batid) game_count,sum(tpa) sum_tpa \
		FROM battervspitcher,pitcher_clusters \
		WHERE batid=%s AND cluster = (SELECT cluster FROM pitcher_clusters WHERE pid=%s) and pitchid=pid and season > '2009' group by cluster)\
		AS hist",(self.pid,self.pitcher))

		result = c.fetchone()

		r_avg = [0.1757,0.0804,0.0547,0.0062,0.0331,0.3048,0.1798,0.0082,0.1119,0.117,0.3819,0.3363,0.2696,0.4358]
		if result != None:
			if result[-1] <5:
				print result[-1],self.name
		for k in range(len(r_avg)):
			if result == None:
				self.data.append(float(r_avg[k]))
			elif result[k] == None:
				self.data.append(float(r_avg[k]))
			else:
				try:
					self.data.append(float(result[k]))
				except:
					self.data.append(float(r_avg[k]))
					print k,result[k]
			# try:
			# 	self.data.append(float(result[k]))
			# except:
			# 	self.data.append(float(r_avg[k]))

		if result == None:
			for k in [0,0,0,0,0,0]:
				self.data.append(k)
		elif result[0] == None:
			for k in [0,0,0,0,0,0]:
				self.data.append(k)
		elif (result[-1] < 4):
			for k in [1,0,0,0,0,0]:
				self.data.append(k)
		elif (result[-1] < 7):
			for k in [0,1,0,0,0,0]:
				self.data.append(k)
		elif (result[-1] < 10):
			for k in [0,0,1,0,0,0]:
				self.data.append(k)
		elif (result[-1] < 15):
			for k in [0,0,0,1,0,0]:
				self.data.append(k)		
		elif (result[-1] < 20):
			for k in [0,0,0,0,1,0]:
				self.data.append(k)
		elif (result[-1] >=20):
			for k in [0,0,0,0,0,1]:
				self.data.append(k) 

		return

	def add_matchup(self,db):
		lvp_avg = [0.2625, 0.2973, 0.0068, 0.0058, 0.0544, 0.0326, 0.0792, 0.4320, 0.2682, 0.1754, 0.1149, 0.1797, 0.1097]
		c = db.cursor()

		c.execute("""SELECT sum(ao)/sum(ab),sum(go)/sum(ab),sum(cs)/sum(tpa),sum(t)/sum(ab),sum(d)/sum(ab),sum(hr)/sum(ab),sum(bb)/sum(tpa),sum(tb)/sum(ab),sum(h)/sum(ab),(sum(h)-sum(d)-sum(t)-sum(hr))/sum(ab),sum(r)/sum(tpa),sum(so)/sum(tpa),sum(rbi)/sum(tpa),sum(ab) \
			from battervspitcher WHERE pitchid = %s and batid = %s""",(self.pitcher,self.pid))
		result = c.fetchone()


		for k in range(len(lvp_avg)):
			if result == None:
				self.data.append(float(lvp_avg[k]))
			elif result[0] == None:
				self.data.append(float(lvp_avg[k]))
			else:
				self.data.append(float(result[k]))
			# try:
			# 	self.data.append(float(result[k]))
			# except:
			# 	self.data.append(float(lvp_avg[k]))

		if result == None:
			for k in [0,0,0,0,0,0]:
				self.data.append(k)
		elif result[0] == None:
			for k in [0,0,0,0,0,0]:
				self.data.append(k)
		elif (result[-1] < 4):
			for k in [1,0,0,0,0,0]:
				self.data.append(k)
		elif (result[-1] < 7):
			for k in [0,1,0,0,0,0]:
				self.data.append(k)
		elif (result[-1] < 10):
			for k in [0,0,1,0,0,0]:
				self.data.append(k)
		elif (result[-1] < 15):
			for k in [0,0,0,1,0,0]:
				self.data.append(k)		
		elif (result[-1] < 20):
			for k in [0,0,0,0,1,0]:
				self.data.append(k)
		elif (result[-1] >=20):
			for k in [0,0,0,0,0,1]:
				self.data.append(k)

		return

	def add_venue_games(self,db):
		venue_games = []
		c = db.cursor()

		c.execute("SELECT venue_id,count(*)/games_total\
		FROM batters,\
		(SELECT count(*) games_total FROM batters WHERE pid=%s AND start=1 and (ab+hbp+bb+sf)>0 and year(gametime)=%s) btotal \
		WHERE pid=%s AND start=1 and (ab+hbp+bb+sf)>0 and year(gametime)=%s group by venue_id",(self.pid,self.year,self.pid,self.year,))
		results = c.fetchall()
		venues = self.transforms['venues'].transform([-1])[0]
		
		for venue in results:
			venues = venues + self.transforms['venues'].transform([venue[0]])[0]*float(venue[1])
		for v in venues:
			self.data.append(v)
		return 

	def add_games_played(self):
		gp = [0,0,0,0]
		if self.gamesPlayed < 50:
			gp[0] = 1
		elif self.gamesPlayed < 70:
			gp[1] = 1
		elif self.gamesPlayed < 90:
			gp[2] = 1
		else:
			gp[3] = 1
		for k in gp:
			self.data.append(k)
		return


	def add_previous_days(self,db,n,no):
		c = db.cursor()
		c.execute("SELECT \
			(sum(h)*3+sum(d)*2+sum(t)*5+sum(hr)*7+sum(rbi)*2+sum(r)*2+sum(bb)*2+sum(sb)*5+sum(hbp)*2-sum(cs)*2)/count(pid) fph,\
			(sum(h)-sum(d)-sum(t)-sum(hr))/(sum(ab)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)), \
			sum(d)/sum(ab),sum(t)/(sum(ab)),\
			sum(hr)/sum(ab),(sum(h)-sum(hr))/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
			sum(so)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(hbp)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			sum(sb)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(rbi)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			sum(r)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(cs)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			(sum(ab)+sum(hbp)+sum(bb)+sum(sf))/count(pid),\
			sum(go)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),sum(ao)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
			sum(h)/sum(ab),(sum(h)+sum(d)+sum(t)*2+sum(hr)*3)/sum(ab) \
			FROM \
			(SELECT pid,go,ao,so,cs,h,d,t,hr,rbi,r,bb,sb,hbp,ab,sf \
			FROM batters \
			WHERE pid=%s AND start=1 AND (ab+hbp+bb+sf)>0 order by gametime desc LIMIT %s OFFSET %s)\
			AS hist",(self.pid,n,no))

		r = list(c.fetchone())
		if r[6] == None:
			r[6] = 0
			r[14] = 0
			r[15] = 0
		for v in r:
			try:
				self.data.append(float(v))
			except:
				print 'ERROR! PREVIOUS DAYS ERROR:',self.pid
				self.data.append(0.)
		return

	def predict(self):


		if self.gamesPlayed > 29:
			self.modulate_features()
			model = pickle.load(open('models/model_batter_dk_R_20.p','rb'))
			data = np.array(self.data,dtype=float)[model['ind']]
			p = model['clf'].predict(data)[0]
			self.fantasy_prediction = float(p)

			if self.fantasy_prediction < 0 or True:
				print self.pid,self.name,self.salary,self.fantasy_prediction,self.gamesPlayed
			# self.fantasy_prediction = p30*model_coefficients['m30'] + p50*model_coefficients['m50'] + p70*model_coefficients['m70']

		else:

			print 'not enough games for player:',self.gamesPlayed,self.pid
			self.fantasy_prediction = 0.

		return self.fantasy_prediction

class pitcher():

	def __init__(self,pid,name,team,pos,salary,home,opponent,temp,fid,gameid,wind_speed,wind_dir,opposing_pitcher,year,db):
		self.transforms = self.load_transforms()
		self.pid = pid
		self.name = name
		self.team = team
		self.home = home
		self.opponent = opponent
		self.pos = pos
		self.salary = salary
		self.temp = temp
		self.data = []
		self.fantasy_prediction = 0.
		self.year = year
		self.fid = fid
		self.gameid = gameid
		self.useWeather = self.checkStadium()
		self.wind_speed = wind_speed
		self.wind_dir = wind_dir
		self.gamesPlayed = self.checkMinGames(db)
		self.opposing_pitcher = opposing_pitcher
		self.data_is_bad = False



	def load_player_data(self,db):
		self.load_data(db)
		self.add_home()
		self.add_venue(db)
		self.add_venue_games(db)
		self.add_previous_year(db)
		self.pitching_prior_games(db,1,0)
		self.pitching_prior_games(db,1,1)
		self.pitching_prior_games(db,1,2)
		self.add_opponent_pitcher(db)
		self.add_opposing_batting(db)
		self.add_team_batting(db)
		self.add_temp()
		self.add_wind()
		self.add_games_played()
		self.lineup_vs_pitcher(db)


	def checkMinGames(self,db):
		c = db.cursor()
		c.execute("SELECT count(*) FROM pitchers WHERE pid = %s and start = 1 and bf > 0 and year(gametime) = %s",(self.pid,self.year))

		result = c.fetchone()[0]
		return int(result)


	def load_transforms(self):

		## Load transformations
		transforms = pickle.load(open('models/transforms_pitcher_0.p','rb'))

		return transforms


	def combine_features(self,X,xnew):
		xnew = np.array(xnew,dtype=float)
		if len(xnew.shape) == 1:
			xnew = xnew.reshape(xnew.shape[0], 1)
		return np.concatenate((X,xnew),axis=1)

	def modulate_features(self):
		num_fea = len(self.data)

		for i in range(num_fea):
			for j in range(i,num_fea):
				self.data.append(self.data[i]*self.data[j])
		return

	def load_data(self,db):
		c = db.cursor()

		c.execute("SELECT \
        ((1./3)*hist.sum_outs*2.25 + hist.sum_so*2 + hist.sum_w*4 - 2*hist.sum_er - 0.6*hist.sum_h - 0.6*hist.sum_bb - 0.6*hist.sum_hb + completed*2.5 + shutouts*2.5 + nohitters*5)/ game_count fph,\
        hist.sum_bb/hist.sum_bf,hist.sum_h/hist.sum_bf,hist.sum_np/hist.sum_bf,hist.sum_s/hist.sum_bf,hist.sum_so/hist.sum_bf,hist.sum_outs/game_count,hist.sum_s/hist.sum_np \
		FROM \
		(SELECT pid,sum(so) sum_so,sum(outs) sum_outs,sum(er) sum_er, sum(w) sum_w,sum(bf) sum_bf,sum(h) sum_h,sum(r) sum_r,sum(hr) sum_hr,sum(bb) sum_bb,sum(s) sum_s,sum(np) sum_np,sum(hb) sum_hb,count(pid) game_count,sum(case when outs=27 then 1 else 0 end) as completed,sum(case when (outs=27 and r=0) then 1 else 0 end) as shutouts,sum(case when (outs=27 and h=0) then 1 else 0 end) as nohitters \
		FROM pitchers \
		WHERE pid = %s and start = 1 and bf > 0 and year(gametime) = %s) \
		AS hist",
        (self.pid,self.year))

		r = c.fetchone()
		try:
			self.data = [float(v) for v in r]
		except:
			self.data_is_bad = True
			print 'ERROR FOR PITCHER'
			print self.pid,r
			self.data = [0. for v in r]
		return

	def add_home(self):
		self.data.append(self.home)
		return

	def add_temp(self):
		self.data.append(self.temp)
		return

	def add_venue(self,db):
		c = db.cursor()
		if self.home == 1:
			team = self.team
		else:
			team = self.opponent
		c.execute("SELECT venue_id FROM venues WHERE fanduel = %s",(team,))
		result = c.fetchone()

		for v in self.transforms['venues'].transform(result)[0]:
			self.data.append(v)

		return

	def add_venue_games(self,db):
		venue_games = []
		c = db.cursor()

		c.execute("SELECT venue_id,count(*)/games_total\
		FROM pitchers,\
		(SELECT count(*) games_total FROM pitchers WHERE pid=%s AND start=1 and bf>0 and year(gametime)=%s) btotal \
		WHERE pid=%s AND start=1 and bf>0 and year(gametime)=%s group by venue_id",(self.pid,self.year,self.pid,self.year,))
		results = c.fetchall()
		venues = self.transforms['venues'].transform([-1])[0]
		
		for venue in results:
			venues = venues + self.transforms['venues'].transform([venue[0]])[0]*float(venue[1])
		for v in venues:
			self.data.append(v)
		return 

	def add_opposing_batting(self,db):
		c = db.cursor()
		c.execute("SELECT \
			(sum(h)-sum(d)-sum(t)-sum(hr))/(sum(ab)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)), \
			sum(d)/sum(ab),sum(t)/(sum(ab)),\
			sum(hr)/sum(ab),(sum(h)-sum(hr))/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
			sum(so)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(hbp)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			sum(sb)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(rbi)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			sum(r)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(cs)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			(sum(ab)+sum(hbp)+sum(bb)+sum(sf))/count(pid),\
			sum(go)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),sum(ao)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
			sum(h)/sum(ab),(sum(h)+sum(d)+sum(t)*2+sum(hr)*3)/sum(ab), \
			sum(r)/count(distinct(gameid)) \
			FROM batters WHERE year(gametime) = %s AND team = (SELECT team from venues WHERE fanduel = %s)",(self.year,self.opponent))
		r = c.fetchone()
		for v in r:
			try:
				self.data.append(float(v))
			except:
				self.data_is_bad = True
				print 'ERROR! OPPOSING BATTING:',self.opponent
				self.data.append(0.)
		return

	def add_team_batting(self,db):
		c = db.cursor()
		c.execute("SELECT \
			(sum(h)-sum(d)-sum(t)-sum(hr))/(sum(ab)),sum(bb)/(sum(ab)+sum(bb)+sum(sf)), \
			sum(d)/sum(ab),sum(t)/(sum(ab)),\
			sum(hr)/sum(ab),(sum(h)-sum(hr))/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
			sum(so)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(hbp)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			sum(sb)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(rbi)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			sum(r)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),sum(cs)/(sum(ab)+sum(hbp)+sum(bb)+sum(sf)),\
			(sum(ab)+sum(hbp)+sum(bb)+sum(sf))/count(pid),\
			sum(go)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),sum(ao)/(sum(ab)-sum(so)-sum(hr)+sum(sf)),\
			sum(h)/sum(ab),(sum(h)+sum(d)+sum(t)*2+sum(hr)*3)/sum(ab), \
			sum(r)/count(distinct(gameid)) \
			FROM batters WHERE year(gametime) = %s AND team = (SELECT team from venues WHERE fanduel = %s)",(self.year,self.team))
		r = c.fetchone()
		for v in r:
			try:
				self.data.append(float(v))
			except:
				self.data_is_bad = True
				print 'ERROR! TEAM BATTING:',self.team
				self.data.append(0.)
		return

	def add_previous_year(self,db):
		c = db.cursor()

		c.execute("""SELECT sum(bb)/sum(bf),sum(h)/sum(bf),sum(np)/sum(bf),sum(s)/sum(bf),sum(so)/sum(bf),sum(r)/sum(bf),sum(s)/sum(np),sum(outs)/count(distinct(gameid)) FROM pitchers \
		WHERE pid = %s and gametime >= %s""",(self.pid,str(int(self.year)-1)+'-01-01'))

		r = c.fetchone()
		for v in r:
			try:
				self.data.append(float(v))
			except:
				self.data_is_bad = True
				print 'ERROR! PREVIOUS YEAR:',self.pid
				self.data.append(0.)
		return

	def pitching_prior_games(self,db,n,no):

		c = db.cursor()

		c.execute("SELECT bb/bf,h/bf,np/bf,s/bf,so/bf,r/bf,s/np,outs FROM pitchers WHERE bf> 0 and start = 1 and pid = %s order by gametime desc limit %s offset %s",(self.pid,n,no))
		r = c.fetchone()
		if r == None:
			self.data_is_bad = True
			for k in range(8):
				self.data.append(0.)
			return
		for v in r:
			try:
				self.data.append(float(v))
			except:
				self.data_is_bad = True
				print 'ERROR! PREVIOUS GAMES:',self.pid
				print 'Previous game:',no+1
				self.data.append(0.)

		return

	def add_opponent_pitcher(self,db):
		c = db.cursor()

		p_avg  = [0.0744,0.2350,3.7733,2.4001,0.1872,0.1187,0.6361,17.6559]

		c.execute("""SELECT sum(bb)/sum(bf),sum(h)/sum(bf),sum(np)/sum(bf),sum(s)/sum(bf),sum(so)/sum(bf),sum(r)/sum(bf),sum(s)/sum(np),sum(outs)/count(distinct(gameid)),count(distinct(gameid)) FROM pitchers \
		WHERE pid = %s and start=1 and bf>0 and gametime >= %s""",(self.opposing_pitcher,str(int(self.year)-1)+'-01-01'))

		r = c.fetchone()

		if r[0] != None:
			for k in r[:-1]:
				self.data.append(float(k))
		else:
			for k in p_avg:
				self.data.append(float(k))
		
		if r[0] == None:
			for k in [1,0,0,0,0,0]:
				self.data.append(k)
		elif r[-1] < 4:
			for k in [0,1,0,0,0,0]:
				self.data.append(k)
		elif r[-1] < 7:
			for k in [0,0,1,0,0,0]:
				self.data.append(k)
		elif r[-1] < 10:
			for k in [0,0,0,1,0,0]:
				self.data.append(k)
		elif r[-1] < 20:
			for k in [0,0,0,0,1,0]:
				self.data.append(k)
		else:
			for k in [0,0,0,0,0,1]:
				self.data.append(k)

		return

	def add_wind(self):
		windI = {'Out to CF':0,'L to R':1,'In from LF':2,'In from CF':3,'In from RF':4,'Out to RF':5,'Out to LF':6,'R to L':7}
		wind = [0,0,0,0,0,0,0,0]
		if self.useWeather:
			wind[windI[self.wind_dir]] = self.wind_speed
		for v in wind:
			self.data.append(v)
		# print wind

		return

	def checkStadium(self):
		if self.home == 1 and self.team not in ['MIA','TAM','ARI','MIL','HOU','TOR']:
			# print 'YES STADIUM',self.team
			return True
		elif self.home == 0 and self.opponent not in ['MIA','TAM','ARI','MIL','HOU','TOR']:
			# print 'YES STADIUM',self.team
			return True
		else:
			# print 'NO STADIUM',self.team
			return False

	def add_games_played(self):
		gp = [0,0,0,0,0]
		if self.gamesPlayed < 7:
			gp[0] = 1		
		elif self.gamesPlayed < 10:
			gp[1] = 1
		elif self.gamesPlayed < 15:
			gp[2] = 1
		elif self.gamesPlayed < 20:
			gp[3] = 1
		else:
			gp[4] = 1
		for k in gp:
			self.data.append(k)
		return

	def lineup_vs_pitcher(self,db):
		c = db.cursor()


		lvp_avg = [0.2625, 0.2973, 0.0068, 0.0058, 0.0544, 0.0326, 0.0792, 0.4320, 0.2682, 0.1754, 0.1149, 0.1797, 0.1097]

		c.execute("""SELECT sum(ao)/sum(ab),sum(go)/sum(ab),sum(cs)/sum(tpa),sum(t)/sum(ab),sum(d)/sum(ab),sum(hr)/sum(ab),sum(bb)/sum(tpa),sum(tb)/sum(ab),sum(h)/sum(ab),(sum(h)-sum(d)-sum(t)-sum(hr))/sum(ab),sum(r)/sum(tpa),sum(so)/sum(tpa),sum(rbi)/sum(tpa),sum(ab) \
		from battervspitcher WHERE pitchid = %s and batid IN (SELECT pid FROM draftkings_contests dk,players WHERE dk.dkid=players.dkid and dk.gameid=%s AND bo > 0 AND dk.pitcher = %s)""",(self.pid,self.gameid,self.pid))
		r = c.fetchone()

		for k in range(len(lvp_avg)):
			if r[0] == None:
				self.data.append(float(lvp_avg[k]))
				# print 'using AVG...'
			else:
				# print 'using DATA!'
				self.data.append(float(r[k]))

		if r[0] == None:
			for k in [1,0,0,0,0,0]:
				self.data.append(k)
		elif (r[-1] < 4):
			for k in [0,1,0,0,0,0]:
				self.data.append(k)
		elif (r[-1] < 7):
			for k in [0,0,1,0,0,0]:
				self.data.append(k)
		elif (r[-1] < 10):
			for k in [0,0,0,1,0,0]:
				self.data.append(k) 
		elif (r[-1] < 20):
			for k in [0,0,0,0,1,0]:
				self.data.append(k)
		elif (r[-1] >=20):
			for k in [0,0,0,0,0,1]:
				self.data.append(k)

		return

	def predict(self):
		if self.data_is_bad:
			print 'skipping pitcher with bad data:',self.pid
			self.fantasy_prediction = 0
			return 0

		elif self.gamesPlayed < 3:
			print 'skipping pitcher with not enough games:',self.pid
			self.fantasy_prediction = 0
			return 0

		else:

			self.modulate_features()
			model = pickle.load(open('models/model_pitcher_dk_R_21.p','rb'))
			data = np.array(self.data,dtype=float)[model['ind']]
			p = model['clf'].predict(data)[0]
			self.fantasy_prediction = float(p)
		return self.fantasy_prediction