import numpy as np
import time,pickle
from datetime import datetime,date,timedelta
import csv, sys, json, os
from sklearn import preprocessing,decomposition
from sklearn.linear_model import ElasticNet
from sklearn.metrics import mean_squared_error,mean_absolute_error
import pylab as plt
from sklearn.pipeline import Pipeline
import MySQLdb
from players_dk import batter as batter_dk
from players_dk import pitcher as pitcher_dk
from players import batter,pitcher
from cvxopt import matrix
from cvxopt.glpk import ilp
from cvxopt import glpk
from collections import defaultdict
from operator import itemgetter
import itertools, copy

glpk.options['msg_lev'] = 'GLP_MSG_OFF'

class lineupOptimizer():

  def __init__(self,excluded_players=[],excluded_teams=[],playersMustStart=True,platform='fanduel',year='2015'):
    self.db = MySQLdb.connect("localhost","lineup","","mlb",charset="utf8")
    self.all_players = []
    self.year = year
    self.excluded_players = excluded_players
    self.excluded_teams = excluded_teams
    self.playersMustStart = playersMustStart
    self.platform = platform

    ### Model parameters
    self.team_count = 0
    self.player_count = 0
    self.dup_player_count = 0
    self.all_players_dups = []
    self.dup_playersI = {}
    self.teamsI = {}


  def get_batters(self,gameid):
    if self.playersMustStart:
      player_condition = ''
    else:
      player_condition = 'OR bo IS NULL'


    c = self.db.cursor()
    if self.platform == 'fanduel':
      c.execute("""SELECT pid,fc.name,team,pos,salary,home,opp,bo,pitcher,temp,wind_speed,wind_dir from fanduel_contests fc,players WHERE fc.fid = players.fid and gameid = %s and (bo > 0 %s) and pos != 'P' and pid not in (%s) and team NOT IN (%s)"""%(gameid,player_condition,"'" + "','".join(self.excluded_players)+ "'","'" + "','".join(self.excluded_teams)+ "'"))
      for result in c.fetchall():
        B = batter(result[0],result[1],result[2],result[3],int(result[4]),result[5],result[6],result[7],result[8],result[9],result[10],result[11],self.year,self.db)
        B.load_player_data_full(self.db)
        B.predict()
        if B.fantasy_prediction > 0:
          self.all_players.append(B)
    elif self.platform == 'draftkings':
      c.execute("""SELECT pid,dk.name,playerteam.fanduel,pos,salary,home,oppteam.fanduel,bo,pitcher,temp,wind_speed,wind_dir from draftkings_contests dk,players,venues as playerteam,venues as oppteam WHERE oppteam.draftkings=dk.opp and playerteam.draftkings=dk.team and dk.dkid = players.dkid and gameid = %s and (bo > 0 %s) and pos != 'SP' and pos != 'RP' and pid not in (%s) and playerteam.fanduel NOT IN (%s)"""%(gameid,player_condition,"'" + "','".join(self.excluded_players)+ "'","'" + "','".join(self.excluded_teams)+ "'"))
      for result in c.fetchall():
        pos = result[3]
        pos_mult = pos.split('/')
        if len(pos_mult) == 1:
          pos_mult = pos_mult[0]
        B = batter_dk(result[0],result[1],result[2],pos_mult,int(result[4]),result[5],result[6],result[7],result[8],result[9],result[10],result[11],self.year,self.db)
        B.load_player_data_full(self.db)
        B.predict()
        if B.fantasy_prediction > 0:
          self.all_players.append(B)
    print 'Number of Players:',len(self.all_players)
    return

  def get_pitchers(self,gameid):
    c = self.db.cursor()
    if self.platform == 'fanduel':
      c.execute("""SELECT pid,fc.name,team,pos,salary,home,opp,temp,fc.fid,fc.gameid,wind_speed,wind_dir,pitcher from fanduel_contests fc,players WHERE fc.fid = players.fid and gameid = %s and pid in (SELECT DISTINCT(pitcher) FROM fanduel_contests WHERE gameid = %s and pitcher not in (%s)) and team NOT IN (%s)"""%(gameid,gameid,"'" + "','".join(self.excluded_players)+ "'","'" + "','".join(self.excluded_teams)+ "'"))
      for result in c.fetchall():
        P = pitcher(result[0],result[1],result[2],result[3],int(result[4]),result[5],result[6],result[7],result[8],result[9],result[10],result[11],result[12],self.year,self.db)
        P.load_player_data(self.db)
        P.predict()
        print P.pid,P.pos,P.name,P.team,P.salary,'%.2f'%P.fantasy_prediction,'%.2f'%(P.fantasy_prediction/P.salary*1000.)
        if P.fantasy_prediction > 0:
          self.all_players.append(P)
    elif self.platform == 'draftkings':
      c.execute("""SELECT pid,dk.name,playerteam.fanduel,pos,salary,home,oppteam.fanduel,temp,dk.dkid,dk.gameid,wind_speed,wind_dir,pitcher from draftkings_contests dk,players,venues as playerteam,venues as oppteam WHERE dk.dkid = players.dkid and oppteam.draftkings = dk.opp and playerteam.draftkings=dk.team and gameid = %s and pid in (SELECT DISTINCT(pitcher) FROM draftkings_contests WHERE gameid = %s and pitcher not in (%s)) and playerteam.fanduel NOT IN (%s)"""%(gameid,gameid,"'" + "','".join(self.excluded_players)+ "'","'" + "','".join(self.excluded_teams)+ "'"))
      for result in c.fetchall():
          P = pitcher_dk(result[0],result[1],result[2],result[3],int(result[4]),result[5],result[6],result[7],result[8],result[9],result[10],result[11],result[12],self.year,self.db)
          P.load_player_data(self.db)
          P.predict()
          print P.pid,P.pos,P.name,P.team,P.salary,'%.2f'%P.fantasy_prediction,'%.2f'%(P.fantasy_prediction/P.salary*1000.),P.opposing_pitcher
          if P.fantasy_prediction > 0:
            self.all_players.append(P)
    return

  def update_counts(self):
    dup_player_count = 0
    player_count = 0
    all_players = []
    dup_playersI = {}

    k = 0
    for _p in self.all_players:
      if type(_p.pos) is list:

        dup_playersI[_p.pid] = k
        dup_player_count += 1
        player_count += len(_p.pos)
        k += 1

        for _pos in _p.pos:
          all_players.append(copy.copy(_p))
          all_players[-1].pos = _pos
          # print all_players[-1].pos,all_players[-1].name
      else:
        player_count += 1
        all_players.append(_p)

    teams = {}
    teamsI = {}
    k = 0

    ## Get unique teams
    for player in self.all_players:
      if teams.has_key(player.team):
        teams[player.team] += 1
      else:
        teams[player.team] = 1
        teamsI[player.team] = k
        k += 1

    team_count = len(teams)

    self.all_players_dups = all_players
    self.team_count = team_count
    self.teamsI = teamsI
    self.dup_player_count = dup_player_count
    self.dup_playersI = dup_playersI
    self.player_count = player_count

    return

  def choose_lineup_teams(self,n=4):
    c = []
    G = []
    A = []


    f = []
    s = []
    first = []
    second = []
    third = []
    catcher = []
    ss = []
    of = []
    pitchers = []

    player_count = len(self.all_players)
    teams = {}
    teamsI = {}
    k = 0

    for player in self.all_players:
      if teams.has_key(player.team):
        teams[player.team] += 1
      else:
        teams[player.team] = 1
        teamsI[player.team] = k
        k += 1

    team_count = len(teams)
    teamG = np.zeros((3*team_count+1,player_count+2*team_count)) # number of players on team <=4 (+1 for salary)

    teamG[team_count+1:2*team_count+1,player_count:player_count+team_count] = -5*np.eye(team_count) ## use up to n teams
    teamG[2*team_count+1:,player_count+team_count:player_count+2*team_count] = -1*np.eye(team_count) ## use at least n teams
    print 'Number of teams:',team_count


    A = np.zeros((9,player_count+2*team_count)) # 7 positions and two alternate constraints for team
    A[7,player_count:player_count+team_count] = 1.
    A[8,player_count+team_count:] = 1.

    positionI = {'P':0,'1B':1,'2B':2,'3B':3,'C':4,'SS':5,'OF':6}


    pI = 0
    for player in self.all_players:

      ## update teams
      teamG[teamsI[player.team]+1,pI] = 1
      teamG[teamsI[player.team]+team_count+1,pI] = 1
      teamG[teamsI[player.team]+2*team_count+1,pI] = -1
      teamG[0,pI] = player.salary

      f.append(player.fantasy_prediction)

      A[positionI[player.pos],pI] = 1


      pI += 1

    c = matrix(f)
    G = matrix(teamG,tc='d')

    h = 4*np.ones((3*team_count+1,1)) ## only 4 players per team max
    h[team_count+1:2*team_count+1] = 0.
    h[2*team_count+1:] = -1.
    h[0] = 35000 ## set salary constraints

    h = matrix(h,tc='d')

    A = matrix(A,tc='d')
    b = matrix([1,1,1,1,1,1,3,n,team_count-n],tc='d')

    print G.size
    print h.size

    print A.size
    print b.size

    # print b

    (status, sol) = ilp(-c,G,h,A,b,B=set(range(player_count+2*team_count)))

    print(status, sol)

    total_sal = 0
    total_fp = 0
    unique_teams = defaultdict(int)

    for row in range(player_count):
      if sol[row] == 1:
        print self.all_players[row].pos,self.all_players[row].name,self.all_players[row].team,self.all_players[row].salary,self.all_players[row].fantasy_prediction
        total_sal += self.all_players[row].salary
        total_fp += self.all_players[row].fantasy_prediction
        unique_teams[self.all_players[row].team] += 1

    print '%i Unique Teams'%(len(unique_teams))
    print 'Total Salary: $%s'%total_sal
    print 'Projected Fantasy Points: %.2f'%total_fp


  def choose_lineup(self,batter_noise=0,pitcher_noise=0,verbose=False):
    c = []
    G = []
    A = []


    f = []
    teamG = np.zeros((2*self.team_count+1,self.player_count)) # number of players on team <=4 (+1 for salary)

    # print 'Number of teams:',team_count


    A = np.zeros((7,self.player_count)) # 7 positions

    positionI = {'P':0,'1B':1,'2B':2,'3B':3,'C':4,'SS':5,'OF':6}


    pI = 0

    for player in self.all_players:
      if batter_noise > 0:
        if player.pos == 'P':
          noise = np.random.normal(0,pitcher_noise)
        else:
          noise = np.random.normal(0,batter_noise)
      else:
        noise = 0

      if player.pos == 'P':
        batter_pitcher_team = player.team
        batter_pitcher_coef = 1
      else:
        batter_pitcher_team = player.opponent
        batter_pitcher_coef = .01

      ## update teams
      teamG[self.teamsI[player.team]+1,pI] = 1 ## no more than 4 players from 1 team

      try:
        teamG[self.teamsI[batter_pitcher_team]+self.team_count+1,pI] = batter_pitcher_coef ## no batters on pitcher's opposing team
      except:
        pass

      teamG[0,pI] = player.salary

      # print player.pid,noise,player.fantasy_prediction,noise+player.fantasy_prediction
      f.append(player.fantasy_prediction+noise)

      A[positionI[player.pos],pI] = 1


      pI += 1

    # print f
    c = matrix(f)
    G = matrix(teamG,tc='d')

    h = 4*np.ones((2*self.team_count+1,1)) ## only 4 players per team max
    h[self.team_count+1:] = 1.
    h[0] = 35000 ## set salary constraints

    h = matrix(h,tc='d')

    A = matrix(A,tc='d')
    b = matrix([1,1,1,1,1,1,3],tc='d')

    # print G.size
    # print h.size

    # print A.size
    # print b.size
    # print b

    (status, sol) = ilp(-c,G,h,A,b,B=set(range(self.player_count)))

    # print(status, sol)

    total_sal = 0
    total_fp = 0
    unique_teams = defaultdict(int)

    sortedplayerlist = sorted(self.all_players, key=lambda k: k.fantasy_prediction/k.salary)

    if verbose:
      for row in range(player_count):
        print sortedplayerlist[row].pid,sortedplayerlist[row].pos,sortedplayerlist[row].name,sortedplayerlist[row].team,sortedplayerlist[row].salary,'%.2f'%sortedplayerlist[row].fantasy_prediction,'%.2f'%(sortedplayerlist[row].fantasy_prediction/sortedplayerlist[row].salary*1000.)
      
      print '\n'
      print '*'*40
      print '\n'

    lineup_pids = set()
    for row in range(self.player_count):
      if sol[row] == 1:
        if verbose:
          print self.all_players[row].pid,self.all_players[row].pos,self.all_players[row].name,self.all_players[row].team,self.all_players[row].salary,'%.2f'%self.all_players[row].fantasy_prediction,'%.2f'%(self.all_players[row].fantasy_prediction/self.all_players[row].salary*1000.)
        if self.all_players[row].pos == 'P':
          selected_pitcher = self.all_players[row].pid
        total_sal += self.all_players[row].salary
        total_fp += self.all_players[row].fantasy_prediction
        unique_teams[self.all_players[row].team] += 1
        lineup_pids.add(str(self.all_players[row].pid)+self.all_players[row].pos)
      elif self.all_players[row].pos == 'P':
        # print self.all_players[row].pid,self.all_players[row].pos,self.all_players[row].name,self.all_players[row].team,self.all_players[row].salary,self.all_players[row].fantasy_prediction
        pass
    if verbose:
      print '%i Unique Teams'%(len(unique_teams))
      print 'Total Salary: $%s'%total_sal
      print 'Projected Fantasy Points: %.2f'%total_fp

    return lineup_pids,selected_pitcher

  def choose_lineup_dk(self,batter_noise=0,pitcher_noise=0,verbose=False):
    c = []
    G = []
    A = []
    f = []

    # number of players on team <=4; no batters opposing pitcher; salary
    teamG = np.zeros((2*self.team_count+self.dup_player_count+1,self.player_count))

    # print 'Number of teams:',team_count


    A = np.zeros((7,self.player_count)) # 7 positions (rows), 1 column for each player

    positionI = {'RP':0,'SP':0,'1B':1,'2B':2,'3B':3,'C':4,'SS':5,'OF':6}


    pI = 0

    for player in self.all_players_dups:
      if batter_noise > 0:
        if player.pos == 'SP' or player.pos == 'RP':
          noise = np.random.normal(0,pitcher_noise)
        else:
          noise = np.random.normal(0,batter_noise)
      else:
        noise = 0

      # assign coefficient for opposing pitcher
      if player.pos == 'SP' or player.pos == 'RP':
        batter_pitcher_team = player.team
        batter_pitcher_coef = 1
      else:
        batter_pitcher_team = player.opponent
        batter_pitcher_coef = .01

      ## update teams (fill in binary features for each player)
      teamG[self.teamsI[player.team]+1,pI] = 1 ## no more than 4 players from 1 team
      try:
        teamG[self.teamsI[batter_pitcher_team]+self.team_count+1,pI] = batter_pitcher_coef ## no batters on pitcher's opposing team
      except:
        pass

      ## Set restriction for using player only once if player has multiple positions
      if self.dup_playersI.has_key(player.pid):
        teamG[self.dup_playersI[player.pid]+2*self.team_count+1,pI] = 1

      teamG[0,pI] = player.salary

      # print player.pid,noise,player.fantasy_prediction,noise+player.fantasy_prediction
      f.append(player.fantasy_prediction+noise)

      A[positionI[player.pos],pI] = 1


      pI += 1

    # print f
    c = matrix(f)
    G = matrix(teamG,tc='d')

    h = 6*np.ones((2*self.team_count+self.dup_player_count+1,1)) ## only 6 players per team max
    h[self.team_count+1:] = 1. ## no batters on pitchers opposing team; max of 1 player with mult. positions
    h[0] = 50000 ## set salary constraints

    h = matrix(h,tc='d')

    A = matrix(A,tc='d')
    b = matrix([2,1,1,1,1,1,3],tc='d')

    # print G.size
    # print h.size

    # print A.size
    # print b.size
    # print b

    (status, sol) = ilp(-c,G,h,A,b,B=set(range(self.player_count)))

    # print(status, sol)

    total_sal = 0
    total_fp = 0
    unique_teams = defaultdict(int)
    unique_batting_teams = defaultdict(int)


    if verbose:
      sortedplayerlist = sorted(self.all_players_dups, key=lambda k: k.fantasy_prediction/k.salary)
      for row in range(self.player_count):
        print sortedplayerlist[row].pid,sortedplayerlist[row].pos,sortedplayerlist[row].name,sortedplayerlist[row].team,sortedplayerlist[row].salary,'%.2f'%sortedplayerlist[row].fantasy_prediction,'%.2f'%(sortedplayerlist[row].fantasy_prediction/sortedplayerlist[row].salary*1000.)
      
      print '\n'
      print '*'*40
      print '\n'

    lineup_pids = set()
    selected_pitchers = []
    for row in range(self.player_count):
      if sol[row] == 1:
        if verbose:
          print self.all_players_dups[row].pid,self.all_players_dups[row].pos,self.all_players_dups[row].name,self.all_players_dups[row].team,self.all_players_dups[row].salary,'%.2f'%self.all_players_dups[row].fantasy_prediction,'%.2f'%(self.all_players_dups[row].fantasy_prediction/self.all_players_dups[row].salary*1000.)
        if self.all_players_dups[row].pos == 'SP':
          selected_pitchers.append(self.all_players_dups[row].pid)
        else:
          unique_batting_teams[self.all_players_dups[row].team] += 1
        total_sal += self.all_players_dups[row].salary
        total_fp += self.all_players_dups[row].fantasy_prediction
        unique_teams[self.all_players_dups[row].team] += 1
        lineup_pids.add(str(self.all_players_dups[row].pid)+self.all_players_dups[row].pos)
      elif self.all_players_dups[row].pos == 'SP':
        # print self.all_players_dups[row].pid,self.all_players_dups[row].pos,self.all_players_dups[row].name,self.all_players_dups[row].team,self.all_players_dups[row].salary,self.all_players_dups[row].fantasy_prediction
        pass
    if verbose:
      print '%i Unique Teams'%(len(unique_teams))
      print 'Total Salary: $%s'%total_sal
      print 'Projected Fantasy Points: %.2f'%total_fp

    return lineup_pids,''.join(np.array(sorted(list(selected_pitchers)),dtype=str)),len(unique_batting_teams)

  def mc_top_lineups(self,iterations,n_l,k_l,p_max,batter_noise=.3,pitcher_noise=2.4,useFirst=False):
    lineup_counts = defaultdict(int)
    lineups = {}
    lineup_pitchers = {}

    bar_length = 30
    print '\n'*5

    ## Run lineup optimizer with projection + noise
    for k in range(iterations):

      if self.platform == 'draftkings':
        lineup,pitcher,n_teams = self.choose_lineup_dk(batter_noise=batter_noise,pitcher_noise=pitcher_noise)
        if n_teams < 3: ## Skip bad solution w/o enough teams for dk
          continue
      elif self.platform == 'fanduel':
        lineup,pitcher = self.choose_lineup(batter_noise=batter_noise,pitcher_noise=pitcher_noise)

      lineup_str = ''.join(np.array(sorted(list(lineup)),dtype=str))
      lineup_counts[lineup_str] += 1
      lineups[lineup_str] = lineup
      lineup_pitchers[lineup_str] = pitcher

      ## Progress bar
      percent = float(k) / iterations
      hashes = '#' * int(round(percent * bar_length))
      spaces = ' ' * (bar_length - len(hashes))
      sys.stdout.write("\rProgress: [{0}] {1}%".format(hashes + spaces, int(round(percent * 100))))
      sys.stdout.flush()      

    print '\n'*5
    print 'Unique lineups =',len(lineups.keys())
    print_max = n_l
    k = 0
    top_lineups = []
    for key, value in sorted(lineup_counts.iteritems(), key=lambda (k,v): (v,k),reverse=True):
      k += 1
      print value, lineup_pitchers[key], lineups[key]
      top_lineups.append(lineups[key])
      if k == print_max:
        break

    max_overlap = 100
    for i in list(itertools.combinations(enumerate(top_lineups), k_l)):
      if (i[0][0] != 0) and useFirst:
        continue
      overlap = 0
      
      ## Pitcher in lineup maximum of ___ times
      pitcher_count = defaultdict(int)
      for lineup_list in i:
        pitcher_count[lineup_pitchers[''.join(np.array(sorted(list(lineup_list[1])),dtype=str))]] += 1

      _flag = 0
      for _p in pitcher_count:
        if pitcher_count[_p] > p_max:
          # print 'Pitcher in too many lineups!'
          _flag=1
          break
      if _flag == 1:
        continue

      ## Compute overlap for valid lineups
      for sub_i in list(itertools.combinations(i, 2)):
        overlap += len(set.intersection(sub_i[0][1],sub_i[1][1]))

      ## Choose lineups if is current best
      if overlap < max_overlap:
        output = i
        max_overlap = int(overlap)
      # print overlap
      # print '*'*40
    try:
      print output
    except:
      print 'No matches found! Try increasing n_l.'
      return

    for l in output:
      print '*'*40
      total_sal = 0
      total_fp = 0
      for p in self.all_players_dups:
        if str(p.pid)+p.pos in l[1]:
          print p.pid,p.pos,p.name,p.team,p.salary,'%.2f'%p.fantasy_prediction,'%.2f'%(p.fantasy_prediction/p.salary*1000.)
          total_sal += p.salary
          total_fp += p.fantasy_prediction
      print 'Total Salary: $%s'%total_sal
      print 'Projected Fantasy Points: %.2f'%total_fp
      print '\n\n'


if __name__ == "__main__":

  try:
    gameid = sys.argv[1]
  except:
    gameid = 12576

  try:
    platform = sys.argv[2]
  except:
    platform = 'fanduel'

  t = time.time()
  ti= time.time()

  opt = lineupOptimizer(excluded_players=[],excluded_teams=[],playersMustStart=False,platform=platform)


  print 'Load Batters for Contest...'
  opt.get_batters(gameid)
  print 'Load Pitchers for Contest...'
  opt.get_pitchers(gameid)
  opt.update_counts()

  print 'Solving for lineups...'
  # opt.choose_lineup_teams(9)
  # opt.choose_lineup(verbose=True)
  # opt.choose_lineup_dk(verbose=True)

  # t = time.time()
  if platform == 'fanduel':
    opt.mc_top_lineups(60000,n_l=12,k_l=4,p_max=2,batter_noise=.36,pitcher_noise=4,useFirst=True)
  elif platform == 'draftkings':
    opt.mc_top_lineups(50000,n_l=8,k_l=4,p_max=2,batter_noise=1.1,pitcher_noise=5,useFirst=True)
  # print time.time()-t






